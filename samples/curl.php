<?php
declare (strict_types=1);

use mark\http\Client\Client;
use mark\Mark;

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

$client = Client::getInstance()
                ->appendData('token', hash('sha256', md5(\mark\system\Os::getAccept())))
                ->appendData('ticket', hash('sha256', md5(\mark\system\Os::getAccept())))
                ->appendData('time', \mark\date\Time::getMillisecond())
                ->appendData('accept', 'json')
                ->set('responseHeader', true)
    // ->setCookie(true)
                ->addHeader('Content-Type', 'application/json')
    // ->addHeader('Content-Type', 'text/plain')
    // ->addHeader('Authorization', 'Basic ' . base64_encode(md5(uniqid('', true))))
                ->addHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0')
                ->addHeader('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8')
                ->addHeader('mark-engine', implode('_', Mark::ENGINE))
                ->addHeader('mark-version', Mark::VERSION)
                ->post('https://auth.tianfu.ink/api.php/extend/response', 'json');

$response = $client->toArray();

$data = array(
    'ResponseCode' => $client->getResponseCode(),

    'Info' => $client->getInfo(),

    'type' => gettype($response),

    'getHeader' => $client->getHeaders(),

    'Response' => $response,
    'Json' => $client->toJson(),
    'Array' => $client->toArray(),
);

print_r($data);