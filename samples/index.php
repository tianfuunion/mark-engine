<?php
declare (strict_types=1);

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

// string_encrypt
$idnum = '412303197010241234';
$phone = '12345678901';
$tel = '0377-12345678';
$email = 'tianfuunion@tianfu.ink';
$short = '411303';

var_dump(string_replace($idnum, '×', 3, 11));
var_dump(string_replace($idnum, '×', 3, -4));
var_dump(string_replace($idnum, '*', 3));
var_dump(string_replace($idnum, '*', 0, -3));
echo PHP_EOL;

var_dump(string_replace($phone, '×', 3, 5));
var_dump(string_replace($phone, '×', 3, -3));
var_dump(string_replace($phone, '×', 0, -3));
echo PHP_EOL;

var_dump(string_replace($tel, '×', 3, 7));
var_dump(string_replace($tel, '*', 3, -3));
var_dump(string_replace($tel, '*', 0, -3));
echo PHP_EOL;

var_dump(string_replace($email, '×', 3, 8));
var_dump(string_replace($email, '×', 3, -11));
var_dump(string_replace($email, '×', 0, -11));
echo PHP_EOL;

var_dump(string_replace($short, '×', 2, 11));
var_dump(string_replace($short, '×', 2, -8));
echo PHP_EOL;

$lang = 'zh-cn';
$CamelCase = toCamelCase($lang, '-');
$toUnderScore = toUnderScore($lang, '-');

var_dump($CamelCase);
var_dump($toUnderScore); // 横线转驼峰
var_dump(toCamelCase(toUnderScore($lang, '-'))); // 横线转下划线

echo PHP_EOL;