<?php
declare (strict_types=1);

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}
try {
    echo PHP_EOL . '测试数据用例：' . PHP_EOL;
    $start = microtime(true);

    $currency = array(
        '整数' => array(
            1,
            10,
            101,
            1001,
            10010,
            110001,
            110101,
            1100010,
            11000100,
            11000101,
            206000.75,
            \mark\core\Cny::$MAX_INTEGRAL,
            \mark\core\Cny::$MAX_DIGITAL
        ),
        '小数' => array(
            PHP_FLOAT_MAX,
            PHP_FLOAT_EPSILON,
            PHP_FLOAT_MIN,
            PHP_FLOAT_MAX,

            '0.0123456',
            '123456789.0123456',
            '123456789.012',
            '123456789.01',
            '123456789012.300678'
        ),
        '零钱' => array(
            '1000.01',
            '508.00',
            20002.0123,
            '98700012001.0010'
        ),
        '零' => array(
            '-0',
            '0',
            '.0',
            '.00',
            '0.0',
            '0.00',
            '0.000',
            '0.0000',
            '0.00000',
            '0.000000',
        ),

        '随机' => array(rand(0, (int)\mark\core\Cny::$MAX_INTEGRAL),
                      rand(0, (int)\mark\core\Cny::$MAX_INTEGRAL),
                      mt_rand(0, (int)\mark\core\Cny::$MAX_INTEGRAL),
                      mt_rand(0, (int)\mark\core\Cny::$MAX_INTEGRAL),
                      random_int(0, (int)\mark\core\Cny::$MAX_INTEGRAL),
                      random_int(0, (int)\mark\core\Cny::$MAX_INTEGRAL)),
        '修正' => array('123456700098.0054321',
                      '0000123456789'),
        '错误' => array('',
                      '',
                      null,
                      true,
                      false,
                      'foo',
                      array())
    );

    foreach ($currency as $key => $item) {
        echo PHP_EOL . $key . '：' . PHP_EOL;

        for ($i = 0, $max = count($item); $i < $max; $i++) {
            echo $item[$i] . ' => '
                // . "简约版："
                . \mark\core\Cny::toupper($item[$i])
                // =. "完整版：" . \mark\core\Cny::toupper($item[$i], 6, true)
                . PHP_EOL;
            // $random = random_int(0, (int)\mark\core\Cny::$MAX_INTEGRAL);
            // echo $random . ' => ' . \mark\core\Cny::toupper($random) . PHP_EOL;
        }
    }

    echo '耗时：' . bcsub((string)microtime(true), (string)$start, 6) . PHP_EOL;

    echo PHP_EOL . '分位分割：如千分位：' . PHP_EOL;
    echo \mark\core\Cny::$MAX_DIGITAL . ' => ' . \mark\core\Cny::separator(\mark\core\Cny::$MAX_DIGITAL) . PHP_EOL;
    echo \mark\core\Cny::$MAX_INTEGRAL . ' => ' . \mark\core\Cny::separator(\mark\core\Cny::$MAX_INTEGRAL) . PHP_EOL;

    echo number_format((float)\mark\core\Cny::$MAX_INTEGRAL) . PHP_EOL;
    echo number_format((float)\mark\core\Cny::$MAX_INTEGRAL, 2) . PHP_EOL;
    echo number_format((float)\mark\core\Cny::$MAX_INTEGRAL, 2, ',', '.') . PHP_EOL;

    echo 'PHP_INT_MAX: ' . PHP_INT_MAX;
    echo 'PHP_FLOAT_MAX: ' . PHP_FLOAT_MAX;
    echo PHP_EOL;

} catch (Exception $e) {
    var_dump($e->getMessage());
}