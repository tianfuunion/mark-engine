<?php
declare (strict_types=1);

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

try {
    $dn = array('countryName' => 'CN', //所在国家名称英文简写
                'stateOrProvinceName' => '河南省', //所在省份名称
                'localityName' => '南阳市', //所在城市名称
                'organizationName' => '南阳市天府网络科技有限公司', //注册人姓名
                'organizationalUnitName' => '南阳市天府网络科技有限公司', //组织名称
                'commonName' => '天府联盟', //公共名称
                'emailAddress' => 'mark@tianfuunion.cn' //邮箱
    );

    p('Distinguished_Names::', $dn);

    $path = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'certificate' . DIRECTORY_SEPARATOR;
    p('Path::', $path);

    $openssl = new mark\crypto\OpenSsl();
    $config_path = '/usr/local/openssl/openssl.cnf';
    $openssl->setConfigPath($config_path);

    $passphrase = 'tianfuunion_private_passphrase_example';             //私钥密码
    $openssl->setPassPhrase($passphrase);

    $pub_key_path = $path . 'pub_unknown_key.key';  //公钥文件
    $pri_key_path = $path . 'pri_unknown_key.pem';  //私钥文件
    $openssl->setKeyPath($pub_key_path, $pri_key_path);
    if (file_exists($pub_key_path)) {
        p('pub_key_path::', file_get_contents($pub_key_path));
    }
    if (file_exists($pri_key_path)) {
        p('pri_key_path::', file_get_contents($pri_key_path));
    }

    $cer_path = $path . 'cert_unknown.cer';       //生成证书路径
    $pfx_path = $path . 'pfx_unknown.pfx';        //密钥文件路径
    $openssl->setCertPath($cer_path, $pfx_path);
    if (file_exists($cer_path)) {
        p('cer_path::', file_get_contents($cer_path));
    }
    if (file_exists($pfx_path)) {
        p('pfx_path::', file_get_contents($pfx_path));
    }

    $origin = json_encode($_SERVER, JSON_UNESCAPED_UNICODE);
    p('Origin::', strlen($origin), $origin);

    // 生成公私钥
    $openssl->export_key();

    $cert = \openssl_x509_read(file_get_contents($cer_path));
    // 生成证书
    $openssl->export_certificate($dn, array(), $cert ? $cert : null, 30, (int)(date('YmdHis') . time()));

    $iv = $openssl->getRandomString();
    $encrypt = $openssl->encrypt($origin, $passphrase, $iv);
    p($iv, 'Symmetric_Encrypt::', strlen($encrypt ?: ''), $encrypt);

    $decrypt = $openssl->decrypt($encrypt, $passphrase, $iv);
    p('Symmetric_Decrypt::', strlen($decrypt ?: ''), $decrypt);

    $encrypt = $openssl->public_encrypt($origin);
    p('Public_Encrypt::', strlen($encrypt ?: ''), $encrypt);

    $decrypt = $openssl->private_decrypt($encrypt);
    p('Private_Decrypt::', strlen($decrypt ?: ''), $decrypt);

    $encrypt = $openssl->private_encrypt($origin);
    p('Private_Encrypt::', strlen($encrypt ?: ''), $encrypt);

    $decrypt = $openssl->public_decrypt($encrypt);
    p('Public_Decrypt::', strlen($decrypt ?: ''), $decrypt);

    $signtext = $openssl->sign($origin);
    p('SignText::', strlen($signtext), $signtext);

    $sign_verify = $openssl->verify($origin, $signtext);
    p('SignVerify::', $sign_verify);
} catch (\mark\exception\InvalidArgumentException $e) {
    p('InvalidArgumentException::', $e);
} catch (\mark\exception\UnexpectedValueException $e) {
    p('UnexpectedValueException::', $e);
}