<?php
declare (strict_types=1);

namespace mark\date;

/**
 * Class Time
 *
 * @description PHP 获取常用的起始时间戳和结束时间戳的时间处理类：
 * 一、常用时间戳
 * 1、前年、去年、今年、明年、后年
 * 2、前季度、上季度、当季度、下季度、后季度；当年的4个季度
 * 3、前月、上月、当月、下月、后月；当年的12个月起始时间戳
 * 4、前周、上周、本周、下周、后周
 * 5、前天、昨天、今天、明天、后天
 * 二、根据传入的时间戳计算出相应的时间戳。
 * 例：传入 {timestamp} 计算出当天的起始时间戳，Input选择时不易选择当天的起始时间
 * 四、时间戳转换
 * 跨时区
 * @author      Mark<mark@tianfuunion.cn>
 * @package     mark\date
 */
class Time {

    /**
     * 返回前天开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function beforeDay($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), (int)date('d', (int)$time) - 2, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (date('d', (int)$time) - 2), (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回昨日开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function yesterday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), (int)date('d', (int)$time) - 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), (int)date('d', (int)$time), (int)date('Y', (int)$time)) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回今日开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function today($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), (int)date('d', (int)$time), (int)date('Y', (int)$time));
        } else {

            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('d', (int)$time), (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回明天开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function tomorrow($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('d', (int)$time), (int)date('Y', (int)$time)) + 1;
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('d', (int)$time) + 1, (int)date('Y', (int)$time));

        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回后天开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function afterDay($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), (int)date('d', (int)$time) + 2, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('d', (int)$time) + 2, (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回周开始或结束的时间戳
     *
     * @param string $flag 默认为周的开始，start null 为开始，end 为结束
     * @param null   $time 默认为本周
     *
     * @return int
     */
    public static function week($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Monday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Monday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周一开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function monday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Monday', (int)$time)));
        } else {

            $timestamp = strtotime(date('Y-m-d', strtotime('this week Tuesday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周二开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function tuesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Tuesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Wednesday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周三开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function wednesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Wednesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Thursday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周四开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function thursday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Thursday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Friday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周五开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function friday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Friday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Saturday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周六开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function saturday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Saturday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Sunday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本周日开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function sunday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Sunday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Monday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回前周开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return float|int
     */
    public static function beforeWeek($flag = '', $time = null) {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Monday', (int)$time))) - 7 * 86400;
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Monday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeek($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Monday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('this week Monday', (int)$time))) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeek($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Monday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回后周开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function afterWeek($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time))) + 86400;
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time))) + 8 * 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周一开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekMonday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Monday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Monday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周二开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekTuesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Tuesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Tuesday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     *  返回上周三开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekWednesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Wednesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Wednesday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周四开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekThursday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Thursday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Thursday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周五开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekFriday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Friday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Friday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周六开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekSaturday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Saturday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Saturday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上周日开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastWeekSunday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Sunday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('last week Sunday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周一开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekMonday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Monday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Monday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周二开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekTuesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Tuesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Tuesday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周三开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekWednesday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Wednesday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Wednesday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周四开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekThursday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Thursday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Thursday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周五开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekFriday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Friday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Friday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周六开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekSaturday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Saturday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Saturday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下周日本开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextWeekSunday($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time)));
        } else {
            $timestamp = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time))) + 86399;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回前月开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function beforeMonth($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time) - 2, 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time) - 1, 0, (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回上个月开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastMonth($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time) - 1, 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), 0, (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回本月开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function month($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time), 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('t', (int)$time), (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回下月开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextMonth($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time), (int)date('t', (int)$time), (int)date('Y', (int)$time)) + 1;
        } else {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time) + 1, (int)date('t', (int)$time), (int)date('Y', (int)$time)) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回后月开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function afterMonth($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, (int)date('m', (int)$time) + 2, 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, (int)date('m', (int)$time) + 2, (int)date('t', (int)$time), (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 获取季度开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function quarter($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        $quarter = (int)ceil((date('n', (int)$time)) / 3);
        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, $quarter * 3 - 2, 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(0, 0, 0, $quarter * 3 + 1, 1, (int)date('Y', (int)$time)) - 1;
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回年开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function year($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, 1, 1, (int)date('Y', (int)$time));
        } else {
            $timestamp = mktime(23, 59, 59, 12, 31, (int)date('Y', (int)$time));
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回前年开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function beforeYear($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, 1, 1, date('Y', (int)$time) - 2);
        } else {
            $timestamp = mktime(23, 59, 59, 12, 31, date('Y', (int)$time) - 2);
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回去年开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function lastYear($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, 1, 1, date('Y', (int)$time) - 1);
        } else {
            $timestamp = mktime(23, 59, 59, 12, 31, date('Y', (int)$time) - 1);
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回明年开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function nextYear($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, 1, 1, date('Y', (int)$time) + 1);
        } else {
            $timestamp = mktime(23, 59, 59, 12, 31, date('Y', (int)$time) + 1);
        }

        return $timestamp === false ? 0 : $timestamp;
    }

    /**
     * 返回后年开始或结束的时间戳
     *
     * @param string $flag
     * @param null   $time
     *
     * @return int
     */
    public static function afterYear($flag = '', $time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        if (empty($flag) || $flag == '' || $flag === 'start' || $flag === 'begin') {
            $timestamp = mktime(0, 0, 0, 1, 1, date('Y', (int)$time) + 2);
        } else {
            $timestamp = mktime(23, 59, 59, 12, 31, date('Y', (int)$time) + 2);
        }

        if ($timestamp === false) {
            return 0;
        }

        return $timestamp;
    }

    /**
     *第几天
     *
     * @param null $time
     *
     * @return int
     */
    public static function dayOf($time = null): int {
        if (empty($time) || $time == '') {
            $time = time();
        }

        $date = strtotime(date('Y-m-d', strtotime('next week Sunday', (int)$time))) + 86399;

        return $date === false ? 0 : $date;
    }

    /**
     * 获取几天前零点到现在/昨日结束的时间戳
     *
     * @param int  $day 天数
     * @param bool $now 返回现在或者昨天结束时间戳
     *
     * @return array
     */
    public static function dayToNow($day = 1, $now = true): array {
        $end = time();
        if (!$now) {
            $end = self::yesterday();
        }

        return array(mktime(0, 0, 0, date('m'), date('d') - $day, date('Y')), $end);
    }

    /**
     * 返回几天前的时间戳
     *
     * @param int $day
     *
     * @return int
     */
    public static function daysAgo($day = 1): int {
        return time() - self::daysToSecond($day);
    }

    /**
     * 返回几天后的时间戳
     *
     * @param int $day
     *
     * @return int
     */
    public static function daysAfter($day = 1): int {
        return time() + self::daysToSecond($day);
    }

    /**
     * 天数转换成秒数
     *
     * @param int $day
     *
     * @return int
     */
    public static function daysToSecond($day = 1): int {
        return $day * 86400;
    }

    /**
     * 周数转换成秒数
     *
     * @param int $week
     *
     * @return int
     */
    public static function weekToSecond(int $week = 1): int {
        return self::daysToSecond() * 7 * $week;
    }

    /**
     * 获取日期对应的星期
     * 参数$date为输入的日期数据，格式如：2018-6-22
     *
     * @param $date
     *
     * @return string
     */
    public static function weekForData($date): string {
        //强制转换日期格式
        $date_str = date('Y-m-d', strtotime($date));
        //封装成数组
        $arr = explode('-', $date_str);
        //参数赋值

        //年
        $year = $arr[0];
        //月，输出2位整型，不够2位右对齐
        $month = sprintf('%02d', $arr[1]);
        //日，输出2位整型，不够2位右对齐
        $day = sprintf('%02d', $arr[2]);
        //时分秒默认赋值为0；
        $hour = $minute = $second = 0;
        //转换成时间戳
        $strap = mktime($hour, $minute, $second, $month, $day, $year);
        //获取数字型星期几
        $number_wk = date('w', $strap);
        //自定义星期数组
        $weekArr = array('0', '1', '2', '3', '4', '5', '6');

        //获取数字对应的星期
        return $weekArr[$number_wk];
    }

    /**
     * 获取一周日期
     *
     * @param null   $time
     * @param string $format
     *
     * @return array
     */
    public static function getDateForWeek($time = null, $format = 'Y-m-d'): array {
        if (empty($time) || $time == '') {
            $time = time();
        }

        $week = date('w', (int)$time);
        $weekName = array('星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日');
        //星期日排到末位
        if (empty($week)) {
            $week = 7;
        }

        $data = array();
        for ($i = 0; $i <= 6; $i++) {
            $data[$i]['date'] = date($format, strtotime('+' . ($i + 1 - $week) . ' days', (int)$time));
            $data[$i]['week'] = $weekName[$i];
        }

        return $data;
    }

    /**
     * 时间戳转星期
     *
     * @param null $time
     *
     * @return string
     */
    public static function getWeekByDate($time = null): string {
        if (empty($time) || $time == '') {
            $time = time();
        }

        $week = date('w', (int)$time);
        //星期日排到末位
        if (empty($week)) {
            $week = 7;
        }

        $weekName = array('星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日');

        return $weekName[$week - 1];
    }

    /**
     * 获取毫秒级别的时间戳
     *
     * @return int
     */
    public static function getMillisecond(): int {
        return (int)(microtime(true) * 1000);
    }

    /**
     * 根据输入的时间戳转换成时间字符串，
     * 最小处理时间为 毫秒MS
     * <p>
     * 此方法尚存在BUG,超过 24 小时输出错误，
     *
     * @param int    $timestamp
     * @param string $accuracy
     *
     * @return string
     */
    public static function timer(int $timestamp, $accuracy = 's'): string {
        if ($timestamp <= 1000 && $accuracy === 'ms') {
            $format = 'u';
        } elseif ($timestamp <= 60 && $accuracy === 's') {
            $format = 's';
        } elseif ($timestamp <= 60 * 60 && $accuracy === 's') {
            $format = 'i:s';
        } elseif ($timestamp <= 60 * 60 * 24 && $accuracy === 's') {
            $format = 'H:i:s';
        } elseif ($timestamp <= 60 * 60 * 24 * 30 && $accuracy === 's') {
            $format = 'd H:i:s';
        } elseif ($timestamp <= 60 * 60 * 24 * 30 * 12 && $accuracy === 's') {
            $format = 'm-d H:i:s';
        } else {
            $format = 'Y-m-d H:i:s';
        }

        return date($format, $timestamp) . '毫秒';
    }

    /**
     * 聊天消息时间显示说明
     * 1、当天的消息，以每5分钟为一个跨度的显示时间；
     * 2、消息超过1天、小于1周，显示星期+收发消息的时间；
     * 3、消息大于1周，显示手机收发时间的日期。
     * 三、消息时间计算：
     * 1、刚刚、5分钟以内
     * 2、今天：20：24
     * 3、昨天：昨天14：10
     * 4、前天：前天10：24
     * 5、周一、周二｛超过3天周显示为本周｝
     * 6、3.8 10：24｛超过本周则带上日期｝
     * 7、2018 11.11 10：24 ｛超过本年则带上年度｝
     *
     * @param int    $time
     * @param string $accuracy
     *
     * @return string
     */
    public static function getChatTime(int $time, $accuracy = 'm'): string {
        $now = time();
        if ($now > $time && ($now - $time) <= 300) {
            return '刚刚';
        }

        if ($time >= self::today() && $time <= self::today('end')) {
            if ($accuracy === 's') {
                return date('H:i:s', $time) ?: '';
            }

            if ($accuracy === 'm') {
                return date('H:i', $time) ?: '';
            }

            return date('H:i', $time) ?: '';
        }

        if ($time >= self::yesterday() && $time <= self::yesterday('end')) {
            if ($accuracy === 's') {
                return '昨天' . date('H:i:s', $time) ?: '';
            }

            if ($accuracy === 'm') {
                return '昨天' . date('H:i', $time) ?: '';
            }

            return '昨天' . date('H:i', $time) ?: '';
        }

        if ($time >= self::beforeDay() && $time <= self::beforeDay('end')) {
            if ($accuracy === 's') {
                return '前天' . date('H:i:s', $time) ?: '';
            }

            if ($accuracy === 'm') {
                return '前天' . date('H:i', $time) ?: '';
            }

            return '前天' . date('H:i', $time) ?: '';
        }

        if ($time >= self::week() && $time <= self::week('end')) {
            if ($accuracy === 's') {
                return self::weekForData($time) . date('H:i:s', $time) ?: '';
            }

            if ($accuracy === 'm') {
                return self::weekForData($time) . date('H:i', $time) ?: '';
            }

            return self::weekForData($time) . date('H:i', $time) ?: '';
        }

        if ($accuracy === 's') {
            return date('Y-m-d H:i:s', $time) ?: '';
        }

        if ($accuracy === 'm') {
            return date('Y-m-d H:i', $time) ?: '';
        }

        return date('Y-m-d H:i', $time) ?: '';
    }

    /**
     * 根据生日计算年龄
     *
     * @param $birthday
     *
     * @return int
     */
    public static function birthday($birthday): int {
        $year = substr($birthday, 0, 4);
        $month = substr($birthday, 4, 2);
        $day = substr($birthday, 6, 2);

        if ($day === false) {
            return 0;
        }
        if ($month === false) {
            return 0;
        }
        if ($day === false) {
            return 0;
        }

        // list($year, $month, $day) = explode('-', $birthday);
        $year_diff = date('Y') - (int)$year;
        $month_diff = date('m') - (int)$month;
        $day_diff = date('d') - (int)$day;
        if ((int)$day_diff < 0 || (int)$month_diff < 0) {
            $year_diff--;
        }

        return (int)$year_diff;
    }
}