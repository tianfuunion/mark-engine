<?php
declare (strict_types=1);

namespace mark\crypto;

use mark\exception\ClassNotFoundException;
use mark\exception\InvalidArgumentException;
use mark\exception\UnexpectedValueException;

/**
 * Class OpenSsl
 *
 * @description PHP OpenSsl扩展学习以及基本使用
 * <p></p>
 * <p>openssl还提供了很多函数，其中涉及了几个名词，pkcs12，pkcs7，x509，围绕这几个名词提供了许多函数，这里介绍一下它们的含义作用以及区别:</p>
 * <p>x509，公钥证书，只有公钥。</p>
 * <p>pkcs7，签名或加密。可以往里面塞x509，同时没有签名或加密内容。</p>
 * <p>pkcs12，含有私钥，同时可以有公钥，有口令保护。</p>
 * <p>pkcs7的作用就是电子信封。</p>
 * <p></p>
 * <p>X509是基本规范，pkcs7和pkcs12是两个实现规范，pkcs7是数字信封，pkcs12是带有私钥的证书规范。</p>
 * <p>x509是数字证书的规范，pkcs7和pkcs12是两种封装形式。比如说同样的电影，有的是avi格式，有的是mpg，大概就这个意思。</p>
 * <p>pkcs7一般是把证书分成两个文件，一个公钥一个私钥，有PEM和DER两种编码方式。PEM比较多见，就是纯文本的，pkcs7一般是分发公钥用，看到的就是一串可见字符串，扩展名经常是.crt,.cer,.key等。.der是二进制编码。</p>
 * <p>pkcs12是把证书压成一个文件，.pfx 。主要是考虑分发证书，私钥是要绝对保密的，不能随便以文本方式散播。所以pkcs7格式不适合分发。.pfx中可以加密码保护，所以相对安全些。</p>
 * <p>在实践中要中，用户证书都是放在USB Key中分发，服务器证书经常还是以文件方式分发。</p>
 * <p></p>
 * <p>服务器证书和用户证书，都是X509证书，就是里面的属性有区别。</p>
 * <p>X509 是证书规范</p>
 * <p>pkcs7 是消息语法 （常用于数字签名与加密）</p>
 * <p>pkcs12 个人消息交换与打包语法 （如.PFX .pkcs12）打包成带公钥与私钥</p>
 * <p></p>
 * @link        https://www.php.net/manual/zh/book.openssl.php
 * @package     mark\crypto
 */
class OpenSsl {

    /**
     * 自定义 openssl.conf 文件的路径。
     *
     * @var string
     */
    private $config_path;

    /**
     * @description 私钥密码：openssl_private_passphrase
     *
     * @param string $passphrase [optional] <p>The key can be optionally protected by a <i>passphrase</i>.</p>
     */
    private $passphrase;

    private $pri_key_path;      //私钥文件路径：./certificate/pri_key.pem
    private $pub_key_path;      //公钥文件路径：./certificate/pub_key.key
    private $cer_path;          //生成证书路径：./certificate/cert.cer
    private $pfx_path;          //密钥文件路径：./certificate/pkcs12.pfx

    private $public_key;      // 公钥内容
    private $private_key;     // 私钥内容
    private $certificate;     // 公钥证书
    private $pkcs12;          // 私钥证书

    public const KEY_LENGTH_BYTE = 32;
    public const AUTH_TAG_LENGTH_BYTE = 16;

    /**
     * OpenSsl constructor.
     */
    public function __construct() {
        if (!extension_loaded('openssl')) {
            throw new ClassNotFoundException('PHP extension "openssl" is required.');
        }
    }

    /**
     * 设置自定义 openssl.conf 文件的路径。
     *
     * @param string $path
     *
     * @return $this
     */
    public function setConfigPath(string $path): self {
        if (!empty($path)) {
            $this->config_path = $path;
        }

        return $this;
    }

    /**
     * 设置密钥文件路径
     *
     * @param string $public_key_path  公钥文件路径
     * @param string $private_key_path 私钥文件路径
     *
     * @return $this
     */
    public function setKeyPath(string $public_key_path, string $private_key_path): self {
        if (!empty($public_key_path)) {
            $this->pub_key_path = $public_key_path;
        }

        if (!empty($private_key_path)) {
            $this->pri_key_path = $private_key_path;
        }

        return $this;
    }

    /**
     * 设置证书文件路径
     *
     * @param string $cer_path 公钥证书文件路径.cer
     * @param string $pfx_path 私钥证书文件路径.pfx
     *
     * @return $this
     */
    public function setCertPath(string $cer_path, string $pfx_path): self {
        if (!empty($cer_path)) {
            $this->cer_path = $cer_path;
        }

        if (!empty($pfx_path)) {
            $this->pfx_path = $pfx_path;
        }

        return $this;
    }

    /**
     * 设置公钥内容
     *
     * @param string $public_key
     *
     * @return $this
     */
    public function setPublicKey(string $public_key): self {
        if (!empty($public_key)) {
            $this->public_key = $public_key;
        }

        return $this;
    }

    /**
     * 设置私钥内容
     *
     * @param string $private_key
     *
     * @return $this
     */
    public function setPrivateKey(string $private_key): self {
        if (!empty($private_key)) {
            $this->private_key = $private_key;
        }

        return $this;
    }

    /**
     * 设置私钥证书
     *
     * @param mixed $certificate
     *
     * @return $this
     */
    public function setCertificate($certificate): self {
        if (!empty($certificate)) {
            $this->certificate = $certificate;
        }

        return $this;
    }

    /**
     * 设置公钥证书
     *
     * @param string $pkcs12
     *
     * @return $this
     */
    public function setPkcs12(string $pkcs12): self {
        if (!empty($pkcs12)) {
            $this->pkcs12 = $pkcs12;
        }

        return $this;
    }

    /**
     * 设置密钥口令
     *
     * @param string $pass
     *
     * @return $this
     */
    public function setPassPhrase(string $pass): self {
        if (strlen($pass) != self::KEY_LENGTH_BYTE) {
            // throw new InvalidArgumentException('无效的密钥口令，长度应为32个字节');
        }

        if (!empty($pass)) {
            $this->passphrase = $pass;
        }

        return $this;
    }

    /**
     * 生成密钥文件
     *
     * @param array $options
     *
     * @return bool
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function export_key($options = array()): bool {
        if (empty($options)) {
            $options = array(
                'private_key_bits' => 4096, // Size of Key.
                'private_key_type' => OPENSSL_KEYTYPE_RSA
            );
        }

        $pkey = openssl_pkey_new($options);
        // 生成一个新的私钥 openssl_pkey_new ([ array $options ] ) options参数微调密钥的生成（比如private_key_bits 指定应该使用多少位来生成私钥）
        if ($pkey == false) {
            // $res返回false的时候，检查发现，是window系统缺少了openssl环境变量，解决方法如下：
            $options['config'] = $this->config_path;
            $pkey = openssl_pkey_new($options);
        }
        if ($pkey == false) {
            throw new UnexpectedValueException('Invalid key information');
        }

        if (empty($this->pri_key_path)) {
            throw new InvalidArgumentException('Invalid private key path');
        }
        // 创建私钥目录
        $pri_dir = dirname($this->pri_key_path);
        if (!is_dir($pri_dir) && !mkdir($pri_dir, 0777, true)) {
            throw new InvalidArgumentException('openssl private key directory creation failed:' . $this->pri_key_path);
        }
        // 创建私钥文件
        if (!file_exists($this->pri_key_path) && !touch($this->pri_key_path)) {
            throw new InvalidArgumentException('openssl private key file creation failed:' . $this->pri_key_path);
        }

        if (empty($this->pub_key_path)) {
            throw new InvalidArgumentException('Invalid public key path');
        }
        // 创建公钥目录
        $pub_dir = dirname($this->pub_key_path);
        if (!is_dir($pub_dir) && !mkdir($pub_dir, 0777, true)) {
            throw new InvalidArgumentException('openssl public key directory creation failed:' . $this->pub_key_path);
        }
        // 创建公钥文件
        if (!file_exists($this->pub_key_path) && !touch($this->pub_key_path)) {
            throw new InvalidArgumentException('openssl public key file creation failed:' . $this->pub_key_path);
        }

        //将密钥字符串输出到文件
        // $result = openssl_pkey_export($pkey, $this->pri_key_path, $this->passphrase?:null, $options);

        //生成密钥文件
        $result = openssl_pkey_export_to_file($pkey, $this->pri_key_path, $this->passphrase ?: null, $options);
        $this->private_key = $pkey;
        if ($result == false) {
            throw new UnexpectedValueException('openssl private key export failure:' . $this->pri_key_path);
        }

        // 从私钥中获取公钥
        $pkey_details = openssl_pkey_get_details($pkey);
        if ($pkey_details == false || empty($pkey_details['key'] ?? '')) {
            throw new UnexpectedValueException('openssl public key export failure');
        }

        return file_put_contents($this->pub_key_path, $pkey_details['key'] ?? '') != false;
    }

    /**
     * 生成证书文件
     *
     * @param array      $dn      The Distinguished Name to be used in the certificate.
     * @param array|null $options 你可以通过options确定CSR签名
     * @param mixed      $ca_cert 生成的证书将由ca_cert签名。 如果ca_cert 为 null, 生成的证书将是自签名证书。
     * @param int        $days    有效时长(天)
     * @param int        $serial  可选的发行证书编号。如果没有指定默认值为0.
     *
     * @return bool
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function export_certificate(array $dn, array $options = null, $ca_cert = null, int $days = 30, int $serial = 0): bool {
        $names['countryName'] = $dn['countryName'] ?: 'CN';
        if (!empty($dn['stateOrProvinceName'] ?? '')) {
            $names['stateOrProvinceName'] = $dn['stateOrProvinceName'];
        }
        if (!empty($dn['localityName'] ?? '')) {
            $names['localityName'] = $dn['localityName'];
        }
        if (!empty($dn['organizationName'] ?? '')) {
            $names['organizationName'] = $dn['organizationName'];
        }
        if (!empty($dn['organizationalUnitName'] ?? '')) {
            $names['organizationalUnitName'] = $dn['organizationalUnitName'];
        }
        if (!empty($dn['commonName'] ?? '')) {
            $names['commonName'] = $dn['commonName'];
        }
        if (!empty($dn['emailAddress'] ?? '')) {
            $names['emailAddress'] = $dn['emailAddress'];
        }

        if (empty($options)) {
            $options = array(
                'private_key_bits' => 4096, // Size of Key.
                'private_key_type' => OPENSSL_KEYTYPE_RSA
            );
        }

        $options['digest_alg'] = 'SHA256';
        if (!empty($this->config_path)) {
            $options['config'] = $this->config_path;
        }

        $private_key = $this->get_private_key();

        //根据dn提供的信息生成新的CSR（证书签名请求） priKey 应该被设置为由openssl_pkey_new()函数预先生成(或者以其他方式从openssl_pkey函数集中获得)的私钥。
        //该密钥的相应公共部分将用于签署CSR.
        $csr = openssl_csr_new($names, $private_key, $options);
        if ($csr == false) {
            throw new UnexpectedValueException('openssl csr new failure');
        }

        //用另一个证书签署 CSR (或者本身) 并且生成一个证书 从给定的 CSR 生成一个x509证书资源
        $signed_cert = openssl_csr_sign($csr, $ca_cert ?: null, $private_key, $days, $options, $serial);
        if ($signed_cert == false || empty($signed_cert)) {
            throw new UnexpectedValueException('openssl csr sign failure');
        }

        if (empty($this->cer_path)) {
            throw new InvalidArgumentException('Invalid certificate path');
        }
        // 创建证书目录
        $dir = dirname($this->cer_path);
        if (!is_dir($dir) && mkdir($dir, 0777, true)) {
            throw new InvalidArgumentException('openssl certificate directory creation failed:' . $this->cer_path);
        }
        // 创建证书文件
        if (!file_exists($this->cer_path) && !touch($this->cer_path)) {
            throw new InvalidArgumentException('openssl certificate file creation failed:' . $this->cer_path);
        }

        if (empty($this->pfx_path)) {
            throw new InvalidArgumentException('Invalid pkcs12 path');
        }
        // 创建密钥目录
        $pfx_dir = dirname($this->pfx_path);
        if (!is_dir($pfx_dir) && !mkdir($pfx_dir, 0777, true)) {
            throw new InvalidArgumentException('openssl pkcs12 directory creation failed:' . $this->pfx_path);
        }
        // 创建密钥文件
        if (!file_exists($this->pfx_path) && !touch($this->pfx_path)) {
            throw new InvalidArgumentException('openssl pkcs12 file creation failed:' . $this->pfx_path);
        }

        //导出证书$csr_key 将 x509 以PEM编码的格式导出到名为 output 的字符串类型的变量中 公钥证书 只有公钥
        // openssl_x509_export($signed_cert, $csr_key);

        //导出证书到文件
        $x509 = openssl_x509_export_to_file($signed_cert, $this->cer_path);
        if ($x509 == false) {
            throw new UnexpectedValueException('openssl x509 export to file failure:' . $this->cer_path);
        }

        // 导出pkcs12密钥
        // openssl_pkcs12_export($signed_cert, $privateKey, $priKey, $this->passphrase?:'');

        // 生成pkcs12密钥文件
        return openssl_pkcs12_export_to_file($signed_cert, $this->pfx_path, $private_key, $this->passphrase ?: '');
    }

    /**
     * 获取公钥资源
     *
     * @return resource
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function get_public_key() {
        if (!empty($this->public_key)) {
            return $this->public_key;
        }

        if (!file_exists($this->pub_key_path)) {
            throw new InvalidArgumentException('The public key file does not exist:' . $this->pub_key_path);
        }

        $contents = file_get_contents($this->pub_key_path);
        if ($contents == false || empty($contents)) {
            throw new InvalidArgumentException('Invalid public key information:' . $this->pub_key_path);
        }

        $pub_key = openssl_get_publickey($contents);
        if (!$pub_key) {
            throw new UnexpectedValueException('Failure of public key information acquisition:' . $this->pub_key_path);
        }

        $this->public_key = $pub_key;
        return $pub_key;
    }

    /**
     * 获取私钥资源
     *
     * @return resource
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function get_private_key() {
        if (!empty($this->private_key)) {
            return $this->private_key;
        }

        if (!file_exists($this->pri_key_path)) {
            throw new InvalidArgumentException('The private key file does not exist:' . $this->pri_key_path);
        }

        $contents = file_get_contents($this->pri_key_path);
        if ($contents == false || empty($contents)) {
            throw new InvalidArgumentException('Invalid private key information:' . $this->pri_key_path);
        }

        $pri_key = openssl_get_privatekey($contents);
        if ($pri_key == false) {
            $pri_key = openssl_get_privatekey($contents, $this->passphrase);
        }
        if ($pri_key == false) {
            throw new UnexpectedValueException('Failure of private key information acquisition:' . $this->pri_key_path);
        }

        $this->private_key = $pri_key;
        return $pri_key;
    }

    /**
     * 获取私钥证书内容
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getCertificate(): string {
        if (!empty($this->certificate)) {
            return $this->certificate;
        }

        if (!file_exists($this->cer_path)) {
            throw new InvalidArgumentException('The certificate file does not exist:' . $this->cer_path);
        }

        $contents = file_get_contents($this->cer_path);
        if ($contents == false || empty($contents)) {
            throw new InvalidArgumentException('Invalid certificate information:' . $this->cer_path);
        }

        $this->certificate = $contents;

        return $contents;
    }

    /**
     * 获取PKCS12证书内容
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getPkcs12(): string {
        if (!empty($this->pkcs12)) {
            return $this->pkcs12;
        }

        if (!file_exists($this->pfx_path)) {
            throw new InvalidArgumentException('Invalid PKCS#12 file:' . $this->pfx_path);
        }

        $contents = file_get_contents($this->pfx_path);
        if ($contents == false) {
            throw new InvalidArgumentException('Invalid PKCS#12 information:' . $this->pfx_path);
        }

        $this->pkcs12 = $contents;

        return $this->pkcs12;
    }

    /**
     * 生成IV随机字符串
     *
     * @param string $cipher
     *
     * @return string
     * @throws \mark\exception\UnexpectedValueException
     */
    public function getRandomString($cipher = 'AES-256-CBC'): string {
        $length = openssl_cipher_iv_length($cipher);     //获取密码初始化向量(iv)长度

        // Use OpenSSL (if available)
        if (function_exists('openssl_random_pseudo_bytes') && $length != false) {
            $bytes = openssl_random_pseudo_bytes($length);   //生成一个伪随机字节串 string ，字节数由 length 参数指定
            if ($bytes === false) {
                throw new UnexpectedValueException('Unable to generate a random string');
            }

            $encode = substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $length);
            if ($encode != false) {
                return $encode;
            }
        }

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuffle = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        if ($shuffle == false) {
            throw new UnexpectedValueException('intercept String failure(substr)');
        }

        return $shuffle;
    }

    /**
     * 对称加密
     *
     * @param string $plaintext  明文
     * @param string $passphrase 密钥
     * @param string $iv         伪随机字节串 | 非 NULL 的初始化向量
     * @param string $cipher     加密方式。{@see openssl_get_cipher_methods()} 可获取有效密码方式列表。
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function encrypt(string $plaintext, string $passphrase, string $iv, $cipher = 'AES-256-CBC'): string {
        //判断传递的加密算法是否在可用的加密算法的列表中
        if (empty($cipher) || !in_array($cipher, openssl_get_cipher_methods())) {
            throw new InvalidArgumentException('This encryption method is not supported:' . $cipher);
        }

        if (empty($iv)) {
            throw new InvalidArgumentException('Using an empty Initialization Vector (iv) is potentially insecure and not recommended');
        }

        $encrypt = openssl_encrypt($plaintext, $cipher, $passphrase, 0, substr($iv, 0, 16));
        if ($encrypt == false) {
            throw new UnexpectedValueException('openssl encrypt failure');
        }

        return $encrypt;
    }

    /**
     * 对称解密
     *
     * @param string $ciphertext 密文
     * @param string $passphrase 密钥
     * @param string $iv         [optional] A non-NULL Initialization Vector.
     * @param string $cipher     加密方式
     *
     * @return string 明文
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function decrypt(string $ciphertext, string $passphrase, string $iv, $cipher = 'AES-256-CBC'): string {
        if (empty($cipher) || !in_array($cipher, openssl_get_cipher_methods())) {
            throw new InvalidArgumentException('This encryption method is not supported:' . $cipher);
        }

        if (empty($iv)) {
            throw new InvalidArgumentException('Using an empty Initialization Vector (iv) is potentially insecure and not recommended');
        }

        $decrypt = openssl_decrypt($ciphertext, $cipher, $passphrase, 0, substr($iv, 0, 16));
        if (!$decrypt) {
            throw new UnexpectedValueException('openssl decrypt failure');
        }

        return $decrypt;
    }

    /**
     * 非对称私钥加密
     *
     * @param string $plaintext
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function private_encrypt(string $plaintext): string {
        if (empty($plaintext)) {
            throw new InvalidArgumentException('Invalid plaintext data');
        }

        $pkey_details = openssl_pkey_get_details($this->get_private_key());
        if ($pkey_details == false) {
            throw new UnexpectedValueException('Failure of public key information acquisition:' . $this->pri_key_path);
        }

        // 校验所需加密数据的长度是否超过密钥所允许的最大值
        $length = strlen($plaintext);
        $allow_length = ($pkey_details['bits'] / 8 - 11);
        if (!empty($pkey_details['bits'] ?? '') && $length > $allow_length) {
            throw new UnexpectedValueException('The data length(' . $length . ') exceeds the maximum allowed by the private key(' . $allow_length . ')');
        }

        $encrypt = openssl_private_encrypt($plaintext, $encrypted, $this->get_private_key());
        if ($encrypt == false) {
            throw new UnexpectedValueException('openssl private encrypt Failure');
        }

        //加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return base64_encode($encrypted ?: '');
    }

    /**
     * 非对称公钥解密
     *
     * @param string $crypted
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function public_decrypt(string $crypted): string {
        if (empty($crypted)) {
            throw new InvalidArgumentException('Invalid encrypt data');
        }

        $decrypt = openssl_public_decrypt(base64_decode($crypted), $decrypted, $this->get_public_key());
        if ($decrypt == false) {
            throw new UnexpectedValueException('openssl public encrypt Failure');
        }

        return $decrypted ?: '';
    }

    /**
     * 非对称公钥加密
     *
     * @param string $plaintext
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function public_encrypt(string $plaintext): string {
        if (empty($plaintext)) {
            throw new InvalidArgumentException('Invalid plaintext data');
        }

        $pkey_details = openssl_pkey_get_details($this->get_public_key());
        if ($pkey_details == false) {
            throw new UnexpectedValueException('Failure of public key information acquisition:' . $this->pub_key_path);
        }

        // 校验加密数据长度是否超过密钥所允许的最大值
        $length = strlen($plaintext);
        $allow_length = $pkey_details['bits'] / 8 - 11;
        if (!empty($pkey_details['bits'] ?? '') && $length > $allow_length) {
            throw new UnexpectedValueException('The data length(' . $length . ') exceeds the maximum allowed by the public key(' . $allow_length . ')');
        }

        $encrypt = openssl_public_encrypt($plaintext, $encrypted, $this->get_public_key());
        if (!$encrypt) {
            throw new UnexpectedValueException('openssl public encrypt failure' . json_encode($pkey_details, JSON_UNESCAPED_UNICODE));
        }

        //加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return base64_encode($encrypted ?: '');
    }

    /**
     * 非对称私钥解密
     *
     * @param string $crypted
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function private_decrypt(string $crypted): string {
        if (empty($crypted)) {
            throw new InvalidArgumentException('Invalid crypted data');
        }

        $decrypt = openssl_private_decrypt(base64_decode($crypted), $decrypted, $this->get_private_key());
        if ($decrypt == false) {
            throw new UnexpectedValueException('openssl private encrypt Failure');
        }

        return $decrypted ?: '';
    }

    /**
     * 使用证书的私钥签名
     *
     * @param array|object|numeric|string $data 原始明文数据
     *
     * @return string 加密信息
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     */
    public function sign($data): string {
        if (empty($data)) {
            throw new InvalidArgumentException('Invalid sign data');
        }

        if (is_string($data)) {
            $value = $data;
        } elseif (is_numeric($data)) {
            $value = $data;
        } elseif (is_array($data)) {
            $value = json_encode($data, JSON_UNESCAPED_UNICODE);
        } elseif (is_object($data)) {
            $value = json_encode($data, JSON_UNESCAPED_UNICODE);
        } else {
            $value = $data;
        }

        if (empty($value)) {
            throw new InvalidArgumentException('Invalid sign data');
        }

        // 私钥加密
        $pkcs12 = openssl_pkcs12_read($this->getPkcs12(), $certs, $this->passphrase ?: '');     //读取公钥、私钥
        if ($pkcs12 == false || empty($certs['pkey'] ?? '')) {
            throw new UnexpectedValueException('Failure of Public Key Cryptography Standards #12:' . $this->pfx_path);
        }

        $result = openssl_sign((string)$value, $sign, $certs['pkey'], OPENSSL_ALGO_SHA1);    //注册生成加密信息
        if ($result == false) {
            throw new UnexpectedValueException('openssl sign Failure');
        }

        //base64转码加密信息 加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return base64_encode($sign ?: '');
    }

    /**
     * 使用证书的公钥验证签名
     *
     * @param string $data 明文
     * @param string $sign 加密信息`
     *
     * @return bool 是否验证通过
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     */
    public function verify(string $data, string $sign): bool {
        if (empty($data)) {
            throw new InvalidArgumentException('Invalid verify data');
        }

        if (empty($sign)) {
            throw new InvalidArgumentException('Invalid sign msg');
        }

        // 读取公钥、私钥
        $pkcs12 = openssl_pkcs12_read($this->getPkcs12(), $certs, $this->passphrase ?: '');
        if ($pkcs12 == false || empty($certs['cert'] ?? '')) {
            throw new UnexpectedValueException('Failure of Public Key Cryptography Standards #12:' . $this->pfx_path);
        }

        // 公钥解密 * 验证,数据与签名是否一致：输出验证结果，1：验证成功，0：验证失败
        return openssl_verify($data, base64_decode($sign), $certs['cert']) == 1;
    }

    /**
     * 魔术方法之为私有成员属性设置值
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value) {
        $this->$name = $value;
    }
}