<?php
declare (strict_types=1);

namespace mark\exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class FileException
 *
 * @description  文件异常类
 * @package      mark\exception
 */
class FileException extends \RuntimeException implements NotFoundExceptionInterface { }