<?php

namespace mark\exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class FunctionNotFoundException
 *
 * @description 未找到方法异常
 * @package     mark\exception
 */
class FunctionNotFoundException extends \RuntimeException implements NotFoundExceptionInterface {
    /**
     * @var string
     */
    protected $function;

    public function __construct(string $message, string $function = '', \Throwable $previous = null) {
        $this->function = $function;

        parent::__construct($message, 500, $previous);
    }

    /**
     * 获取方法名
     *
     * @return string
     */
    public function getFunction(): string {
        return $this->function;
    }
}