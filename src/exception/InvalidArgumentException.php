<?php
declare (strict_types=1);

namespace mark\exception;

use Psr\Cache\InvalidArgumentException as Psr6CacheInvalidArgumentInterface;
use Psr\SimpleCache\InvalidArgumentException as SimpleCacheInvalidArgumentInterface;

/**
 * Class InvalidArgumentException
 * One or more invalid arguments were passed to a method.
 *
 * @description 无效参数异常
 * @package     mark\exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements Psr6CacheInvalidArgumentInterface, SimpleCacheInvalidArgumentInterface, \mark\http\Exception {

    /**
     * InvalidArgumentException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = 412, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}