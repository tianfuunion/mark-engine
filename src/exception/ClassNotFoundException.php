<?php
declare (strict_types=1);

namespace mark\exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class ClassNotFoundException
 *
 * @description 未找到类异常
 * @package     mark\exception
 */
class ClassNotFoundException extends \RuntimeException implements NotFoundExceptionInterface {
    /**
     * @var string
     */
    protected $class;

    public function __construct(string $message, string $class = '', \Throwable $previous = null) {
        $this->class = $class;

        parent::__construct($message, 500, $previous);
    }

    /**
     * 获取类名
     *
     * @access public
     * @return string
     */
    public function getClass(): string {
        return $this->class;
    }
}