<?php
declare (strict_types=1);

namespace mark\exception;

/**
 * Class ValidateException
 *
 * @description 数据验证异常
 * @package     mark\exception
 */
class ValidateException extends \RuntimeException {
    protected $error;

    /**
     * ValidateException constructor.
     *
     * @param $error
     */
    public function __construct($error) {
        $this->error = $error;
        $this->message = is_array($error) ? implode(PHP_EOL, $error) : $error;

        parent::__construct($this->message, 510);
    }

    /**
     * 获取验证错误信息
     *
     * @access public
     * @return array|string
     */
    public function getError() {
        return $this->error;
    }
}