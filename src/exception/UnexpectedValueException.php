<?php
declare (strict_types=1);

namespace mark\exception;

/**
 * Class UnexpectedValueException
 *
 * @description 意外值异常
 * @package     mark\exception
 */
class UnexpectedValueException extends \LogicException implements HttpException {

    /**
     * UnexpectedValueException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = 409, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}