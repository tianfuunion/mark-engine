<?php
declare (strict_types=1);

namespace mark\exception;

/**
 * Class LogicException
 *
 * @package mark\exception
 */
class LogicException extends \LogicException implements ExceptionInterface { }