<?php
declare (strict_types=1);

namespace mark\exception;

/**
 * Interface HttpException
 *
 * @package mark\exception
 * @method string getMessage()
 * @method \Throwable|null getPrevious()
 * @method mixed getCode()
 * @method string getFile()
 * @method int getLine()
 * @method array getTrace()
 * @method string getTraceAsString()
 * @since   7.0
 */
interface HttpException extends \Throwable { }