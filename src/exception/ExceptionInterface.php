<?php
declare (strict_types=1);

namespace mark\exception;

/**
 * Interface ExceptionInterface
 *
 * @package mark\exception
 */
interface ExceptionInterface extends \Throwable { }