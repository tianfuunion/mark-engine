<?php
declare (strict_types=1);

namespace mark\system;

/**+==================================================
 * |  名称：MarkEngine
 * +--------------------------------------------------
 * |  文件：Operating System php
 * +--------------------------------------------------
 * |  概要: 操作系统浏览器信息管理类.
 * +--------------------------------------------------
 * |  版权：Copyright (c) 2017~2022 https://open.tianfu.ink All rights reserved.
 * +--------------------------------------------------
 * |  许可：Licensed ( https://license.coscl.org.cn/MulanPSL2 )
 * +--------------------------------------------------
 * |  作者：Author: Mark <mark@tianfuunion.cn>
 * +--------------------------------------------------
 * |  创建时间: 2018-12-12
 * +--------------------------------------------------
 * |  修改时间: 2022-04-22 20:00:00
 * +--------------------------------------------------
 * Class Os
 *
 * @author  Mark<mark@tianfuunion.cn>
 * @package mark\system
 **+==================================================*/
final class Os {
    private function __construct() { }

    /**
     * 获取HTTP代理
     *
     * @param bool $origin
     *
     * @return string
     * @see \mark\system\Os::getUserAgent()
     */
    public static function getAgent(bool $origin = true): string {
        return self::getUserAgent($origin);
    }

    /**
     * 获取HTTP代理
     *
     * @param bool $origin
     *
     * @return string
     */
    public static function getUserAgent(bool $origin = true): string {
        if (isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT']) && $_SERVER['HTTP_USER_AGENT'] != '') {
            return $origin ? $_SERVER['HTTP_USER_AGENT'] : strtolower($_SERVER['HTTP_USER_AGENT']);
        }

        if (isset($_SERVER['HTTP_X_UCBROWSER_UA']) && !empty($_SERVER['HTTP_X_UCBROWSER_UA']) && $_SERVER['HTTP_X_UCBROWSER_UA'] != '') {
            return $origin ? $_SERVER['HTTP_X_UCBROWSER_UA'] : strtolower($_SERVER['HTTP_X_UCBROWSER_UA']);
        }

        return '';
    }

    /**
     * HTTP请求头
     *
     * @param bool $origin
     *
     * @return string
     */
    public static function getAccept(bool $origin = true): string {
        if (isset($_SERVER['HTTP_ACCEPT']) && !empty($_SERVER['HTTP_ACCEPT'])) {
            return $origin ? $_SERVER['HTTP_ACCEPT'] : strtolower($_SERVER['HTTP_ACCEPT']);
        }

        return $_SERVER['ACCEPT'] ?? '';
    }

    /**
     * 获取HTTP代理
     *
     * @param bool $origin
     *
     * @return string
     * @see Os::getUserAgent()
     */
    public static function getProxy(bool $origin = true): string {
        return self::getUserAgent($origin);
    }

    /**
     * 获取客户端操作系统信息包括win10
     *
     * @link    https://docs.microsoft.com/zh-cn/windows/win32/sysinfo/operating-system-version?redirectedfrom=MSDN
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return array
     */
    public static function getOs(string $agent = '', bool $origin = true): array {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        $os = 'Unknown';
        $version = 0;

        if (isset($_SERVER['HTTP_X_UCBROWSER_UA']) && !empty($_SERVER['HTTP_X_UCBROWSER_UA'])) {
            //获取UC用户代理字符串
            $vers = explode(';', $_SERVER['HTTP_X_UCBROWSER_UA']);
            $os = trim($vers[2], '\ov( | )');
            $version = trim($vers[2], '\ov( | ))');

            $ver = explode(' ', $vers[2]);
            if ($ver != false) {
                $os = trim($ver[0], '\ov( | )');
                $version = trim($ver[1], '\ov( | )');
            }
        } elseif (false !== stripos($agent, 'Android')) {
            $os = 'Android';
            preg_match('/Android ([\d.]+)/i', $agent, $matches);
            $version = $matches[1] ?? 0;
        } elseif (false !== stripos($agent, 'iphone')) {
            preg_match('/iphone os [\w]*/i', $agent, $matches);
            $os = 'iPhone';
            $version = preg_replace('/iphone os +/i', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'ipad')) {
            preg_match('/cpu os [\d_]+/', $agent, $matches);
            $os = 'iPad';
            $version = preg_replace('/cpu os +/i', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'win') || false !== stripos($agent, 'Windows')) {
            $os = 'Windows';
            if (preg_match('/nt 10.0/i', $agent)) {
                $version = '10';
            } elseif (preg_match('/nt 6.2/i', $agent)) {
                $version = '8';
            } elseif (preg_match('/nt 6.1/i', $agent)) {
                $version = '7';
            } elseif (preg_match('/nt 6.0/i', $agent)) {
                $version = 'Vista';
            } elseif (preg_match('/nt 5.1/i', $agent)) {
                $version = 'XP';
            } elseif (preg_match('/nt 5/i', $agent)) {
                $version = '2000';
            } elseif (false !== strpos($agent, '98')) {
                $version = '98';
            } elseif (strpos($agent, '95')) {
                $version = '95';
            } elseif (preg_match('/win 9x/i', $agent) || strpos($agent, '4.90')) {
                $version = 'ME';
            } elseif (false !== stripos($agent, 'nt')) {
                $version = 'NT';
            } elseif (false !== strpos($agent, '32')) {
                $version = '32';
            } elseif (false !== strpos($agent, '64')) {
                $version = '64';
            }
        } elseif (false !== stripos($agent, 'Ubuntu')) {
            $os = 'Ubuntu Linux';
            if (false !== stripos($agent, 'x11')) {
                $version = 'X11';
            } elseif (false !== stripos($agent, 'x11')) {
                $version = 'X12';
            }
        } elseif (false !== stripos($agent, 'uos')) {
            $os = 'Linux UOS';
            if (false !== stripos($agent, 'x11')) {
                $version = 'X11';
            } elseif (false !== stripos($agent, 'x12')) {
                $version = 'X12';
            }
        } elseif (false !== stripos($agent, 'linux')) {
            $os = 'Linux';
            if (false !== stripos($agent, 'x11')) {
                $version = 'X11';
            } elseif (false !== stripos($agent, 'x12')) {
                $version = 'X12';
            }
        } elseif (false !== stripos($agent, 'unix')) {
            $os = 'Unix';
        } elseif (false !== stripos($agent, 'sun') && false !== stripos($agent, 'os')) {
            $os = 'SunOS';
            preg_match('/Os [.|\s\w]*/i', $agent, $matches);
            $model = explode(' ', $matches[0] ?? '');
            if (!empty($model) && $model != false) {
                $version = end($model);
            }
        } elseif (false !== stripos($agent, 'ibm') && false !== stripos($agent, 'os')) {
            $os = 'IBM OS/2';
            preg_match('/Os [.|\s\w]*/i', $agent, $matches);
            $model = explode(' ', $matches[0] ?? '');
            if (!empty($model) && $model != false) {
                $version = end($model);
            }
        } elseif (false !== stripos($agent, 'Macintosh')) {
            $os = 'Macintosh';
            preg_match('/Os [.|\s\w]*/i', $agent, $matches);
            $model = explode(' ', $matches[0] ?? '');
            if (!empty($model) && $model != false) {
                $version = end($model);
            }
        } elseif (false !== stripos($agent, 'mac') && false !== stripos($agent, 'os')) {
            $os = 'MAC';
            preg_match('/Os [.|\s\w]*/i', $agent, $matches);
            $model = explode(' ', $matches[0] ?? '');
            if (!empty($model) && $model != false) {
                $version = end($model);
            }
        } elseif (false !== stripos($agent, 'powerpc')) {
            $os = 'PowerPC';
        } elseif (false !== stripos($agent, 'aix')) {
            $os = 'AIX';
        } elseif (false !== stripos($agent, 'hpux')) {
            $os = 'HPUX';
        } elseif (false !== stripos($agent, 'netbsd')) {
            $os = 'NetBSD';
        } elseif (false !== stripos($agent, 'bsd')) {
            $os = 'BSD';
        } elseif (false !== stripos($agent, 'osf1')) {
            $os = 'OSF1';
        } elseif (false !== stripos($agent, 'irix')) {
            $os = 'IRIX';
        } elseif (false !== stripos($agent, 'freebsd')) {
            $os = 'FreeBSD';
        } elseif (false !== stripos($agent, 'teleport')) {
            $os = 'teleport';
        } elseif (false !== stripos($agent, 'flashget')) {
            $os = 'flashget';
        } elseif (false !== stripos($agent, 'webzip')) {
            $os = 'webzip';
        } elseif (false !== stripos($agent, 'compatible')) {
            $os = 'Compatible';
        } elseif (false !== stripos($agent, 'offline')) {
            $os = 'offline';
        }

        if (is_numeric($version)) {
            $str_version = $version;
        } else {
            $str_version = str_ireplace(['_', '-'], '.', $version);
        }

        return array('os' => $origin ? $os : strtolower($os), 'version' => $origin ? $str_version : strtolower($str_version));
    }

    /**
     * 获取设备品牌型号
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return array
     */
    public static function getBrand(string $agent = '', bool $origin = true): array {
        $agent = $agent ?: self::getUserAgent($origin);
        $brand = self::isMobile($agent) ? '手机' : '计算机';
        $model = '';

        if (isset($_SERVER['HTTP_X_UCBROWSER_UA']) && !empty($_SERVER['HTTP_X_UCBROWSER_UA'])) {
            $vers = explode(';', $_SERVER['HTTP_X_UCBROWSER_UA']);
            $model = trim($vers[0], '\dv(|)');//品牌
        } elseif (false !== stripos($agent, 'iphone')) {
            $brand = 'iPhone';
            preg_match('/iphone os [\w]*/i', $agent, $matches);
            $model = str_ireplace('iphone os', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'ipad')) {
            $brand = 'iPad';
            preg_match('/cpu os [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (
            false !== stripos($agent, 'samsung') ||
            false !== stripos($agent, 'galaxy') ||
            false !== stripos($agent, 'gt-') ||
            false !== stripos($agent, 'sch-') ||
            false !== stripos($agent, 'sm-')) {
            $brand = '三星';
            preg_match('/samsung [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'huawei') || false !== stripos($agent, 'honor')) {
            if (false !== stripos($agent, 'huawei')) {
                $brand = '华为';
                preg_match('/huawei[\w|-]*/i', $agent, $matches);
                $model = str_ireplace('huawei', '', $matches[0] ?? '');
            }
            if (false !== stripos($agent, 'honor')) {
                $brand = '荣耀';
                preg_match('/honor[\w|-]*/i', $agent, $matches);
                $model = str_ireplace('honor', '', $matches[0] ?? '');
            }

            switch (strtolower($model)) {
                case 'gra-ul10':
                    $model = 'P8';
                    break;
                case 'ale-cl00':
                    $model = 'P8 青春版';
                    break;
                case 'ale-tl00':
                    $model = 'P8 lite';
                    break;
                case 'eva-al10':
                    $model = 'P9';
                    break;
                case 'vie-al10':
                    $model = 'P9 Plus';
                    break;
                case 'vtr-al00':
                    $model = 'P10';
                    break;
                case 'vky-al00':
                    $model = 'P10 Plus';
                    break;
                case 'eml-al00':
                    $model = 'P20';
                    break;
                case 'clt-al00':
                    $model = 'P20 Pro';
                    break;
                case 'ele-al00':
                    $model = 'P30';
                    break;
                case 'lio-al00':
                case 'lio-tl00':
                    $model = 'P30 Pro';
                    break;
                case 'lio-an00':
                    $model = 'P30 Pro 5G';
                    break;
                case 'vog-al00':
                    $model = 'P30 Pro 标配版';
                    break;
                case 'vog-al10':
                    $model = 'P30 Pro 尊享版';
                    break;
                case 'lio-an00p':
                    $model = 'P30 RS保时捷';
                    break;
                case 'els-an00':
                case 'els-tn00':
                    $model = 'P40';
                    break;
                case 'abr-al00':
                case 'abr-al80':
                    $model = 'P50';
                    break;
                case 'mt7-tl10':
                case 'mt7-cl00':
                case 'mt7-al00':
                    $model = 'Mate 7';
                    break;
                case 'nxt-tl00':
                case 'nxt-ql10':
                    $model = 'Mate 8';
                    break;
                case 'mha-al00':
                    $model = 'Mate 9';
                    break;
                case 'crr-cl00':
                case 'crr-ul00':
                case 'crr-tl00':
                    $model = 'Mate S';
                    break;
                case 'alp-al00':
                    $model = 'Mate 10';
                    break;
                case 'bla-al00':
                    $model = 'Mate 10 Pro';
                    break;
                case 'hma-al00':
                    $model = 'Mate 20';
                    break;
                case 'evr-l29':
                case 'evr-tl00':
                case 'evr-al00':
                    $model = 'Mate 20 X';
                    break;
                case 'lya-al00':
                    $model = 'Mate 20 Pro';
                    break;
                case 'tas-an00':
                case 'tas-al00':
                    $model = 'Mate 30';
                    break;
                case 'lio-an00m':
                    $model = 'Mate 30 SE';
                    break;
                case 'oce-an00':
                    $model = 'Mate 40';
                    break;
                case 'oce-an50':
                    $model = 'Mate 40 E';
                    break;
                case 'noh-an00':
                case 'noh-an50':
                    $model = 'Mate 40 Pro';
                    break;
                case 'noh-an01':
                    $model = 'Mate 40 Pro 5G';
                    break;
                case 'nop-an00':
                    $model = 'Mate 40 Pro+';
                    break;
                case 'bne-al00':
                case 'cet-al00':
                case 'cet-al60':
                    $model = 'Mate 50';
                    break;
                case 'dco-al00':
                    $model = 'Mate 50 Pro';
                    break;
                case 'aln-al00':
                case 'aln-al80':
                    $model = 'Mate 60 Pro';
                    break;
                case 'dby-w09':
                    $brand = '华为';
                    $model = 'MatePad C7';
                    break;
                case 'art-al00':
                case 'art-al00x':
                case 'art-tl00':
                    $brand = '华为';
                    $model = 'Enjoy 10';
                    break;
                case 'pic-al00':
                    $model = 'Nova 2';
                    break;
                case 'hwi-al00':
                case 'hwi-tl00':
                    $model = 'Nova 2S';
                    break;
                case 'bac-al00':
                    $model = 'Nova 2 Plus';
                    break;
                case 'par-al00':
                    $model = 'Nova 3';
                    break;
                case 'vce-al00':
                    $model = 'Nova 4';
                    break;
                case 'sea-al10':
                    $model = 'Nova 5 Pro';
                    break;
                case 'glk-al00':
                case 'glk-tl00':
                    $model = 'Nova 5i';
                    break;
                case 'spn-al00':
                    $model = 'Nova 5i Pro';
                    break;
                case 'jny-al10':
                    $model = 'Nova 6 SE';
                    break;
                case 'wlz-an00':
                    $model = 'Nova 6 5G';
                    break;
                case 'jef-an00':
                    $model = 'Nova 7';
                    break;
                case 'cdy-an00':
                case 'cdy-tn20':
                    $model = 'Nova 7 SE';
                    break;
                case 'jef-an20':
                    $model = 'Nova 7 5G';
                    break;
                case 'jer-an10':
                    $model = 'Nova 7 Pro';
                    break;
                case 'ang-an00':
                    $model = 'Nova 8';
                    break;
                case 'rte-al00':
                case 'rna-al00':
                    $model = 'Nova 9 Pro';
                    break;
                case 'nco-al00':
                    $model = 'Nova 10';
                    break;
                case 'foa-al00':
                    $model = 'Nova 11';
                    break;
                case 'rio-ul00':
                    $model = 'G7 Plus';
                    break;
                case 'vns-dl00':
                case 'vns-al00':
                    $model = 'G9 青春版';
                    break;
                case 'uk-al20':
                    $model = 'V9d';
                    break;
                case 'oxf-an10':
                    $model = 'V30 Pro';
                    break;
                case 'chm-tl00h':
                case 'chm-cl00':
                    $model = '畅玩4C';
                    break;
                case 'che2-ul00':
                case 'che1-cl20':
                    $model = '畅玩4X';
                    break;
                case 'cun-al00':
                    $model = '畅玩5';
                    break;
                case 'cam-al00':
                    $model = '畅玩5A';
                    break;
                case 'dli-al10':
                    $model = '畅玩6A';
                    break;
                case 'bln-al40':
                    $model = '畅玩6X';
                    break;
                case 'tag-al00':
                case 'tag-ul00':
                case 'tag-tl00':
                    $model = '畅享5S';
                    break;
                case 'nce-al00':
                case 'nce-al10':
                    $model = '畅享6';
                    break;
                case 'dig-tl10':
                case 'dig-al00':
                    $model = '畅享6S';
                    break;
                case 'sla-al00':
                    $model = '畅享7';
                    break;
                case 'trt-al10':
                    $model = '畅享7 Plus';
                    break;
                case 'aqm-al00':
                    $model = '畅享10S';
                    break;
                case 'stk-al00':
                    $model = '畅享10 Plus';
                    break;
                case 'rio-al00':
                    $model = '麦芒4';
                    break;
                case 'mla-tl00':
                case 'mla-al10':
                    $model = '麦芒5';
                    break;
                case 'rne-al00':
                    $model = '麦芒6';
                    break;
                case 'sne-al00':
                    $model = '麦芒7';
                    break;
                case 'h30-t00':
                    $brand = '荣耀';
                    $model = '3C';
                    break;
                case 'scl-tl00':
                case 'scl-al00':
                case 'scl-cl00':
                    $brand = '荣耀';
                    $model = '4A';
                    break;
                case 'che-tl00h':
                    $brand = '荣耀';
                    $model = '4X';
                    break;
                case 'kiw-al10':
                case 'kiw-ul00':
                case 'kiw-al20':
                    $brand = '荣耀';
                    $model = '5X';
                    break;
                case 'h60-l03':
                    $brand = '荣耀';
                    $model = '6';
                    break;
                case 'pe-tl00m':
                case 'pe-tl20':
                    $brand = '荣耀';
                    $model = '6 Plus';
                    break;
                case 'plk-al10':
                    $brand = '荣耀';
                    $model = '7';
                    break;
                case 'ath-al00':
                    $brand = '荣耀';
                    $model = '7I';
                    break;
                case 'frd-al00':
                    $brand = '荣耀';
                    $model = '8';
                    break;
                case 'pra-al00x':
                    $brand = '荣耀';
                    $model = '8 青春版';
                    break;
                case 'edi-al10':
                    $brand = '荣耀';
                    $model = 'Note 8';
                    break;
                case 'knt-al10':
                    $brand = '荣耀';
                    $model = 'V8';
                    break;
                case 'hlk-al00':
                    $brand = '荣耀';
                    $model = '9X';
                    break;
                case 'hlk-al10':
                case 'hlk-l41':
                case 'hlk-l42':
                    $brand = '荣耀';
                    $model = '9X Pro';
                    break;
                case 'col-al10':
                    $brand = '荣耀';
                    $model = '10';
                    break;
                case 'bkl-al20':
                    $brand = '荣耀';
                    $model = 'V10';
                    break;
                case 'pct-al10':
                    $brand = '荣耀';
                    $model = 'V20';
                    break;
                case 'tel-an00a':
                case 'tel-an00':
                case 'tel-tn00':
                    $brand = '荣耀';
                    $model = 'X10';
                    break;
                case 'chl-an00':
                    $brand = '荣耀';
                    $model = 'X20 SE';
                    break;
                case 'yal-al00':
                case 'yal-tl00':
                case 'yal-l21':
                    $brand = '荣耀';
                    $model = '20';
                    break;
                case 'hry-al00ta':
                    $brand = '荣耀';
                    $model = '20i';
                    break;
                case 'yal-al10':
                    $brand = '荣耀';
                    $model = '20 Pro';
                    break;
                case 'cdy-an90':
                    $brand = '荣耀';
                    $model = '30 S';
                    break;
                case 'any-an00':
                    $brand = '荣耀';
                    $model = 'X30';
                    break;
                case 'tfy-an00':
                    $brand = '荣耀';
                    $model = 'X30i';
                    break;
                case 'rmo-an00':
                    $brand = '荣耀';
                    $model = 'X40';
                    break;
                case 'adt-an00':
                    $brand = '荣耀';
                    $model = 'X40 GT';
                    break;
                case 'dio-an00':
                case 'dio-tn00':
                    $brand = '荣耀';
                    $model = 'X40i';
                    break;
                case 'nth-nx9':
                case 'nth-an00':
                    $brand = '荣耀';
                    $model = '50';
                    break;
                case 'rna-an00':
                    $brand = '荣耀';
                    $model = '50 Pro';
                    break;
                case 'jlh-an00':
                    $brand = '荣耀';
                    $model = '50 SE';
                    break;
                case 'lsa-an00':
                    $brand = '荣耀';
                    $model = '60 Pro';
                    break;
                case 'fne-an00':
                    $brand = '荣耀';
                    $model = '70';
                    break;
                case 'rea-nx9':
                case 'rea-an00':
                    $brand = '荣耀';
                    $model = '90';
                    break;
                case 'ask-al00x':
                    $brand = '荣耀';
                    $model = 'Play 3 全网通';
                    break;
                case 'tnnh-an00':
                    $brand = '荣耀';
                    $model = 'Play 4';
                    break;
                case 'aka-al10':
                    $brand = '荣耀';
                    $model = 'Play 4T';
                    break;
                case 'aqm-al10':
                    $brand = '荣耀';
                    $model = 'Play 4T Pro';
                    break;
                case 'new-an90':
                    $brand = '荣耀';
                    $model = 'Play 5 活力版';
                    break;
                case 'rky-an10':
                    $brand = '荣耀';
                    $model = 'Play 7t';
                    break;
                case 'koz-al00':
                    $brand = '荣耀';
                    $model = 'Play 20';
                    break;

                case 'elz-an00':
                    $brand = '荣耀';
                    $model = 'Magic3';
                    break;
                case 'lge-an00':
                    $brand = '荣耀';
                    $model = 'Magic4';
                    break;
                default:
                    break;
            }
        } elseif (false !== stripos($agent, 'hera-bd00')) {
            $brand = '华为智选';
            $model = 'Hi Nova 9';
        } elseif (false !== stripos($agent, 'heba-bd00')) {
            $brand = '华为智选';
            $model = 'Hi Nova 9 Pro';
        } elseif (false !== stripos($agent, 'sp100')) {
            $brand = '华为';
            $model = 'N ZONE S7';
        } elseif (
            false !== stripos($agent, 'oppo') || false !== stripos($agent, 'x9007') || false !== stripos($agent, 'x907') ||
            false !== stripos($agent, 'x909') || false !== stripos($agent, 'r831s') || false !== stripos($agent, 'r827t') ||
            false !== stripos($agent, 'r821t') || false !== stripos($agent, 'r811') || false !== stripos($agent, 'r2017') ||
            false !== stripos($agent, 'r11')) {
            $brand = 'OPPO';
            preg_match('/oppo [\w]*/i', $agent, $matches);
            $model = str_ireplace('oppo', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'pckm00')) {
            $brand = 'OPPO';
            $model = 'Reno 2';
        } elseif (false !== stripos($agent, 'pclm50')) {
            $brand = 'OPPO';
            $model = 'Reno 3';
        } elseif (false !== stripos($agent, 'pcrm00')) {
            $brand = 'OPPO';
            $model = 'Reno 3 Pro';
        } elseif (false !== stripos($agent, 'pdpm00') || false !== stripos($agent, 'pdpt00')) {
            $brand = 'OPPO';
            $model = 'Reno 4';
        } elseif (false !== stripos($agent, 'peam00') || false !== stripos($agent, 'peat00')) {
            $brand = 'OPPO';
            $model = 'Reno 4 SE';
        } elseif (false !== stripos($agent, 'pegt00') || false !== stripos($agent, 'pegm00') || false !== stripos($agent, 'paam00')) {
            $brand = 'OPPO';
            $model = 'Reno 5';
        } elseif (false !== stripos($agent, 'peqm00')) {
            $brand = 'OPPO';
            $model = 'Reno 6';
        } elseif (false !== stripos($agent, 'pepm00') || false !== stripos($agent, 'cph2249')) {
            $brand = 'OPPO';
            $model = 'Reno 6 Pro 5G';
        } elseif (false !== stripos($agent, 'pfjm10')) {
            $brand = 'OPPO';
            $model = 'Reno 7';
        } elseif (false !== stripos($agent, 'pfdm00') || false !== stripos($agent, 'cph2293')) {
            $brand = 'OPPO';
            $model = 'Reno 7 Pro';
        } elseif (false !== stripos($agent, 'pgbm10')) {
            $brand = 'OPPO';
            $model = 'Reno 8';
        } elseif (false !== stripos($agent, 'pfzm10')) {
            $brand = 'OPPO';
            $model = 'Reno 8 Pro';
        } elseif (false !== stripos($agent, 'phm110')) {
            $brand = 'OPPO';
            $model = 'Reno 9';
        } elseif (false !== stripos($agent, 'pgx110')) {
            $brand = 'OPPO';
            $model = 'Reno 9 Pro';
        } elseif (false !== stripos($agent, 'pgw110')) {
            $brand = 'OPPO';
            $model = 'Reno 9 Pro+';
        } elseif (false !== stripos($agent, 'phu110') || false !== stripos($agent, 'phv110')) {
            $brand = 'OPPO';
            $model = 'Reno 10 Pro+';
        } elseif (false !== stripos($agent, 'pjh110')) {
            $brand = 'OPPO';
            $model = 'Reno 11';
        } elseif (false !== stripos($agent, 'pacm00')) {
            $brand = 'OPPO';
            $model = 'R15';
        } elseif (false !== stripos($agent, 'pbcm10')) {
            $brand = 'OPPO';
            $model = 'R15x';
        } elseif (false !== stripos($agent, 'pbem00')) {
            $brand = 'OPPO';
            $model = 'R17';
        } elseif (false !== stripos($agent, 'pdem10')) {
            $brand = 'OPPO';
            $model = 'Find X2';
        } elseif (false !== stripos($agent, 'pedm00')) {
            $brand = 'OPPO';
            $model = 'Find X3';
        } elseif (false !== stripos($agent, 'pffm10') || false !== stripos($agent, 'cph2307')) {
            $brand = 'OPPO';
            $model = 'Find X5';
        } elseif (false !== stripos($agent, 'pbbm30')) {
            $brand = 'OPPO';
            $model = 'A5 全网通';
        } elseif (false !== stripos($agent, 'pbam00')) {
            $brand = 'OPPO';
            $model = 'A5 电信版';
        } elseif (false !== stripos($agent, 'pbat00')) {
            $brand = 'OPPO';
            $model = 'A5 移动版4G+';
        } elseif (false !== stripos($agent, 'pbbt30')) {
            $brand = 'OPPO';
            $model = 'A5 移动版';
        } elseif (false !== stripos($agent, 'pchm30')) {
            $brand = 'OPPO';
            $model = 'A11x';
        } elseif (false !== stripos($agent, 'pdvm00')) {
            $brand = 'OPPO';
            $model = 'A32';
        } elseif (false !== stripos($agent, 'pefm00') || false !== stripos($agent, 'pehm00')) {
            $brand = 'OPPO';
            $model = 'A35';
        } elseif (false !== stripos($agent, 'pemm00')) {
            $brand = 'OPPO';
            $model = 'A55';
        } elseif (false !== stripos($agent, 'pfvm10')) {
            $brand = 'OPPO';
            $model = 'A56';
        } elseif (false !== stripos($agent, 'pftm20')) {
            $brand = 'OPPO';
            $model = 'A57 5G';
        } elseif (false !== stripos($agent, 'pdym20')) {
            $brand = 'OPPO';
            $model = 'A72';
        } elseif (false !== stripos($agent, 'pcpm00')) {
            $brand = 'OPPO';
            $model = 'A91';
        } elseif (false !== stripos($agent, 'pdkm00')) {
            $brand = 'OPPO';
            $model = 'A92 S';
        } elseif (false !== stripos($agent, 'pehm00')) {
            $brand = 'OPPO';
            $model = 'A93';
        } elseif (false !== stripos($agent, 'pfgm00')) {
            $brand = 'OPPO';
            $model = 'A93 S';
        } elseif (false !== stripos($agent, 'pftm10')) {
            $brand = 'OPPO';
            $model = 'A97 5G';
        } elseif (false !== stripos($agent, 'pcnm00')) {
            $brand = 'OPPO';
            $model = 'K5';
        } elseif (false !== stripos($agent, 'pexm00')) {
            $brand = 'OPPO';
            $model = 'K9 5G';
        } elseif (false !== stripos($agent, 'perm10')) {
            $brand = 'OPPO';
            $model = 'K9 S';
        } elseif (false !== stripos($agent, 'peym00')) {
            $brand = 'OPPO';
            $model = 'K9 Pro';
        } elseif (false !== stripos($agent, 'v1831a') || false !== stripos($agent, 'v1831t')) {
            $brand = 'VIVO';
            $model = 'S1';
        } elseif (false !== stripos($agent, 'v1962a') || false !== stripos($agent, 'v1962ba')) {
            $brand = 'VIVO';
            $model = 'S6 5G';
        } elseif (false !== stripos($agent, 'v2072a')) {
            $brand = 'VIVO';
            $model = 'S9';
        } elseif (false !== stripos($agent, 'v2207a')) {
            $brand = 'VIVO';
            $model = 'S15 Pro';
        } elseif (false !== stripos($agent, 'v1838a') || false !== stripos($agent, 'v1838t') || false !== stripos($agent, 'v1829a') || false !== stripos($agent, 'v1829t')) {
            $brand = 'VIVO';
            $model = 'X27';
        } elseif (false !== stripos($agent, 'v1836a') || false !== stripos($agent, 'v1836t')) {
            $brand = 'VIVO';
            $model = 'X27 Pro';
        } elseif (false !== stripos($agent, 'v2001a')) {
            $brand = 'VIVO';
            $model = 'X50';
        } elseif (false !== stripos($agent, 'v2046a')) {
            $brand = 'VIVO';
            $model = 'X60';
        } elseif (false !== stripos($agent, 'v2133a')) {
            $brand = 'VIVO';
            $model = 'X70';
        } elseif (false !== stripos($agent, 'v2183a')) {
            $brand = 'VIVO';
            $model = 'X80';
        } elseif (false !== stripos($agent, 'v2309a') || false !== stripos($agent, 'v2308')) {
            $brand = 'VIVO';
            $model = 'X100';
        } elseif (false !== stripos($agent, 'v1945a') || false !== stripos($agent, 'v1945t')) {
            $brand = 'VIVO';
            $model = 'Y9 S';
        } elseif (false !== stripos($agent, 'v2036a')) {
            $brand = 'VIVO';
            $model = 'Y30';
        } elseif (false !== stripos($agent, 'v2068a')) {
            $brand = 'VIVO';
            $model = 'Y31 S';
        } elseif (false !== stripos($agent, 'v1934a') || false !== stripos($agent, 'v1934t')) {
            $brand = 'VIVO';
            $model = 'Y5 S';
        } elseif (false !== stripos($agent, 'v2002a')) {
            $brand = 'VIVO';
            $model = 'Y70 S';
        } elseif (false !== stripos($agent, 'V2156A')) {
            $brand = 'VIVO';
            $model = 'Y76 S';
        } elseif (false !== stripos($agent, 'v2166ba')) {
            $brand = 'VIVO';
            $model = 'Y77 E';
        } elseif (false !== stripos($agent, 'v2219a')) {
            $brand = 'VIVO';
            $model = 'Y77';
        } elseif (false !== stripos($agent, 'v1818a')) {
            $brand = 'VIVO';
            $model = 'Y93 全网通版';
        } elseif (false !== stripos($agent, 'v1818t')) {
            $brand = 'VIVO';
            $model = 'Y93 移动全网通版';
        } elseif (false !== stripos($agent, 'v1813a')) {
            $brand = 'VIVO';
            $model = 'Y97 全网通版';
        } elseif (false !== stripos($agent, 'v1813t')) {
            $brand = 'VIVO';
            $model = 'Y97 移动全网通版';
        } elseif (false !== stripos($agent, 'v2055a')) {
            $brand = 'VIVO';
            $model = 'iQOO Neo5';
        } elseif (false !== stripos($agent, 'v2231a')) {
            $brand = 'VIVO';
            $model = 'iQOO Neo7';
        } elseif (false !== stripos($agent, 'v2232a')) {
            $brand = 'VIVO';
            $model = 'iQOO Neo7 Racing Edition';
        } elseif (false !== stripos($agent, 'v2301a')) {
            $brand = 'VIVO';
            $model = 'iQOO Neo8';
        } elseif (false !== stripos($agent, 'v2012a')) {
            $brand = 'VIVO';
            $model = 'iQOO Z1x';
        } elseif (false !== stripos($agent, 'v2018')) {
            $brand = 'VIVO';
            $model = 'iQOO Z5';
        } elseif (false !== stripos($agent, 'v2131a')) {
            $brand = 'VIVO';
            $model = 'iQOO Z5x';
        } elseif (false !== stripos($agent, 'v2148a')) {
            $brand = 'VIVO';
            $model = 'iQOO Z5 Pro';
        } elseif (false !== stripos($agent, 'v2218a')) {
            $brand = 'VIVO';
            $model = 'iQOO 10 Pro';
        } elseif (false !== stripos($agent, 'v2243a')) {
            $brand = 'VIVO';
            $model = 'iQOO 11 5G';
        } elseif (false !== stripos($agent, 'v1824ba') || false !== stripos($agent, 'v1824a')) {
            $brand = 'VIVO';
            $model = 'iQOO 全网通';
        } elseif (false !== stripos($agent, 'v1824a')) {
            $brand = 'VIVO';
            $model = 'iQOO Monster';
        } elseif (false !== stripos($agent, 'vivo')) {
            $brand = 'VIVO';
            preg_match('/vivo [\w]*/i', $agent, $matches);
            $model = str_ireplace('vivo', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'hm note') || false !== stripos($agent, 'hm201')) {
            $brand = '红米';
            preg_match('/hm [\w]*/i', $agent, $matches);
            $model = str_ireplace('hm', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'm2102j2sc')) {
            $brand = '小米';
            $model = '10S';
        } elseif (false !== stripos($agent, 'm2002j9e')) {
            $brand = '小米';
            $model = '10 青春版';
        } elseif (false !== stripos($agent, 'm2011k2c')) {
            $brand = '小米';
            $model = '11';
        } elseif (false !== stripos($agent, '21091116uc')) {
            $brand = '小米';
            $model = '11 Pro';
        } elseif (false !== stripos($agent, '2201123c')) {
            $brand = '小米';
            $model = '12';
        } elseif (false !== stripos($agent, '2112123ac')) {
            $brand = '小米';
            $model = '12X';
        } elseif (false !== stripos($agent, '2201122c')) {
            $brand = '小米';
            $model = '12 Pro';
        } elseif (false !== stripos($agent, '2206123sc')) {
            $brand = '小米';
            $model = '12S';
        } elseif (false !== stripos($agent, '2206122sc')) {
            $brand = '小米';
            $model = '12 Pro';
        } elseif (false !== stripos($agent, '2207122mc')) {
            $brand = '小米';
            $model = '12S Pro 天玑版';
        } elseif (false !== stripos($agent, '2203121c')) {
            $brand = '小米';
            $model = '12 Ultra';
        } elseif (false !== stripos($agent, '22120rn86c')) {
            $brand = '小米 Redmi';
            $model = '11A';
        } elseif (false !== stripos($agent, 'm2102k1ac')) {
            $brand = '小米 Redmi';
            $model = '11 Pro';
        } elseif (false !== stripos($agent, '23049rad8c')) {
            $brand = '小米 Redmi';
            $model = 'Note 12 Turbo';
        } elseif (false !== stripos($agent, 'm2007j3sc')) {
            $brand = '小米 Redmi';
            $model = 'K30S';
        } elseif (false !== stripos($agent, 'm2012k11ac')) {
            $brand = '小米 Redmi';
            $model = 'K40';
        } elseif (false !== stripos($agent, 'm2012k11c')) {
            $brand = '小米 Redmi';
            $model = 'K40 Pro';
        } elseif (false !== stripos($agent, '21121210C')) {
            $brand = '小米 Redmi';
            $model = 'K50';
        } elseif (false !== stripos($agent, '22041211ac')) {
            $brand = '小米 Redmi';
            $model = 'K50 Pro';
        } elseif (false !== stripos($agent, '22081212c')) {
            $brand = '小米 Redmi';
            $model = 'K50 Ultra';
        } elseif (false !== stripos($agent, '23013rk75c')) {
            $brand = '小米 Redmi';
            $model = 'K60';
        } elseif (false !== stripos($agent, 'redmi')) {
            $brand = '小米';
            preg_match('/redmi [\w|\s]*/i', $agent, $matches);
            $model = str_ireplace('build', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'm2007j22c')) {
            $brand = '小米';
            $model = 'Note 9 5G';
        } elseif (false !== stripos($agent, 'm2007j17c')) {
            $brand = '小米';
            $model = 'Note 9 Pro 5G';
        } elseif (false !== stripos($agent, 'm2104k10ac')) {
            $brand = '小米';
            $model = 'Note 10 Pro';
        } elseif (
            false !== stripos($agent, 'mi ') ||
            false !== stripos($agent, 'mi-one') ||
            false !== stripos($agent, 'mi 1s') ||
            false !== stripos($agent, 'mi 2') ||
            false !== stripos($agent, 'mi 3') ||
            false !== stripos($agent, 'mi 4') ||
            false !== stripos($agent, 'mi 5') ||
            false !== stripos($agent, 'mi pad')) {
            $brand = '小米';
            preg_match('/mi [\w]*/i', $agent, $matches);
            $model = str_ireplace('mi', '', $matches[0] ?? '');
        } elseif (false !== stripos($agent, 'coolpad') || false !== stripos($agent, '8190q') || false !== strpos($agent, '5910')) {
            $brand = '酷派';
            preg_match('/coolpad [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'zte') || false !== stripos($agent, 'x9180') || false !== stripos($agent, 'n9180') || false !== stripos($agent, 'u9180')) {
            $brand = '中兴';
            preg_match('/zte [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
            if (false !== stripos($agent, 'a2022')) {
                $model = 'Axon 30 Pro';
            }
            if (false !== stripos($agent, 'a2023')) {
                $model = 'Axon 40 Pro';
            }
        } elseif (false !== stripos($agent, 'lenovo')) {
            $brand = '联想';
            preg_match('/lenovo [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'htc') || false !== stripos($agent, 'desire')) {
            $brand = 'HTC';
            preg_match('/htc [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'k-touch')) {
            $brand = '天语';
            preg_match('/touch [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'Nubia') || false !== stripos($agent, 'NX50') || false !== stripos($agent, 'NX40')) {
            $brand = '努比亚';
            preg_match('/Nubia [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'nx651j')) {
            $brand = '努比亚';
            $model = 'Play';
        } elseif (false !== stripos($agent, 'M045') || false !== stripos($agent, 'M032') || false !== stripos($agent, 'M355')) {
            $brand = '魅族';
            preg_match('/MEIZU [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'doov')) {
            $brand = '朵唯';
            preg_match('/doov [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'gfive')) {
            $brand = '基伍';
            preg_match('/gfive [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'gionee') || false !== stripos($agent, 'gn')) {
            $brand = '金立';
            preg_match('/gionee [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, '20190619g')) {
            $brand = '金立';
            $model = 'M11';
        } elseif (false !== stripos($agent, '20190620g')) {
            $brand = '金立';
            $model = 'M11s';
        } elseif (false !== stripos($agent, 'hs-u') || false !== stripos($agent, 'hs-e')) {
            $brand = '海信';
            preg_match('/hs [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'rmx2121')) {
            $brand = '真我 RealMe';
            $model = 'X7 Pro';
        } elseif (false !== stripos($agent, 'tdt-ma01')) {
            $brand = '鼎桥';
            $model = 'TD Tech M40';
        } elseif (false !== stripos($agent, 'pgkm10')) {
            $brand = '一加';
            $model = 'One Plus Ace';
        } elseif (false !== stripos($agent, 'Nokia')) {
            $brand = '诺基亚';
            preg_match('/nokia [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        } elseif (false !== stripos($agent, 'nexus')) {
            $brand = 'Nexus';
            preg_match('/nexus [\w]*/i', $agent, $matches);
            $model = $matches[0] ?? '';
        }

        $model = str_ireplace(['_', '-'], '.', $model);

        return array('brand' => $origin ? $brand : strtolower($brand), 'model' => $origin ? trim($model) : strtolower(trim($model)));
    }

    /**
     * 获取客户端浏览器信息 添加win10 edge浏览器判断
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return array
     * @example trim(implode(' ', $browser));
     */
    public static function getBrowser(string $agent = '', bool $origin = true): array {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }

        $browser = array('title' => '未知浏览器', 'name' => 'UnknownBrowser', 'version' => '');

        if (false !== stripos($agent, 'AlipayClient')) {
            preg_match('/AlipayClient\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '支付宝', 'name' => 'AliPay', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'DingTalk')) {
            preg_match('/DingTalk\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '钉钉', 'name' => 'DingTalk', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Quark')) {
            preg_match('/Quark\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '夸克浏览器', 'name' => 'Quark', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'UCBrowser')) {
            preg_match('/UCBrowser\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'UC浏览器', 'name' => 'UCBrowser', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'UBrowser')) {
            preg_match('/UBrowser\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'UC电脑版', 'name' => 'UBrowser', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'MicroMessenger')) {
            if (false !== stripos($agent, 'wxwork')) {
                preg_match('/wxwork\/([\d.]+)/i', $agent, $matches);
                $browser = array('title' => '企业微信', 'name' => 'WeWork', 'version' => $matches[1] ?? '');
            } elseif (false !== stripos($agent, 'wechatdevtools')) {
                preg_match('/wechatdevtools\/([\d.]+)/i', $agent, $matches);
                $browser = array('title' => '微信开发者工具', 'name' => 'WeChatDevTools', 'version' => $matches[1] ?? '');
            } elseif (false !== stripos($agent, 'WindowsWechat')) {
                preg_match('/MicroMessenger\/([\d.]+)/i', $agent, $matches);
                $browser = array('title' => '微信电脑版', 'name' => 'WindowsWeChat', 'version' => $matches[1] ?? '');
            } else {
                preg_match('/MicroMessenger\/([\d.]+)/i', $agent, $matches);
                $browser = array('title' => '微信', 'name' => 'WeChat', 'version' => $matches[1] ?? '');
            }
        } elseif (false !== stripos($agent, 'MQQBrowser')) {
            preg_match('/MQQBrowser\/([\d.]+)/i', $agent, $matches);
            if (false !== stripos($agent, 'TIM')) {
                $browser = array('title' => 'TIM', 'name' => 'MQQBrowser', 'version' => $matches[1] ?? '');
            } else {
                $browser = array('title' => '手机QQ', 'name' => 'MQQBrowser', 'version' => $matches[1] ?? '');
            }
        } elseif (false !== stripos($agent, 'QQBrowser')) {
            preg_match('/QQBrowser\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'QQ浏览器', 'name' => 'QQBrowser', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Firefox')) {
            preg_match('/Firefox\/([^;)]+)+/i', $agent, $matches);
            $browser = array('title' => '火狐浏览器', 'name' => 'Firefox', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Weibo')) {
            preg_match('/Weibo([^;)]+)+/i', $agent, $matches);
            $browser = array('title' => '新浪微博', 'name' => 'SinaWeibo', 'version' => explode('__', trim($matches[1] ?? '', '()'))[2] ?? '');
        } elseif (false !== stripos($agent, 'LBBROWSER')) {
            preg_match('/LBBROWSER\/([^;)]+)+/i', $agent, $matches);
            $browser = array('title' => '猎豹浏览器', 'name' => 'LBBrowser', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'MetaSr')) {
            preg_match('/MetaSr([^;)]+)+/i', $agent, $matches);
            $browser = array('title' => '搜狗浏览器', 'name' => 'MetaSr', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Maxthon')) {
            preg_match('/Maxthon\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '傲游浏览器', 'name' => 'Maxthon', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'OPR')) {
            preg_match('/OPR\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '欧朋浏览器', 'name' => 'Opera', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, '2345Explorer')) {
            preg_match('/2345Explorer\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '2345浏览器', 'name' => '2345Explorer', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'OppoBrowser')) {
            preg_match('/OppoBrowser\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'OPPO浏览器', 'name' => 'OPPO', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'VivoBrowser')) {
            preg_match('/VivoBrowser\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'VIVO浏览器', 'name' => 'VIVO', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'MSIE')) {
            preg_match('/MSIE\s+([^;)]+)+/i', $agent, $matches);
            $browser = array('title' => '微软IE浏览器', 'name' => 'IE', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Edge')) {
            //win10 Edge浏览器 添加了chrome内核标记 在判断Chrome之前匹配
            preg_match('/Edge\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '微软Edge浏览器', 'name' => 'Edge', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Edg')) {
            //win10 Edge浏览器 添加了chrome内核标记 在判断Chrome之前匹配
            preg_match('/Edg\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '微软Edge浏览器', 'name' => 'Edge', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Trident')) {
            preg_match('/Trident\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '微软浏览器', 'name' => 'Trident', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'rv:') && false !== stripos($agent, 'Gecko')) {
            preg_match('/rv:([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '微软IE浏览器', 'name' => 'IE', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Chrome')) {
            preg_match('/Chrome\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => 'Google浏览器', 'name' => 'Chrome', 'version' => $matches[1] ?? '');
        } elseif (false !== stripos($agent, 'Safari')) {
            preg_match('/Safari\/([\d.]+)/i', $agent, $matches);
            $browser = array('title' => '苹果浏览器', 'name' => 'Safari', 'version' => $matches[1] ?? '');
        }

        $browser['kernel'] = self::getKernel($agent, $origin);

        return $browser;
    }

    /**
     * 获取浏览器内核
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return string
     */
    public static function getKernel(string $agent = '', bool $origin = true): string {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }

        if (false !== stripos($agent, 'Blink')) {
            return $origin ? 'Blink' : 'blink';
        }
        if (false !== stripos($agent, 'Trident')) {
            return $origin ? 'Trident' : 'trident';
        }
        if (false !== stripos($agent, 'Gecko')) {
            return $origin ? 'Gecko' : 'gecko';
        }
        if (false !== stripos($agent, 'Webkit')) {
            return $origin ? 'Webkit' : 'webkit';
        }

        return '';
    }

    /**
     * 获取设备类型
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return string
     */
    public static function getDeviceType(string $agent = '', bool $origin = true): string {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }

        if (false !== stripos($agent, 'ipad')) {
            return $origin ? 'Pad' : 'pad';
        }

        if (false !== stripos($agent, 'iphone')) {
            return $origin ? 'Phone' : 'phone';
        }

        if (stripos($agent, 'android')) {
            if (false === stripos($agent, 'mobile')) {
                return $origin ? 'Tablet' : 'tablet';
            }
            return $origin ? 'Mobile' : 'mobile';
        }

        return $origin ? 'PC' : 'pc';
    }

    /**
     * 获取软件平台
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return string
     */
    public static function getPlatform(string $agent = '', bool $origin = true): string {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }
        if (false !== stripos($agent, 'tim')) {
            return $origin ? 'TIM' : 'tim';
        }
        if (false !== stripos($agent, 'wxwork')) {
            return $origin ? 'WeWork' : 'wework';
        }
        if (false !== stripos($agent, 'wecom')) {
            return $origin ? 'WeCom' : 'wecom';
        }
        if (false !== stripos($agent, 'wechatdevtools')) {
            return $origin ? 'WeChatDevTools' : 'wechatdevtools';
        }
        if (self::isWeChat($agent)) {
            return $origin ? 'WeChat' : 'wechat';
        }
        if (self::isQQ($agent)) {
            return $origin ? 'QQ' : 'qq';
        }

        if (self::isAliPay($agent)) {
            return $origin ? 'AliPay' : 'alipay';
        }
        if (self::isDingTalk($agent)) {
            return $origin ? 'DingTalk' : 'dingtalk';
        }

        if (false !== stripos($agent, 'aweme')) {
            return $origin ? 'DouYin' : 'douyin';
        }
        if (false !== stripos($agent, 'VideoArticle')) {
            return $origin ? 'XiGuaVideo' : 'xiguavideo';
        }
        if (false !== stripos($agent, 'feishu')) {
            return $origin ? 'FeiShu' : 'feishu';
        }

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'wework')) {
                return $origin ? 'WeWork' : 'wework';
            }
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'tim')) {
                return $origin ? 'TIM' : 'tim';
            }
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'wecom')) {
                return $origin ? 'WeCom' : 'wecom';
            }
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'rimet')) {
                return $origin ? 'DingTalk' : 'dingtalk';
            }
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'kuaishou')) {
                return $origin ? 'KuaiShou' : 'kuaishou';
            }
            if (false !== stripos($_SERVER['HTTP_X_REQUESTED_WITH'], 'weibo')) {
                return $origin ? 'WeiBo' : 'weibo';
            }
        }
        if (false !== stripos($agent, 'kuaishou')) {
            return $origin ? 'KuaiShou' : 'kuaishou';
        }
        if (false !== stripos($agent, 'AppName')) {
            preg_match('/AppName\/([\w.]+)/i', $agent, $matches);
            return $origin ? $matches[1] : strtolower($matches[1] ?? '');
        }

        if (self::isMobile($agent)) {
            return $origin ? 'Mobile' : 'mobile';
        }

        if (self::isTablet($agent)) {
            return $origin ? 'Tablet' : 'tablet';
        }

        return $origin ? 'PC' : 'pc';
    }

    /**
     * 获取网络类型 * 暂时仅实现腾讯浏览器,
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return string
     */
    public static function getNetType(string $agent = '', bool $origin = true): string {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }

        $net = 'online';
        if (false !== stripos($agent, 'NetType')) {
            preg_match('/NetType\/([\w.]+)/i', $agent, $matches);
            $net = $matches[1] ?? '';
        }

        return $origin ? strtolower($net) : $net;
    }

    /**
     * 获取网络运营商
     *
     * @note 暂未实现
     * @return string
     */
    public static function getIsp(): string {
        return '';
    }

    /**
     * 获得访问者浏览器语言
     *
     * @param bool $origin
     *
     * @return string
     */
    public static function getLang(bool $origin = true): string {
        $lang = 'zh-cn';
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && !empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            if (false !== stripos($lang, 'zh-cn')) {
                $lang = '简体中文';
            } elseif (false !== stripos($lang, 'zh')) {
                $lang = '繁体中文';
            } else {
                $lang = 'English';
            }
        }

        return $origin ? $lang : strtolower($lang);
    }

    /**
     * 获取IPv4
     *
     * @return string
     */
    public static function getIpvs(): string {
        if (isset($_SERVER['HTTP_MARK_CLIENT_IP']) && !empty($_SERVER['HTTP_MARK_CLIENT_IP']) && strcasecmp($_SERVER['HTTP_MARK_CLIENT_IP'], '')) {
            $ip = $_SERVER['HTTP_MARK_CLIENT_IP'];
        } elseif (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), '')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), '')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), '')) {
            $ip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']) && strcasecmp($_SERVER['HTTP_CLIENT_IP'], '')) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], '')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = '';
        }

        return strtolower($ip);
    }

    /**
     * 获取IPv4
     *
     * @return string
     */
    public static function getIPV4(): string {
        return self::get_real_ip([], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
    }

    /**
     * 获取IPv6
     *
     * @return string
     */
    public static function getIPV6(): string {
        return self::get_real_ip([], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
    }

    /**
     * 获取IPv6
     *
     * @return string
     */
    public static function getIpvf(): string {
        return self::get_real_ip([], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
    }

    /**
     * 获取真实IP
     *
     * @param array $headers proxyIpHeader
     * @param int   $filter  FILTER_VALIDATE_IP
     * @param null  $options FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE
     *
     * @return string
     */
    public static function get_real_ip($headers = [], $filter = FILTER_VALIDATE_IP, $options = null): string {
        if (empty($headers)) {
            $headers = array('HTTP_X_REAL_IP', 'HTTP_X_FORWARDED', 'HTTP_X_FORWARDED_FOR', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'HTTP_CLIENT_IP', 'HTTP_X_CLIENT_IP',
                             'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_MARK_CLIENT_IP', 'REMOTE_ADDR', 'HTTP_CF_PSEUDO_IPV4', 'HTTP_CF_CONNECTING_IP');

        }

        if (empty($options)) {
            $options = FILTER_FLAG_IPV4;
        }

        foreach ($headers as $key) {
            if (array_key_exists($key, $_SERVER)) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    //会过滤掉保留地址和私有地址段的IP，例如 127.0.0.1会被过滤
                    if ((bool)filter_var($ip, $filter, $options)) {
                        return $ip;
                    }
                }
            }
        }

        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), '')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), '')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), '')) {
            $ip = getenv('REMOTE_ADDR');
        } else {
            $ip = '';
        }

        $ip = trim($ip);
        if ((bool)filter_var($ip, $filter, $options)) {
            return $ip;
        }

        return '';
    }

    /**
     * 检测是否使用手机访问
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isMobile(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        if (
            !empty($agent) &&
            preg_match('/(blackberry|configuration\/cldc|hp |hp-|htc|htc_|htc-|iemobile|kindle|midp|mmp|motorola|nokia|opera mini|opera|YahooSeeker\/M1A1-R2D2|palm|palmos|pocket|portalmmm|ppc;|smartphone|sonyericsson|sqh|spv|symbian|treo|up.browser|up.link|vodafone|windows ce|xda|xda_)/i',
                $agent)) {
            return true;
        }

        if (!empty($agent) && false != preg_match('/(mobile|android|iphone|ipod|mobi)/i', $agent)) {
            return true;
        }

        if (isset($_SERVER['HTTP_VIA']) && !empty($_SERVER['HTTP_VIA']) && stripos($_SERVER['HTTP_VIA'], 'wap') !== false) {
            return true;
        }

        if (isset($_SERVER['HTTP_ACCEPT']) && !empty($_SERVER['HTTP_ACCEPT']) && stripos($_SERVER['HTTP_ACCEPT'], 'VND.WAP.WML') !== false) {
            return true;
        }

        if (isset($_SERVER['HTTP_X_WAP_PROFILE']) && !empty($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }

        if (isset($_SERVER['HTTP_PROFILE']) && !empty($_SERVER['HTTP_PROFILE'])) {
            return true;
        }

        return false;
    }

    /**
     * 检测是否为平板设备，暂未实现
     *
     * @param string $agent
     *
     * @return false
     */
    public static function isTablet(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }
        if (empty($agent)) {
            return false;
        }

        if (false != preg_match('/(Tablet)/i', $agent) || false != preg_match('/(ipad)/i', $agent)) {
            return true;
        }

        return false;
    }

    /**
     * 检测是否为微信，包括手机端、PC端
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isWeChat(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        if (!empty($agent) && stripos($agent, 'MicroMessenger') !== false) {
            return true;
        }

        if (!empty($agent) && stripos($agent, 'WindowsWechat') !== false) {
            return true;
        }

        if (!empty($agent) && stripos($agent, 'Wechat') !== false) {
            return true;
        }

        return false;
    }

    /**
     * 检测是否为QQ，包括手机端、PC端
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isQQ(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }
        if (!empty($agent) && stripos($agent, 'MQQBrowser') !== false) {
            return true;
        }

        if (!empty($agent) && stripos($agent, 'QQ/') !== false) {
            return true;
        }

        return false;
    }

    /**
     * 检测是否为企业微信
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isWeWork(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        return !empty($agent) && false !== stripos($agent, 'wxwork');
    }

    /**
     * 检测是否为支付宝
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isAliPay(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        return !empty($agent) && false !== stripos($agent, 'alipay');
    }

    /**
     * 检测是否为钉钉
     *
     * @param string $agent
     *
     * @return bool
     */
    public static function isDingTalk(string $agent = ''): bool {
        if (empty($agent)) {
            $agent = self::getUserAgent();
        }

        return !empty($agent) && false !== stripos($agent, 'dingtalk');
    }

    /**
     * 判断浏览器是否支持webp解析
     *
     * @return bool
     */
    public static function isSupportWebp(): bool {
        return isset($_SERVER['HTTP_ACCEPT']) && !empty($_SERVER['HTTP_ACCEPT']) && false !== stripos($_SERVER['HTTP_ACCEPT'], 'image/webp');
    }

    /**
     * 获取系统信息
     *
     * @param string $agent
     * @param bool   $origin
     *
     * @return array
     */
    public static function getInfo(string $agent = '', bool $origin = true): array {
        if (empty($agent)) {
            $agent = self::getUserAgent($origin);
        }

        return [
            'agent' => $agent,
            'accept' => self::getAccept($origin),
            'os' => self::getOs($agent, $origin),

            'brand' => self::getBrand($agent, $origin),
            'browser' => self::getBrowser($agent, $origin),
            'kernel' => self::getKernel($agent, $origin),
            'type' => self::getDeviceType($agent),
            'platform' => self::getPlatform($agent, $origin),

            'isMobile' => self::isMobile($agent),
            'isTablet' => self::isTablet($agent),
            'isWeChat' => self::isWeChat($agent),
            'isWeWork' => self::isWeWork($agent),
            'isQQ' => self::isQQ($agent),
            'isAliPay' => self::isAliPay($agent),
            'isDingTalk' => self::isDingTalk($agent),
            'isWebp' => self::isSupportWebp(),

            'lang' => self::getLang($origin),
            'newtype' => self::getNetType($agent, $origin),
            'isp' => self::getIsp(),

            'ipv4' => self::getIPV4(),
            'ipv6' => self::getIPV6(),

            'ipvs' => self::getIpvs(),
            'ipvf' => self::getIpvf()
        ];
    }
}