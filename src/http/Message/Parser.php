<?php
declare (strict_types=1);

namespace mark\http\Message;

/**
 * The parser which is underlying http\Message.
 * > ***NOTE:***
 * > This class was added in v2.2.0.
 */
class Parser {

    /**
     * Finish up parser at end of (incomplete) input.
     */
    public const CLEANUP = 1;
    /**
     * Soak up the rest of input if no entity length is deducible.
     */
    public const DUMB_BODIES = 2;
    /**
     * Redirect messages do not contain any body despite of indication of such.
     */
    public const EMPTY_REDIRECTS = 4;
    /**
     * Continue parsing while input is available.
     */
    public const GREEDY = 8;
    /**
     * Parse failure.
     */
    public const STATE_FAILURE = -1;
    /**
     * Expecting HTTP info (request/response line) or headers.
     */
    public const STATE_START = 0;
    /**
     * Parsing headers.
     */
    public const STATE_HEADER = 1;
    /**
     * Completed parsing headers.
     */
    public const STATE_HEADER_DONE = 2;
    /**
     * Parsing the body.
     */
    public const STATE_BODY = 3;
    /**
     * Soaking up all input as body.
     */
    public const STATE_BODY_DUMB = 4;
    /**
     * Reading body as indicated by `Content-Length` or `Content-Range`.
     */
    public const STATE_BODY_LENGTH = 5;
    /**
     * Parsing `chunked` encoded body.
     */
    public const STATE_BODY_CHUNKED = 6;
    /**
     * Finished parsing the body.
     */
    public const STATE_BODY_DONE = 7;
    /**
     * Updating Content-Length based on body size.
     */
    public const STATE_UPDATE_CL = 8;
    /**
     * Finished parsing the message.
     * > ***NOTE:***
     * > Most of this states won't be returned to the user, because the parser immediately jumps to the next expected state.
     */
    public const STATE_DONE = 9;

    /**
     * Retrieve the current state of the parser.
     * See http\Message\Parser::STATE_* constants.
     *
     * @return int http\Message\Parser::STATE_* constant.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getState() { }

    /**
     * Parse a string.
     *
     * @param string        $data    The (part of the) message to parse.
     * @param int           $flags   Any combination of [parser flags](http/Message/Parser#Parser.flags:).
     * @param \mark\http\Message $message The current state of the message parsed.
     *
     * @return int http\Message\Parser::STATE_* constant.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function parse(string $data, int $flags, \mark\http\Message $message) { }

    /**
     * Parse a stream.
     *
     * @param resource      $stream  The message stream to parse from.
     * @param int           $flags   Any combination of [parser flags](http/Message/Parser#Parser.flags:).
     * @param \mark\http\Message $message The current state of the message parsed.
     *
     * @return int http\Message\Parser::STATE_* constant.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function stream($stream, int $flags, \mark\http\Message $message) { }

}