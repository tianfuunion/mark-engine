<?php
declare (strict_types=1);

namespace mark\http\Message;

/**
 * The message body, represented as a PHP (temporary) stream.
 * > ***NOTE:***
 * > Currently, http\Message\Body::addForm() creates multipart/form-data bodies.
 */
class Body implements \Serializable {

    /**
     * Create a new message body, optionally referencing $stream.
     *
     * @param resource $stream A stream to be used as message body.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\UnexpectedValueException
     */
    public function __construct($stream = null) { }

    /**
     * String cast handler.
     *
     * @return string the message body.
     */
    public function __toString() { }

    /**
     * Add form fields and files to the message body.
     * > ***NOTE:***
     * > Currently, http\Message\Body::addForm() creates "multipart/form-data" bodies.
     *
     * @param array $fields List of form fields to add.
     * @param array $files  List of form files to add.
     *                      $fields must look like:
     *                      [
     *                      "field_name" => "value",
     *                      "multi_field" => ["value1", "value2"]
     *                      ]
     *                      $files must look like:
     *                      [
     *                      [
     *                      "name" => "field_name",
     *                      "type" => "content/type",
     *                      "file" => "/path/to/file.ext"
     *                      ], [
     *                      "name" => "field_name2",
     *                      "type" => "text/plain",
     *                      "file" => "file.ext",
     *                      "data" => "string"
     *                      ], [
     *                      "name" => "field_name3",
     *                      "type" => "image/jpeg",
     *                      "file" => "file.ext",
     *                      "data" => fopen("/home/mike/Pictures/mike.jpg","r")
     *                      ]
     *                      As you can see, a file structure must contain a "file" entry,
     *                      which holds a file path, and an optional "data" entry, which may either contain a resource to read from or the actual data as string.
     *
     * @return \mark\http\Message\Body self.
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addForm(array $fields = null, array $files = null) { }

    /**
     * Add a part to a multipart body.
     *
     * @param \mark\http\Message $part The message part.
     *
     * @return \mark\http\Message\Body self.
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addPart(\mark\http\Message $part) { }

    /**
     * Append plain bytes to the message body.
     *
     * @param string $data The data to append to the body.
     *
     * @return \mark\http\Message\Body self.
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function append(string $data) { }

    /**
     * Retrieve the ETag of the body.
     *
     * @return string|false string an Apache style ETag of inode, mtime and size in hex concatenated by hyphens if the message body stream is stat-able.
     *         or string a content hash (which algorithm is determined by INI http.etag.mode) if the stream is not stat-able.
     *         or false if http.etag.mode is not a known hash algorithm.
     */
    public function etag() { }

    /**
     * Retrieve any boundary of the message body.
     *
     * @See \mark\http\Message::splitMultipartBody().
     * @return string|null string the message body boundary.
     *         or NULL if this message body has no boundary.
     */
    public function getBoundary() { }

    /**
     * Retrieve the underlying stream resource.
     *
     * @return resource the underlying stream.
     */
    public function getResource() { }

    /**
     * Implements Serializable.
     * Alias of http\Message\Body::__toString().
     *
     * @return string serialized message body.
     */
    public function serialize() { }

    /**
     * Stat size, atime, mtime and/or ctime.
     *
     * @param string $field A single stat field to retrieve.
     *
     * @return int|object int the requested stat field.
     *         or object stdClass instance holding all four stat fields.
     */
    public function stat(string $field = null) { }

    /**
     * Stream the message body through a callback.
     *
     * @param callable $callback The callback of the form function(http\Message\Body $from, string $data).
     * @param int      $offset   Start to stream from this offset.
     * @param int      $maxlen   Stream at most $maxlen bytes, or all if $maxlen is less than 1.
     *
     * @return \mark\http\Message\Body self.
     */
    public function toCallback(callable $callback, int $offset = 0, int $maxlen = 0) { }

    /**
     * Stream the message body into another stream $stream, starting from $offset, streaming $maxlen at most.
     *
     * @param resource $stream The resource to write to.
     * @param int      $offset The starting offset.
     * @param int      $maxlen The maximum amount of data to stream. All content if less than 1.
     *
     * @return \mark\http\Message\Body self.
     */
    public function toStream($stream, int $offset = 0, int $maxlen = 0) { }

    /**
     * Retrieve the message body serialized to a string.
     * Alias of http\Message\Body::__toString().
     *
     * @return string message body.
     */
    public function toString() { }

    /**
     * Implements Serializable.
     *
     * @param string $serialized The serialized message body.
     */
    public function unserialize($serialized) { }

}