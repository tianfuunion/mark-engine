<?php
declare (strict_types=1);

namespace mark\http;

/**
 * A class representing a list of cookies with specific attributes.
 */
class Cookie {
    /**
     * Do not decode cookie contents.
     */
    public const PARSE_RAW = 1;
    /**
     * The cookies' flags have the secure attribute set.
     */
    public const SECURE = 16;
    /**
     * The cookies' flags have the httpOnly attribute set.
     */
    public const HTTPONLY = 32;

    /**
     * Create a new cookie list.
     *
     * @param mixed $cookies        The string or list of cookies to parse or set.
     * @param int   $flags          Parse flags. @See \mark\http\Cookie::PARSE_* constants.
     * @param array $allowed_extras List of extra attribute names to recognize.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\RuntimeException
     */
    public function __construct($cookies = null, int $flags = 0, array $allowed_extras = null) { }

    /**
     * String cast handler. Alias of http\Cookie::toString().
     *
     * @return string the cookie(s) represented as string.
     */
    public function __toString() { }

    /**
     * Add a cookie.
     *
     * @See \mark\http\Cookie::setCookie() and http\Cookie::addCookies().
     *
     * @param string $cookie_name  The key of the cookie.
     * @param string $cookie_value The value of the cookie.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addCookie(string $cookie_name, string $cookie_value) { }

    /**
     * (Re)set the cookies.
     *
     * @See \mark\http\Cookie::setCookies().
     *
     * @param array $cookies Add cookies of this array of form ["name" => "value"].
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addCookies(array $cookies) { }

    /**
     * Add an extra attribute to the cookie list.
     *
     * @See \mark\http\Cookie::setExtra().
     *
     * @param string $extra_name  The key of the extra attribute.
     * @param string $extra_value The value of the extra attribute.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addExtra(string $extra_name, string $extra_value) { }

    /**
     * Add several extra attributes.
     *
     * @See \mark\http\Cookie::addExtra().
     *
     * @param array $extras A list of extra attributes of the form ["key" => "value"].
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addExtras(array $extras) { }

    /**
     * Retrieve a specific cookie value.
     * @@See \mark\http\Cookie::setCookie().
     *
     * @param string $cookie_name The key of the cookie to look up.
     *
     * @return string|null string the cookie value. or NULL if $cookie_name could not be found.
     */
    public function getCookie(string $cookie_name) { }

    /**
     * Get the list of cookies.
     *
     * @See \mark\http\Cookie::setCookies().
     * @return array the list of cookies of form ["name" => "value"].
     */
    public function getCookies() { }

    /**
     * Retrieve the effective domain of the cookie list.
     *
     * @See \mark\http\Cookie::setDomain().
     * @return string the effective domain.
     */
    public function getDomain() { }

    /**
     * Get the currently set expires attribute.
     *
     * @See \mark\http\Cookie::setExpires().
     * > ***NOTE:***
     * > A return value of -1 means that the attribute is not set.
     * @return int the currently set expires attribute as seconds since the epoch.
     */
    public function getExpires() { }

    /**
     * Retrieve an extra attribute.
     *
     * @See \mark\http\Cookie::setExtra().
     *
     * @param string $name The key of the extra attribute.
     *
     * @return string the value of the extra attribute.
     */
    public function getExtra(string $name) { }

    /**
     * Retrieve the list of extra attributes.
     *
     * @See \mark\http\Cookie::setExtras().
     * @return array the list of extra attributes of the form ["key" => "value"].
     */
    public function getExtras() { }

    /**
     * Get the currently set flags.
     *
     * @See \mark\http\Cookie::SECURE and http\Cookie::HTTPONLY constants.
     * @return int the currently set flags bitmask.
     */
    public function getFlags() { }

    /**
     * Get the currently set max-age attribute of the cookie list.
     *
     * @See \mark\http\Cookie::setMaxAge().
     * > ***NOTE:***
     * > A return value of -1 means that the attribute is not set.
     * @return int the currently set max-age.
     */
    public function getMaxAge() { }

    /**
     * Retrieve the path the cookie(s) of this cookie list are effective at.
     *
     * @See \mark\http\Cookie::setPath().
     * @return string the effective path.
     */
    public function getPath() { }

    /**
     * (Re)set a cookie.
     *
     * @See \mark\http\Cookie::addCookie() and http\Cookie::setCookies().
     * > ***NOTE:***
     * > The cookie will be deleted from the list if $cookie_value is NULL.
     *
     * @param string $cookie_name  The key of the cookie.
     * @param string $cookie_value The value of the cookie.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setCookie(string $cookie_name, string $cookie_value) { }

    /**
     * (Re)set the cookies.
     *
     * @See \mark\http\Cookie::addCookies().
     *
     * @param array $cookies Set the cookies to this array.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setCookies(array $cookies = null) { }

    /**
     * Set the effective domain of the cookie list.
     *
     * @See \mark\http\Cookie::setPath().
     *
     * @param string $value The domain the cookie(s) belong to.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setDomain(string $value = null) { }

    /**
     * Set the traditional expires timestamp.
     *
     * @See \mark\http\Cookie::setMaxAge() for a safer alternative.
     *
     * @param int $value The expires timestamp as seconds since the epoch.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setExpires(int $value = -1) { }

    /**
     * (Re)set an extra attribute.
     *
     * @See \mark\http\Cookie::addExtra().
     * > ***NOTE:***
     * > The attribute will be removed from the extras list if $extra_value is NULL.
     *
     * @param string $extra_name  The key of the extra attribute.
     * @param string $extra_value The value of the extra attribute.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setExtra(string $extra_name, string $extra_value = null) { }

    /**
     * (Re)set the extra attributes.
     *
     * @See \mark\http\Cookie::addExtras().
     *
     * @param array $extras Set the extra attributes to this array.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setExtras(array $extras = null) { }

    /**
     * Set the flags to specified $value.
     *
     * @See \mark\http\Cookie::SECURE and http\Cookie::HTTPONLY constants.
     *
     * @param int $value The new flags bitmask.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setFlags(int $value = 0) { }

    /**
     * Set the maximum age the cookie may have on the client side.
     * This is a client clock departure safe alternative to the "expires" attribute.
     *
     * @See \mark\http\Cookie::setExpires().
     *
     * @param int $value The max-age in seconds.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setMaxAge(int $value = -1) { }

    /**
     * Set the path the cookie(s) of this cookie list should be effective at.
     *
     * @See \mark\http\Cookie::setDomain().
     *
     * @param string $path The URL path the cookie(s) should take effect within.
     *
     * @return \mark\http\Cookie self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setPath(string $path = null) { }

    /**
     * Get the cookie list as array.
     *
     * @return array the cookie list as array.
     */
    public function toArray() { }

    /**
     * Retrieve the string representation of the cookie list.
     *
     * @See \mark\http\Cookie::toArray().
     * @return string the cookie list as string.
     */
    public function toString() { }
}