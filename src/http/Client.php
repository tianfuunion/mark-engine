<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The HTTP client.
 *
 * @See \mark\http\Client\Curl's [options](/mark/http/Client/Curl#Options:) which is the only driver currently supported.
 */
class Client implements \SplSubject, \Countable {

    /**
     * Debug callback's $data contains human readable text.
     */
    public const DEBUG_INFO = 0;
    /**
     * Debug callback's $data contains data received.
     */
    public const DEBUG_IN = 1;
    /**
     * Debug callback's $data contains data sent.
     */
    public const DEBUG_OUT = 2;
    /**
     * Debug callback's $data contains headers.
     */
    public const DEBUG_HEADER = 16;
    /**
     * Debug callback's $data contains a body part.
     */
    public const DEBUG_BODY = 32;
    /**
     * Debug callback's $data contains SSL data.
     */
    public const DEBUG_SSL = 64;
    /**
     * Attached observers.
     *
     * @var \SplObjectStorage
     */
    private $observers = null;
    /**
     * Set options.
     *
     * @var array
     */
    protected $options = null;
    /**
     * Request/response history.
     *
     * @var \mark\http\Message
     */
    protected $history = null;
    /**
     * Whether to record history in http\Client::$history.
     *
     * @var bool
     */
    public $recordHistory = false;

    /**
     * Create a new HTTP client.
     * Currently only "curl" is supported as a $driver, and used by default.
     * Persisted resources identified by $persistent_handle_id will be re-used if available.
     *
     * @param string $driver               The HTTP client driver to employ. Currently only the default driver, "curl", is supported.
     * @param string $persistent_handle_id If supplied, created curl handles will be persisted with this identifier for later reuse.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\http\exception\RuntimeException
     */
    public function __construct(string $driver = null, string $persistent_handle_id = null) { }

    /**
     * Add custom cookies.
     *
     * @See \mark\http\Client::setCookies().
     *
     * @param array $cookies Custom cookies to add.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addCookies(array $cookies = null) { }

    /**
     * Add specific SSL options.
     *
     * @See \mark\http\Client::setSslOptions()
     * @See \mark\http\Client::setOptions()
     * @See \mark\http\Client\Client::$ssl options.
     *
     * @param array $ssl_options Add this SSL options.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addSslOptions(array $ssl_options = null) { }

    /**
     * Implements SplSubject. Attach another observer.
     * Attached observers will be notified with progress of each transfer.
     *
     * @param \SplObserver $observer An implementation of SplObserver.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function attach(\SplObserver $observer) { }

    /**
     * Configure the client's low level options.
     * > ***NOTE:***
     * > This method has been added in v2.3.0.
     *
     * @param array $configuration Key/value pairs of low level options.
     *                             See f.e. the [configuration options for the Curl driver](http/Client/Curl#Configuration:).
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function configure(array $configuration) { }

    /**
     * Implements Countable. Retrieve the number of enqueued requests.
     * > ***NOTE:***
     * > The enqueued requests are counted without regard whether they are finished or not.
     *
     * @return int number of enqueued requests.
     */
    public function count() { }

    /**
     * Dequeue the \mark\http\Client\Request $request.
     *
     * @See \mark\http\Client::requeue(), if you want to requeue the request, instead of calling http\Client::dequeue() and then http\Client::enqueue().
     *
     * @param \mark\http\Client\Request $request The request to cancel.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function dequeue(\mark\http\Client\Request $request) { }

    /**
     * Implements SplSubject. Detach $observer, which has been previously attached.
     *
     * @param \SplObserver $observer Previously attached instance of SplObserver implementation.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function detach(\SplObserver $observer) { }

    /**
     * Enable usage of an event library like libevent, which might improve performance with big socket sets.
     *
     * @param bool $enable Whether to enable libevent usage.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     * @see Client::configure()
     * #[Deprecated('This method has been deprecated in 2.3.0. Use http\Client::configure() instead')]
     */
    public function enableEvents(bool $enable = true) { }

    /**
     * Enable sending pipelined requests to the same host if the driver supports it.
     *
     * @param bool $enable Whether to enable pipelining.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     * @see Client::configure()
     * #[Deprecated('This method has been deprecated in 2.3.0. Use http\Client::configure() instead')]
     */
    public function enablePipelining(bool $enable = true) { }

    /**
     * Add another \mark\http\Client\Request to the request queue.
     * If the optional callback $cb returns true, the request will be automatically dequeued.
     * > ***Note:***
     * > The http\Client\Response object resulting from the request is always stored
     * > internally to be retrieved at a later time, __even__ when $cb is used.
     * >
     * > If you are about to send a lot of requests and do __not__ need the response
     * > after executing the callback, you can use http\Client::getResponse() within
     * > the callback to keep the memory usage level as low as possible.
     *
     * @See \mark\http\Client::dequeue()
     * @See \mark\http\Client::send().
     *
     * @param \mark\http\Client\Request $request The request to enqueue.
     * @param callable                  $cb      as function(\mark\http\Response $response) : ?bool A callback to automatically call when the request has finished.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function enqueue(\mark\http\Client\Request $request, callable $cb = null) { }

    /**
     * Get a list of available configuration options and their default values.
     * See f.e. the [configuration options for the Curl driver](http/Client/Curl#Configuration:).
     *
     * @return array list of key/value pairs of available configuration options and their default values.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getAvailableConfiguration() { }

    /**
     * List available drivers.
     *
     * @return array list of supported drivers.
     */
    public function getAvailableDrivers() { }

    /**
     * Retrieve a list of available request options and their default values.
     * See f.e. the [request options for the Curl driver](http/Client/Curl#Options:).
     *
     * @return array list of key/value pairs of available request options and their default values.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getAvailableOptions() { }

    /**
     * Get priorly set custom cookies.
     *
     * @See \mark\http\Client::setCookies().
     * @return array custom cookies.
     */
    public function getCookies() { }

    /**
     * Simply returns the http\Message chain representing the request/response history.
     * > ***NOTE:***
     * > The history is only recorded while http\Client::$recordHistory is true.
     *
     * @return \mark\http\Message the request/response message chain representing the client's history.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getHistory() { }

    /**
     * Returns the SplObjectStorage holding attached observers.
     *
     * @return \SplObjectStorage observer storage.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getObservers() { }

    /**
     * Get priorly set options.
     *
     * @See \mark\http\Client::setOptions().
     * @return array options.
     */
    public function getOptions() { }

    /**
     * Retrieve the progress information for $request.
     *
     * @param \mark\http\Client\Request $request The request to retrieve the current progress information for.
     *
     * @return object|null object stdClass instance holding progress information.
     *         or NULL if $request is not enqueued.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getProgressInfo(\mark\http\Client\Request $request) { }

    /**
     * Retrieve the corresponding response of an already finished request, or the last received response if $request is not set.
     * > ***NOTE:***
     * > If $request is NULL, then the response is removed from the internal storage (stack-like operation).
     *
     * @param \mark\http\Client\Request $request The request to fetch the stored response for.
     *
     * @return \mark\http\Client\Response|null \mark\http\Client\Response the stored response for the request, or the last that was received.
     *         or NULL if no more response was available to pop, when no $request was given.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getResponse(\mark\http\Client\Request $request = null) { }

    /**
     * Retrieve priorly set SSL options.
     *
     * @See \mark\http\Client::getOptions()
     * @See \mark\http\Client::setSslOptions().
     * @return array SSL options.
     */
    public function getSslOptions() { }

    /**
     * Get transfer related information for a running or finished request.
     *
     * @param \mark\http\Client\Request $request The request to probe for transfer info.
     *
     * @return object stdClass instance holding transfer related information.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getTransferInfo(\mark\http\Client\Request $request) { }

    /**
     * Implements SplSubject. Notify attached observers about progress with $request.
     *
     * @param \mark\http\Client\Request $request  The request to notify about.
     * @param object                    $progress stdClass instance holding progress information.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function notify(\mark\http\Client\Request $request = null, $progress = null) { }

    /**
     * Perform outstanding transfer actions.
     *
     * @See \mark\http\Client::wait() for the completing interface.
     * @return bool true if there are more transfers to complete.
     */
    public function once() { }

    /**
     * Requeue an \mark\http\Client\Request.
     * The difference simply is, that this method, in contrast to \mark\http\Client::enqueue(),
     * does not throw an \mark\http\Exception when the request to queue is already enqueued and dequeues it automatically prior enqueueing it again.
     *
     * @param \mark\http\Client\Request $request  The request to queue.
     * @param callable                  $callable as function(\mark\http\Response $response) : ?bool
     *                                            A callback to automatically call when the request has finished.
     *
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function requeue(\mark\http\Client\Request $request, callable $callable = null) { }

    /**
     * Reset the client to the initial state.
     *
     * @return \mark\http\Client self.
     */
    public function reset() { }

    /**
     * Send all enqueued requests.
     *
     * @See \mark\http\Client::once() and http\Client::wait() for a more fine grained interface.
     * @return \mark\http\Client self.
     * @throws \mark\http\exception\RuntimeException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function send() { }

    /**
     * Set custom cookies.
     *
     * @See \mark\http\Client::addCookies() and http\Client::getCookies().
     *
     * @param array $cookies Set the custom cookies to this array.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setCookies(array $cookies = null) { }

    /**
     * Set client debugging callback.
     * > ***NOTE:***
     * > This method has been added in v2.6.0, resp. v3.1.0.
     *
     * @param callable $callback as function(http\Client $c, \mark\http\Client\Request $r, int $type, string $data)
     *                           The debug callback. For $type see http\Client::DEBUG_* constants.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setDebug(callable $callback) { }

    /**
     * Set client options.
     * > ***NOTE:***
     * > Only options specified prior enqueueing a request are applied to the request.
     *
     * @param array $options The options to set.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setOptions(array $options = null) { }

    /**
     * Specifically set SSL options.
     *
     * @See \mark\http\Client::setOptions() and http\Client\Client\$ssl options.
     *
     * @param array $ssl_options Set SSL options to this array.
     *
     * @return \mark\http\Client self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setSslOptions(array $ssl_options = null) { }

    /**
     * Wait for $timeout seconds for transfers to provide data.
     * This is the completion call to @See \mark\http\Client::once().
     *
     * @param float $timeout Seconds to wait for data on open sockets.
     *
     * @return bool success.
     */
    public function wait(float $timeout = 0) { }
}