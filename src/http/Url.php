<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The \mark\http\Url class provides versatile means to parse, construct and manipulate URLs.
 */
class Url {
    /**
     * Replace parts of the old URL with parts of the new.
     */
    public const REPLACE = 0;
    /**
     * Whether a relative path should be joined into the old path.
     */
    public const JOIN_PATH = 1;
    /**
     * Whether the querystrings should be joined.
     */
    public const JOIN_QUERY = 2;
    /**
     * Strip the user information from the URL.
     */
    public const STRIP_USER = 4;
    /**
     * Strip the password from the URL.
     */
    public const STRIP_PASS = 8;
    /**
     * Strip user and password information from URL (same as STRIP_USER|STRIP_PASS).
     */
    public const STRIP_AUTH = 12;
    /**
     * Do not include the port.
     */
    public const STRIP_PORT = 32;
    /**
     * Do not include the URL path.
     */
    public const STRIP_PATH = 64;
    /**
     * Do not include the URL querystring.
     */
    public const STRIP_QUERY = 128;
    /**
     * Strip the fragment (hash) from the URL.
     */
    public const STRIP_FRAGMENT = 256;
    /**
     * Strip everything except scheme and host information.
     */
    public const STRIP_ALL = 492;
    /**
     * Import initial URL parts from the SAPI environment.
     */
    public const FROM_ENV = 4096;
    /**
     * Whether to sanitize the URL path (consolidate double slashes, directory jumps etc.)
     */
    public const SANITIZE_PATH = 8192;
    /**
     * Parse UTF-8 encoded multibyte sequences.
     */
    public const PARSE_MBUTF8 = 131072;
    /**
     * Parse locale encoded multibyte sequences (on systems with wide character support).
     */
    public const PARSE_MBLOC = 65536;
    /**
     * Parse and convert multibyte hostnames according to IDNA (with IDNA support).
     */
    public const PARSE_TOIDN = 1048576;
    /**
     * Explicitly request IDNA2003 implementation if available (libidn, idnkit or ICU).
     */
    public const PARSE_TOIDN_2003 = 9437184;
    /**
     * Explicitly request IDNA2008 implementation if available (libidn2, idnkit2 or ICU).
     */
    public const PARSE_TOIDN_2008 = 5242880;
    /**
     * Percent encode multibyte sequences in the userinfo, path, query and fragment parts of the URL.
     */
    public const PARSE_TOPCT = 2097152;
    /**
     * Continue parsing when encountering errors.
     */
    public const IGNORE_ERRORS = 268435456;
    /**
     * Suppress errors/exceptions.
     */
    public const SILENT_ERRORS = 536870912;
    /**
     * Standard flags used by default internally for e.g. http\Message::setRequestUrl().
     *   Enables joining path and query, sanitizing path, multibyte/unicode, international domain names and transient percent encoding.
     */
    public const STDFLAGS = 3350531;
    /**
     * The URL's scheme.
     *
     * @var string
     */
    public $scheme = null;
    /**
     * Authenticating user.
     *
     * @var string
     */
    public $user = null;
    /**
     * Authentication password.
     *
     * @var string
     */
    public $pass = null;
    /**
     * Hostname/domain.
     *
     * @var string
     */
    public $host = null;
    /**
     * Port.
     *
     * @var string
     */
    public $port = null;
    /**
     * URL path.
     *
     * @var string
     */
    public $path = null;
    /**
     * URL querystring.
     *
     * @var string
     */
    public $query = null;
    /**
     * URL fragment (hash).
     *
     * @var string
     */
    public $fragment = null;

    /**
     * Create an instance of an http\Url.
     * > ***NOTE:***
     * > Prior to v3.0.0, the default for the $flags parameter was http\Url::FROM_ENV.
     * See also http\Env\Url.
     *
     * @param mixed $old_url Initial URL parts. Either an array, object, http\Url instance or string to parse.
     * @param mixed $new_url Overriding URL parts. Either an array, object, http\Url instance or string to parse.
     * @param int   $flags   The modus operandi of constructing the url. See http\Url constants.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\BadUrlException
     */
    public function __construct($old_url = null, $new_url = null, int $flags = 0) { }

    /**
     * String cast handler. Alias of http\Url::toString().
     *
     * @return string the URL as string.
     */
    public function __toString() { }

    /**
     * Clone this URL and apply $parts to the cloned URL.
     * > ***NOTE:***
     * > This method returns a clone (copy) of this instance.
     *
     * @param mixed $parts New URL parts.
     * @param int   $flags Modus operandi of URL construction. See http\Url constants.
     *
     * @return \mark\http\Url clone.
     * @throws \mark\http\exception\BadUrlException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function mod($parts, int $flags = \mark\http\Url::JOIN_PATH | \mark\http\Url::JOIN_QUERY | \mark\http\Url::SANITIZE_PATH) { }

    /**
     * Retrieve the URL parts as array.
     *
     * @return array the URL parts.
     */
    public function toArray() { }

    /**
     * Get the string prepresentation of the URL.
     *
     * @return string the URL as string.
     */
    public function toString() { }
}