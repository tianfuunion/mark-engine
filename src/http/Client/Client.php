<?php
declare (strict_types=1);

namespace mark\http\Client;

use mark\cache\CacheHandle;
use mark\exception\ClassNotFoundException;
use mark\exception\FunctionNotFoundException;
use mark\exception\InvalidArgumentException;
use mark\exception\UnexpectedValueException;
use mark\http\exception\ResponseException;
use mark\Mark;
use mark\system\Os;
use Psr\SimpleCache\CacheInterface;

/**
 * Class Http Curl Client
 *
 * @description HttpClient Curl for php 封装类
 * @author      Mark<mark@tianfuunion.cn>
 * @site https://www.tianfuunion.cn
 * @time        2019年10月11日 15:27:00
 * @modifyTime  2020年02月28日 00:38:00
 * @modifyTime  2020年07月25日 17:00:00
 * @modifyTime  2020年11月17日 20:16:00 修复post传参无法获取问题
 * @modifyTime  2021年12月04日 00:05:00 修复设置接收响应头时无法正常接收响应内容的问题
 * @modifyTime  2022年03月14日 01:40:00 完善添加Cookie的方法
 * @modifyTime  2022年04月22日 01:20:00 抛出响应异常; 优化响应头数组方式输出
 * HTTP/1.1协议中共定义了八种方法（有时也叫“动作”）来表明Request-URI指定的资源的不同操作方式：
 * OPTIONS：返回服务器针对特定资源所支持的HTTP请求方法。也可以利用向Web服务器发送"*"的请求来测试服务器的功能性。
 * HEAD：向服务器索要与GET请求相一致的响应，只不过响应体将不会被返回。这一方法可以在不必传输整个响应内容的情况下，就可以获取包含在响应消息头中的元信息。
 * GET：向特定的资源发出请求。注意：GET方法不应当被用于产生“副作用”的操作中。
 * POST：向指定资源提交数据进行处理请求（例如提交表单或者上传文件）。数据被包含在请求体中。POST请求可能会导致新的资源的建立和/或已有资源的修改。
 * PUT：向指定资源位置上传其最新内容。
 * DELETE：请求服务器删除Request-URI所标识的资源。
 * TRACE：回显服务器收到的请求，主要用于测试或诊断。
 * CONNECT：HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器
 * #method self setLocation($location = true);
 * #method self setTransfer($transfer = true);
 * #method self setResponseHeader($header = true);
 * #method self setRequestHeaderOut($output = true);
 * #method self setTimeout($timeout = 60);
 * #method self setConnectTimeout($timeout = 0);
 * #method self setUserAgent(string $userAgent);
 * @package     mark\http\Client
 */
class Client {

    /**
     * @var resource
     */
    private $curl;

    private $transfer = true;

    /**
     * @param bool $transfer
     *
     * @return $this
     */
    public function setTransfer($transfer = true): self {
        $this->transfer = $transfer;

        return $this;
    }

    private $url;                        // 访问的url
    private $method = 'get';             // 访问方式,默认是GET请求
    private $charset = 'utf-8';

    /**
     * 是否开启请求头
     *
     * @link https://www.runoob.com/http/http-content-type.html
     * @var bool
     */
    private $requestHeaderOut = false;

    /**
     * The headers being sent in the request.
     *
     * @var array
     */
    private $requestHeader = array();

    /**
     * @var string
     */
    private $requestType = 'text';

    /**
     * 是否支持重定向(默认不支持)
     *
     * @see set('location',true)
     * @var bool
     */
    private $location = false;

    /**
     * @param bool $location
     *
     * @return $this
     */
    public function setLocation($location = true): self {
        $this->location = $location;

        return $this;
    }

    /**
     * @var int
     */
    private $maxredirs = 10;

    /**
     * 最大递归返回的数量
     *
     * @param int $maxredirs
     *
     * @return $this
     */
    public function setMaxredirs(int $maxredirs): self {
        $this->maxredirs = $maxredirs;

        return $this;
    }

    private $timeout = 60;

    /**
     * @param int $timeout
     *
     * @return $this
     */
    public function setTimeout($timeout = 0): self {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * 在发起连接前等待的时间，如果设置为0，则不等待。
     *
     * @var int
     */
    private $connectTimeout = 0;

    /**
     * @param int $timeout
     *
     * @return $this
     */
    public function setConnectTimeout($timeout = 0): self {
        $this->connectTimeout = $timeout;

        return $this;
    }

    private $formData = array();
    private $fileData = array();
    private $upfilesize = 0;

    private $boundary = '';
    private $userpwd = '';

    private $cookie;                              // 设定HTTP请求中'Cookie: '部分的内容。多个cookie用分号分隔，分号后带一个空格(例如， 'fruit=apple; colour=red')。
    private $cookie_file = '';                    // 包含cookie数据的文件名，cookie文件的格式可以是Netscape格式，或者只是纯HTTP头部信息存入文件。
    private $cookie_jar = '';                     // 连接结束后保存cookie信息的文件。

    private $outfile;

    private $errno;
    private $errmsg;

    // 响应头
    private $responseHeader = false;              // 是否接收响应头
    private $responseHeaderSize = 0;              // 响应头大小
    private $responseHeaderContent = '';          // 响应头内容

    /**
     * 设置是否接收响应头
     *
     * @param bool $header
     *
     * @return $this
     */
    public function setResponseHeader($header = true): self {
        $this->responseHeader = $header;
        return $this;
    }

    /**
     * @param bool $output
     *
     * @return $this
     * @see \mark\http\Client\Client::setResponseHeader()
     * @deprecated
     */
    public function setRequestHeaderOut($output = true): self {
        $this->responseHeaderContent = $output;

        return $this;
    }

    /**
     * 响应内容
     *
     * @var \mark\response\Response
     */
    private \mark\response\Response $response;

    private $fileName = '';
    private $fileExtension = '';
    private $filePath = '';

    /**
     * Curl constructor.
     */
    private function __construct() {
        if (!extension_loaded('curl')) {
            throw new ClassNotFoundException('cURL library is not loaded', 'CURL');
        }
    }

    /**
     * 创建Facade实例。
     *
     * @return static
     */
    public static function getInstance(): self {
        return new self();
    }

    /**
     * Curl初始化
     *
     * @param string $url
     *
     * @return $this
     */
    private function initialize(string $url): self {
        if (!empty($url)) {
            $this->url = trim($url);
        }

        if (!empty($this->url)) {
            $this->curl = curl_init($this->url) or die('Curl初始化失败');
        } else {
            $this->curl = curl_init() or die('Curl初始化失败');
        }

        return $this;
    }

    /**
     * 发起HTTP请求
     *
     * @param string $method
     * @param string $url
     * @param string $type
     *
     * @return \mark\response\Response
     */
    public function request(string $method, string $url, $type = 'text'): \mark\response\Response {
        switch (strtolower($method ?: '')) {
            case 'get':
                return $this->get($url, $type);
            // not break;
            case 'post':
                return $this->post($url, $type);
            // not break;
            case 'head':
                return $this->head($url, $type);
            // not break;
            case 'upload':
                return $this->upload($url);
            // not break;
            case 'download':
                return $this->download($url);
            // not break;
            case 'pub':
                return $this->put($url);
            // not break;
            case 'ftp':
                return $this->ftp($url);
            // not break;
            case 'delete':
                return $this->delete($url);
            // not break;
            default:
                return $this->execute();
            // not break;
        }
    }

    /**
     * Get 请求
     *
     * @param string $url
     * @param string $type
     *
     * @return \mark\response\Response
     */
    public function get(string $url, $type = 'text'): \mark\response\Response {
        $this->method = 'get';
        $this->initialize($url);
        switch ($type) {
            case 'json':
                $this->addHeader('Content-Type', 'application/json');
                break;
            case 'pdf':
                $this->addHeader('Content-Type', 'application/pdf');
                break;
            case 'msword':
                $this->addHeader('Content-Type', 'application/msword');
                break;
            case 'stream':
                $this->addHeader('Content-Type', 'application/octet-stream');
                break;

            case 'html':
                $this->addHeader('Content-Type', 'text/html');
                break;
            case 'text':
                $this->addHeader('Content-Type', 'text/plain');
                break;
            case 'xml':
                $this->addHeader('Content-Type', 'text/xml');
                break;
            case 'js':
                $this->addHeader('Content-Type', 'application/x-javascript');
                break;
            case 'css':
                $this->addHeader('Content-Type', 'text/css');
                break;

            case 'gif':
                $this->addHeader('Content-Type', 'image/gif');
                break;
            case 'jpg':
            case 'jpeg':
                $this->addHeader('Content-Type', 'image/jpeg');
                break;
            case 'png':
                $this->addHeader('Content-Type', 'image/png');
                break;
            case 'webp':
                $this->addHeader('Content-Type', 'image/webp');
                break;

            case 'mp3':
                $this->addHeader('Content-Type', 'audio/mp3');
                break;
            case 'wav':
                $this->addHeader('Content-Type', 'audio/wav');
                break;

            case 'mp4':
            case 'm4e':
                $this->addHeader('Content-Type', 'video/mpeg4');
                break;

            case 'mpg':
            case 'mpeg':
                $this->addHeader('Content-Type', 'video/mpg');
                break;

            case 'm3u':
            case 'm3u8':
                $this->addHeader('Content-Type', 'audio/x-mpegurl');
                break;
            default:
                break;
        }

        $this->addHeader('Charset', $this->charset);
        $this->requestType = $type;

        return $this->execute();
    }

    /**
     * Post 请求
     *
     * @param string $url
     * @param string $type
     *
     * @return \mark\response\Response
     */
    public function post(string $url, $type = ''): \mark\response\Response {
        $this->method = 'post';
        $this->initialize($url);
        switch ($type) {
            case 'text':
                $this->addHeader('Content-Type', 'text/plain');
                break;
            case 'json':
                $this->addHeader('Content-Type', 'application/json');
                break;
            case 'multipart':
            case 'formdata':
                $this->addHeader('Content-Type', 'multipart/form-data');
                break;
            default:
                $this->addHeader('Content-Type', 'application/x-www-form-urlencoded');
                break;
        }

        $this->requestType = $type;

        return $this->execute();
    }

    /**
     * Head 请求
     *
     * @param string $url
     * @param string $type
     *
     * @return \mark\response\Response
     */
    public function head(string $url, $type = ''): \mark\response\Response {
        $this->method = 'head';
        $this->initialize($url);
        switch ($type) {
            case 'text':
                $this->addHeader('Content-Type', 'text/plain');
                break;
            case 'json':
                $this->addHeader('Content-Type', 'application/json');
                break;
            case 'multipart':
            case 'formdata':
                $this->addHeader('Content-Type', 'multipart/form-data');
                break;
            default:
                $this->addHeader('Content-Type', 'application/x-www-form-urlencoded');
                break;
        }

        $this->requestType = $type;

        return $this->execute();
    }

    /**
     * 上传文件
     *
     * @param string $url
     * @param array  $data
     *
     * @return \mark\response\Response
     */
    public function upload(string $url, array $data = array()): \mark\response\Response {
        $this->method = 'upload';
        $this->initialize($url);
        $this->addHeader('Content-Type', 'multipart/form-data');
        $this->addHeader('Charset', $this->charset);
        $this->requestType = 'multipart';

        $this->append($data);

        return $this->execute();
    }

    /**
     * 下载文件
     *
     * @param string $url
     * @param string $savePath
     * @param string $fileName
     * @param string $extension
     *
     * @return \mark\response\Response
     */
    public function download(string $url, string $savePath = '', string $fileName = '', string $extension = ''): \mark\response\Response {
        $this->method = 'download';
        $this->initialize($url);
        $this->addHeader('Content-Type', 'application/octet-stream');
        $this->addHeader('Charset', $this->charset);
        $this->requestType = 'stream';
        // 设置文件保存路径
        $this->setFilePath($savePath);

        //保存文件名
        $this->setFileName($fileName);

        // 文件后缀
        $this->setFileExtension($extension);

        return $this->execute();
    }

    /**
     * Put 请求
     *
     * @param string $url
     *
     * @return \mark\response\Response
     */
    public function put(string $url): \mark\response\Response {
        $this->method = 'put';
        $this->initialize($url);

        return $this->execute();
    }

    /**
     * Delete 请求
     *
     * @param string $url
     *
     * @return \mark\response\Response
     */
    public function delete(string $url): \mark\response\Response {
        $this->method = 'delete';
        $this->initialize($url);

        return $this->execute();
    }

    /**
     * FTP 请求
     *
     * @param string $url
     *
     * @return \mark\response\Response
     */
    public function ftp(string $url): \mark\response\Response {
        $this->method = 'ftp';
        $this->initialize($url);

        return $this->execute();
    }

    /**
     * 设置文件保存路径
     *
     * @param string $filepath
     *
     * @return $this
     */
    public function setFilePath(string $filepath = ''): self {
        if (!empty($filepath)) {
            $this->filePath = trim($filepath);
        }

        return $this;
    }

    /**
     * 获取文件路径
     *
     * @return string
     * @throws \mark\exception\UnexpectedValueException
     */
    public function getFilePath(): string {
        if (file_exists($this->filePath)) {
            return $this->filePath;
        }

        if (!mkdir($directory = $this->filePath, 0777, true) && !is_dir($directory)) {
            throw new UnexpectedValueException(sprintf('Directory "%s" was not created', $directory));
        }

        return $this->filePath;
    }

    /**
     * 设置文件名
     *
     * @param string $filename
     *
     * @return $this
     */
    public function setFileName(string $filename = ''): self {
        if (!empty(trim($filename))) {
            $this->fileName = trim($filename);
        }

        return $this;
    }

    /**
     * 获取文件名
     *
     * @param bool $complete
     *
     * @return string
     */
    public function getFileName(bool $complete = true): string {
        if (!empty($this->fileName)) {
            return $this->fileName . ($complete ? '.' . $this->getFileExtension() : '');
        }

        // 后缀为空则使用文件名后缀
        $filename = pathinfo($this->fileName, PATHINFO_BASENAME);
        if (!empty($filename)) {
            return $filename . '.' . $this->getFileExtension();
        }

        // 文件名后缀为空则使用Url后缀
        $filename = pathinfo($this->url, PATHINFO_BASENAME);
        if (!empty($filename)) {
            return $filename . '.' . $this->getFileExtension();
        }

        try {
            return 'unknown_' . time() . '_' . random_int(1000, 9999) . '.' . $this->getFileExtension();
        } catch (\Exception $e) {
            return 'unknown_' . time() . '_' . rand(1000, 9999) . '.' . $this->getFileExtension();
        }
    }

    /**
     * 设置文件扩展名
     *
     * @param string $extension
     *
     * @return $this
     */
    public function setFileExtension(string $extension = ''): self {
        if (!empty(trim($extension))) {
            $this->fileExtension = trim($extension);
        }

        return $this;
    }

    /**
     *获取文件后缀
     *
     * @return string|string[]
     */
    public function getFileExtension() {
        if (!empty($this->fileExtension)) {
            return $this->fileExtension;
        }

        // 后缀为空则使用文件名后缀
        $extension = pathinfo($this->fileName, PATHINFO_EXTENSION);
        if (!empty($extension)) {
            return $extension !== 'jpeg' ? $extension : 'jpg';
        }

        // 文件名后缀为空则使用Url后缀
        $extension = pathinfo($this->url, PATHINFO_EXTENSION);
        if (!empty($extension)) {
            return $extension !== 'jpeg' ? $extension : 'jpg';
        }

        $imginfo = getimagesize($this->url);
        $extension = trim(strrchr($imginfo['mime'], '/'), '/');

        return $extension !== 'jpeg' ? $extension : 'jpg';
    }

    /**
     * 添加请求数据，后添加会覆盖之后添加的数据
     *
     * @param $data
     *
     * @return $this
     */
    public function append($data): self {
        if (!empty($data)) {
            if (!empty($this->formData)) {
                $this->formData = array_merge($this->formData, $data);
            } else {
                $this->formData = $data;
            }
        }

        return $this;
    }

    /**
     * 添加请求数据，后添加会覆盖之后添加的数据
     *
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function appendData($key, $value): self {
        if (!empty($key)) {
            $this->formData[$key] = $value;
        }

        return $this;
    }

    /**
     * 添加请求Json数据，后添加会覆盖之后添加的数据
     *
     * @param $value
     *
     * @return $this
     */
    public function appendJson($value): self {
        if (!empty($value)) {
            $this->formData = $value;
        }

        return $this;
    }

    /**
     * 递归添加数据
     *
     * @param $data
     *
     * @return $this
     */
    public function appendRecursive($data): self {
        if (!empty($data)) {
            $this->formData = array_merge_recursive($this->formData, $data);
        }

        return $this;
    }

    /**
     * 向指定字段添加数据
     *
     * @param $key
     * @param $field
     * @param $value
     *
     * @return $this
     */
    public function appendPush($key, $field, $value): self {
        if (!empty($value)) {
            $this->formData[$key][$field] = $value;
        }

        return $this;
    }

    /**
     * 追加数据
     *
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function push($key, $value): self {
        if (!empty($key)) {
            $this->formData[$key][] = $value;
        }

        return $this;
    }

    /**
     * 添加文件
     *
     * @param string $file
     *
     * @return $this
     * @throws \mark\exception\InvalidArgumentException
     */
    public function appendFile(string $file): self {
        return $this->appendFiles('file', $file);
    }

    /**
     * 添加多个文件
     *
     * @param string $key
     * @param string $path
     *
     * @return $this
     * @throws \mark\exception\InvalidArgumentException
     */
    public function appendFiles(string $key, string $path): self {
        if (!is_file($path) || !is_readable($path) || !file_exists(realpath($path))) {
            throw new InvalidArgumentException(sprintf('The "%s" file does not exist or is not readable.', $path));
        }

        $mime = '';
        if (\function_exists('fileinfo')) {
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->file(realpath($path));
        }

        $this->fileData[] = curl_file_create(realpath($path), $mime, basename($path));
        $this->upfilesize += filesize($path);

        /*
        if(!empty($key)){
        // $this->formData[$key] = $file;

        // 从php5.5开始,反对使用"@"前缀方式上传,可以使用CURLFile替代;
        // 据说php5.6开始移除了"@"前缀上传的方式
        if (class_exists("\CURLFile")){
        if(array_key_exists($key, $this->formData)){
        $newArr = array();
        array_push($newArr, $this->formData[$key]);
        // array_push($newArr, new CURLFile(realpath($file)));
        array_push($newArr, new CURLFile($file));
        // array_push($newArr, curl_file_create($file));
        $this->upfilesize+=filesize($file);

        $this->formData[$key] = $newArr;
        }else{
        $this->formData[$key] = new CURLFile(realpath($file));
        $this->upfilesize+=filesize(realpath($file));
        }
        // 禁用"@"上传方法,这样就可以安全的传输"@"开头的参数值
        // curl_setopt($this->getCurl(), CURLOPT_SAFE_UPLOAD, false);
        }else{
        $this->formData[$key] = "@".realpath($file);
        $this->upfilesize+=filesize($file);
        }
        }
        */

        return $this;
    }

    /**
     * 2、设置选项
     *
     * @param $option
     * @param $value
     *
     * @return $this
     */
    public function set($option, $value): self {
        if (!empty($option)) {
            $this->$option = $value;
        }

        return $this;
    }

    /**
     * 设置请求字符集
     *
     * @param string $charset
     *
     * @return $this
     */
    public function setCharset(string $charset = 'utf-8'): self {
        if (!empty($charset)) {
            $this->addHeader('Charset', $charset);
            $this->charset = $charset;
        }

        return $this;
    }

    /**
     * @description 传递一个连接中需要的用户名和密码
     *
     * @param string $username
     * @param string $password
     *
     * @return $this
     */
    public function setUserPassword(string $username, string $password): self {
        if (!empty($username) && !empty($password)) {
            $this->userpwd = $username . ':' . $password;
        }

        return $this;
    }

    /**
     * 设置Cookie
     *
     * @param mixed $cookie
     *
     * @return $this
     */
    public function setCookie($cookie): self {
        if (is_file($cookie) && file_exists($cookie)) {
            $this->cookie_file = $cookie;
        } elseif (is_string($cookie) && !empty($cookie)) {
            $this->cookie = $cookie;
        } elseif ((is_array($cookie) || is_object($cookie)) && !empty($cookie)) {
            foreach ($cookie as $key => $item) {
                $this->cookie .= $key . '=' . $item . ';';
            }
            $this->cookie = $cookie;
        } elseif (is_bool($cookie) && $cookie === true) {
            foreach ($_COOKIE as $key => $item) {
                $this->cookie .= $key . '=' . $item . ';';
            }
        }

        return $this;
    }

    private $callback;

    /**
     * @param callable $callback
     *
     * @return $this
     */
    public function setCallback(callable $callback): self {
        $this->callback = $callback;

        return $this;
    }

    private function each() {
        if (!empty($this->callback) && is_callable($this->callback)) {
            $response = call_user_func($this->callback, $this->response, $this);

            if ($response instanceof \mark\response\Response) {
                $this->response->setResponse($response);
            }
        }
    }

    /**
     * 外置缓存回调
     *
     * @param callable $callback
     *
     * @return $this
     * @throws \mark\http\exception\BadMessageException
     * @throws \mark\exception\InvalidArgumentException
     * @see  \mark\http\Client\Client::setCallback()
     * @deprecated
     */
    public function cache(callable $callback): self {
        $result = $callback($this->response, $this);

        if ($result instanceof \mark\response\Response) {
            $this->response->setResponse($result);
        }

        return $this;
    }

    private $cache = true;

    /**
     * @param bool $cache
     *
     * @return $this
     */
    public function setCache($cache = true): self {
        $this->cache = is_true($cache);

        return $this;
    }

    /**
     * @var \Psr\SimpleCache\CacheInterface|null
     */
    private ?CacheInterface $cache_handle = null;

    /**
     * 设置缓存实例
     *
     * @param \Psr\SimpleCache\CacheInterface $cache_handle
     *
     * @return $this
     */
    final public function setCacheHandle(CacheInterface $cache_handle): self {
        if ($cache_handle instanceof CacheInterface) {
            $this->cache_handle = new CacheHandle($cache_handle);
        }

        return $this;
    }

    /**
     * 获取缓存实例
     *
     * @return \Psr\SimpleCache\CacheInterface
     */
    final public function getCacheHandle(): CacheInterface {
        if ($this->cache_handle instanceof CacheInterface) {
            return $this->cache_handle;
        }

        return new CacheHandle();
    }

    /**
     * @var int
     */
    public $expire = 7200;

    /**
     * 设置缓存有效期
     *
     * @param int $expire
     *
     * @return $this
     */
    final public function setExpire(int $expire): self {
        $this->expire = $expire;

        return $this;
    }

    /**
     * @return int
     */
    final public function getExpire(): int {
        return $this->expire;
    }

    /**
     * 一个用来设置HTTP头字段的数组。使用如下的形式的数组进行设置
     * Add a custom HTTP header to the cURL request.
     *
     * @param string $key   (Required) The custom HTTP header to set.
     * @param mixed  $value (Required) The value to assign to the custom HTTP header.
     *
     * @return $this A reference to the current instance.
     */
    public function addHeader(string $key, $value): self {
        if (!empty($key)) {
            $this->requestHeader[$key] = $value;
        }

        return $this;
    }

    /**
     * Remove an HTTP header from the cURL request.
     *
     * @param string $key (Required) The custom HTTP header to set.
     *
     * @return $this A reference to the current instance.
     */
    public function removeHeader(string $key): self {
        if (!empty($key)) {
            if (isset($this->requestHeader[$key])) {
                unset($this->requestHeader[$key]);
            }
        }

        return $this;
    }

    /**
     * Initialize headers
     *
     * @return array
     */
    private function generateHeaders(): array {
        $headers = $this->requestHeader;

        $headers['mark-auth'] = 'OAuth2';
        $headers['mark-engine'] = 'Mark_Engine';
        $headers['mark-proxy'] = 'LibCurl';
        $headers['mark-version'] = Mark::VERSION;

        if (!isset($headers['Client-Ip'])) {
            $headers['Client-Ip'] = Os::getIPV4();
        }
        $headers['Accept'] = $headers['Accept'] ?? Os::getAccept();
        $headers['Charset'] = $headers['Charset'] ?? $this->charset;
        $headers['Cache-control'] = $headers['Cache-Control'] ?? 'no-cache';
        $headers['Pragma'] = $headers['Pragma'] ?? 'no-cache';
        $headers['Date'] = $headers['Date'] ?? gmdate('D, d M Y H:i:s \G\M\T');
        $headers['Expect'] = $headers['Expect'] ?? ' ';

        return $headers;
    }

    /**
     * 获取HTTP头字段的数组
     *
     * @return array
     */
    private function getHeader(): array {
        $headers = array();
        foreach ($this->generateHeaders() as $k => $v) {
            $headers[] = $k . ':' . $v;
        }

        return $headers;
    }

    /**
     * 获取请求头
     *
     * @alias getRequestHeaders()
     * @return array
     */
    public function getHeaders(): array {
        return $this->generateHeaders();
    }

    private $userAgent = '';

    /**
     * @param string $agent
     *
     * @return $this
     */
    public function setUserAgent($agent = ''): self {
        if (!empty($agent)) {
            $this->userAgent = $agent;
        }

        return $this;

    }

    /**
     * Get User Agent
     *
     * @return string
     */
    protected function getUserAgent(): string {
        if (!empty($this->userAgent)) {
            return $this->userAgent;
        }

        $agent = Os::getAgent(true);
        if (empty($agent)) {
            $agent = 'Mozilla/5.0 (' . PHP_OS_FAMILY . '; ' . php_uname('r') . ') AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36';
        }
        $agent .= ' Mark/' . Mark::VERSION;
        if (function_exists('curl_version')) {
            $agent .= ' Curl/' . \curl_version()['version'] ?? '';
        }
        $agent .= ' PHP/' . PHP_VERSION;

        $this->userAgent = $agent;

        return $this->userAgent;
    }

    /**
     * 提交执行
     *
     * @return \mark\response\Response
     * @see          Client::execute();
     * @deprecated   过时的，可用execute() 代替
     */
    public function commit(): \mark\response\Response {
        return $this->execute();
    }

    /**
     * 3、执行CURL请求 * execute
     *
     * @return \mark\response\Response
     */
    public function execute(): \mark\response\Response {
        if (empty($this->url)) {
            return \mark\response\Response::create('', 414, 'Domain Name Not Found', '无效的请求网址', 'json');
        }
        // 设置请求地址
        curl_setopt($this->getCurl(), CURLOPT_URL, $this->url);
        //设置请求头(可有可无)
        curl_setopt($this->getCurl(), CURLOPT_HTTPHEADER, $this->getHeader());

        curl_setopt($this->getCurl(), CURLOPT_USERAGENT, $this->getUserAgent());

        //设置cURL允许执行的最长秒数。
        if ($this->timeout > 0) {
            curl_setopt($this->getCurl(), CURLOPT_TIMEOUT, $this->timeout);
        }

        curl_setopt($this->getCurl(), CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);

        // 忽略HTTPS的安全证书
        if (1 == str_contains('$' . $this->url, 'https://')) {
            curl_setopt($this->getCurl(), CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->getCurl(), CURLOPT_SSL_VERIFYHOST, false);
        }

        curl_setopt($this->getCurl(), CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_NONE);

        if (!empty($this->userpwd)) {
            curl_setopt($this->getCurl(), CURLOPT_USERPWD, $this->userpwd);
        }

        // 让CURL支持页面连接跳转
        if ($this->location) {
            curl_setopt($this->getCurl(), CURLOPT_FOLLOWLOCATION, true);
            // 启用时会将服务器服务器返回的"Location: "放在header中递归的返回给服务器,使用CURLOPT_MAXREDIRS可以限定递归返回的数量。

            // 指定最多的HTTP重定向的数量,这个选项是和CURLOPT_FOLLOWLOCATION一起使用的
            curl_setopt($this->getCurl(), CURLOPT_MAXREDIRS, $this->maxredirs ?: 10);
        }

        // 添加来源referer
        curl_setopt($this->getCurl(), CURLOPT_REFERER, getRequestUrl());

        // 当根据Location:重定向时,自动设置header中的Referer:信息。
        curl_setopt($this->getCurl(), CURLOPT_AUTOREFERER, true);

        if ($this->cookie) {
            curl_setopt($this->getCurl(), CURLOPT_COOKIE, $this->cookie);
            if (!empty($this->cookie_file)) {
                if (!file_exists($this->cookie_file)) {
                    $fp = fopen($this->cookie_file, 'w+');
                    fclose($fp);
                }

                if (!empty($this->cookie_jar) && !file_exists($this->cookie_jar)) {
                    $fp = fopen($this->cookie_jar, 'w+');
                    fclose($fp);
                }

                curl_setopt($this->getCurl(), CURLOPT_COOKIEFILE, $this->cookie_file ?: '');
                curl_setopt($this->getCurl(), CURLOPT_COOKIEJAR, $this->cookie_jar ?: $this->cookie_file ?: '');
            }

            curl_setopt($this->getCurl(), CURLOPT_COOKIESESSION, true);
        }

        //至关重要，CURLINFO_HEADER_OUT选项可以拿到请求头信息
        if ($this->requestHeaderOut) {
            curl_setopt($this->getCurl(), CURLINFO_HEADER_OUT, true);
        }

        if ($this->responseHeader) {
            //设置响应头信息是否返回
            curl_setopt($this->getCurl(), CURLOPT_HEADER, $this->responseHeader);
        }

        // 将curl_exec()获取的信息以文件流的形式返回,而不是直接输出。(自定义默认不输出)
        if ($this->transfer) {
            curl_setopt($this->getCurl(), CURLOPT_RETURNTRANSFER, $this->transfer);
        }

        switch ($this->getMethod()) {
            case 'head':
                //设置为Head请求
                curl_setopt($this->getCurl(), CURLOPT_HEADER, true);          // header will be at output
                curl_setopt($this->getCurl(), CURLOPT_CUSTOMREQUEST, 'HEAD'); // HTTP request is 'HEAD'
                // 重新为Url添加Get参数
                if (!empty($this->url) && !empty($this->formData)) {
                    $this->url .= (false === strpos($this->url, '?') ? '?' : '&') . http_build_query($this->formData);
                    curl_setopt($this->getCurl(), CURLOPT_URL, $this->url);
                }

                if (is_true($this->cache)) {
                    try {
                        return $this->getCacheData();
                    } catch (InvalidArgumentException $e) {
                        // InvalidArgumentException
                    }
                }
                break;
            case 'get':
                //设置为get请求
                curl_setopt($this->getCurl(), CURLOPT_HTTPGET, true);

                // 重新为Url添加Get参数
                if (!empty($this->url) && !empty($this->formData)) {
                    $this->url .= (false === strpos($this->url, '?') ? '?' : '&') . http_build_query($this->formData);
                    curl_setopt($this->getCurl(), CURLOPT_URL, $this->url);
                }

                if (is_true($this->cache)) {
                    try {
                        return $this->getCacheData();
                    } catch (InvalidArgumentException $e) {
                        // InvalidArgumentException
                    }
                }
                break;
            case 'post':
                //设置为post请求
                curl_setopt($this->getCurl(), CURLOPT_POST, true);
                if (!empty($this->formData)) {
                    switch (strtolower($this->requestType)) {
                        case 'text':
                            $this->addHeader('Content-Type', 'text/plain');
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, http_build_query($this->formData));
                            break;
                        case 'json': // json
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, json_encode($this->formData, JSON_UNESCAPED_UNICODE));
                            break;
                        case 'raw': // raw
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, json_encode($this->formData, JSON_UNESCAPED_UNICODE));
                            break;
                        case 'multipart': // multipart
                            $this->addHeader('Content-Type', 'multipart/form-data');
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, $this->formData);
                            break;
                        case 'formdata':
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, $this->formData);
                            break;
                        default:
                            curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, http_build_query($this->formData));
                            break;
                    }
                }
                break;
            case 'upload':
                curl_setopt($this->getCurl(), CURLOPT_CUSTOMREQUEST, 'POST');
                // curl_setopt($this->getCurl(), CURLOPT_CONNECTTIMEOUT, 60);

                // 接收通过浏览器上传的文件
                curl_setopt($this->getCurl(), CURLOPT_POST, 1); //设置为post请求

                // curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, http_build_query($this->formData));
                // curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, $this->formData);
                // curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, $this->fileData);

                // curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, array_merge($this->fileData, http_build_query($this->formData)));
                curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, array_merge($this->fileData, $this->formData));

                // curl_setopt($this->getCurl(), CURLOPT_PUT, 1);
                // curl_setopt($this->getCurl(), CURLOPT_INFILE, realpath($file));
                // curl_setopt($this->getCurl(), CURLOPT_INFILE, $this->formData);
                // curl_setopt($this->getCurl(), CURLOPT_INFILESIZE, $this->upfilesize);

                // 启用时允许文件传输
                // curl_setopt($this->getCurl(), CURLOPT_UPLOAD, true);

                /*
                if (isset($this->read_stream)) {
                    if (!isset($this->read_stream_size) || $this->read_stream_size < 0) {
                        throw new RequestCore_Exception("The stream size for the streaming upload cannot be determined.");
                    }

                    curl_setopt($this->getCurl(), CURLOPT_INFILESIZE, $this->read_stream_size);
                    curl_setopt($this->getCurl(), CURLOPT_UPLOAD, true);
                } else {
                    curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, $this->formData);
                }
                */
                break;
            case 'put':
                //定义请求类型
                curl_setopt($this->getCurl(), CURLOPT_CUSTOMREQUEST, 'PUT');
                //定义提交的数据
                curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, http_build_query($this->formData));
                break;
            case 'download':
                //定义请求类型
                // curl_setopt($this->getCurl(), CURLOPT_CUSTOMREQUEST, "put");
                //定义提交的数据
                // curl_setopt($this->getCurl(), CURLOPT_POSTFIELDS, http_build_query($this->formData));

                // 将传输结果作为curl_exec的返回值,而不是直接输出
                curl_setopt($this->getCurl(), CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->getCurl(), CURLOPT_VERBOSE, 1);

                // $this->outfile = fopen("desc.txt", "wb");
                // $this->outfile = fopen($this->filePath . "/" . $this->fileName, "wb");
                // curl_setopt($this->getCurl(), CURLOPT_FILE, $this->outfile);
                break;
            default:
                break;
        }

        $response = curl_exec($this->getCurl());
        $responseCode = curl_getinfo($this->getCurl(), CURLINFO_HTTP_CODE);

        // 检查是否有错误发生
        $this->errno = curl_errno($this->getCurl());
        $this->errmsg = curl_error($this->getCurl());

        if ($this->errno) {
            $this->errmsg = curl_strerror($this->errno);
            return \mark\response\Response::create('', $this->errno ?: $responseCode, 'Curl Error：' . $this->errmsg, $this->errmsg, 'json');
        }

        if (false === $response || is_bool($response)) {
            return \mark\response\Response::create('', $this->errno ?: $responseCode, 'Curl Error：' . $this->errmsg, $this->errmsg, 'json');
        }

        // 检查是否需要获取响应头
        if ($this->responseHeader) {
            // 获得响应结果里的：头大小
            $this->responseHeaderSize = curl_getinfo($this->getCurl(), CURLINFO_HEADER_SIZE);
            // 根据头大小去获取头信息内容
            $this->responseHeaderContent = trim(substr($response, 0, $this->responseHeaderSize));
            $body = substr($response, $this->responseHeaderSize);
            if (false !== $body) {
                $response = $body;
            }
        }

        if ($this->getMethod() === 'download') {
            try {
                $file = $this->getFilePath() . DIRECTORY_SEPARATOR . $this->getFileName();
                // 保存文件到指定路径
                $bytes = file_put_contents($file, $response);
                if (false === $bytes) {
                    $this->errmsg[] = '写入文件失败';
                    return \mark\response\Response::create('', 415, 'File write failure', '文件写入失败', 'json');
                }

                if (!is_file($file)) {
                    $this->errmsg[] = '保存文件失败';
                    return \mark\response\Response::create('', 415, 'File save failed', '保存文件失败', 'json');
                }

                $md5_file = md5_file($file);
                $response = array('url' => $this->url,
                                  'path' => $this->getFilePath(),
                                  'title' => pathinfo($this->url, PATHINFO_BASENAME),
                                  'name' => $this->getFileName(),
                                  'size' => $bytes,
                                  'extension' => $this->getFileExtension(),
                                  'md5' => $md5_file != false ? $md5_file : '',
                                  'hash' => hash_file('sha256', $file));
            } catch (UnexpectedValueException $e) {
                $this->errmsg[] = $e->getMessage();
            }
        }

        $this->response = \mark\response\Response::create($response, $responseCode);
        if ($this->responseHeader) {
            $this->response->addHeaders($this->getResponseHeader(true), true);
        }

        $this->each();

        try {
            $this->setCacheData();
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
        }

        $this->response->setRequestUrl($this->url);
        $this->response->setRequestMethod($this->getMethod());

        return $this->response;
    }

    private $cacheKey = '';

    /**
     * @param string $key
     *
     * @return $this
     */
    public function setCacheKey(string $key): self {
        if (!empty($key)) {
            $this->cacheKey = $key;
        }

        return $this;
    }

    /**
     * @return string
     */
    private function getCacheKey(): string {
        if (!empty($this->cacheKey)) {
            return $this->cacheKey;
        }

        return 'mark:http:' . Os::getIpvs() . ':agent:' . hash('sha256', Os::getAgent()) . ':uri:' . hash('sha256', $this->url);
    }

    /**
     * 将HTTP请求写入缓存
     *
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function setCacheData(): bool {
        if ($this->getMethod() == 'get' && $this->response->getResponseCode() == 200 && !$this->response->isEmpty()) {
            $data = [
                'url' => $this->url,
                'data' => $this->response->getData(),
                'code' => $this->response->getResponseCode(),
                'status' => $this->response->getResponseStatus(),
                'reason' => $this->response->getResponseReason(),
                'type' => $this->response->getResponseType(),
                'headers' => $this->response->getHeaders()];

            $content = $this->response->toArray();
            if (isset($content['data']) && isset($content['code']) && is_numeric($content['code'] ?? '')) {
                if (!empty($content['data'] ?? '') && !empty($content['code'] ?? '') && $content['code'] == 200) {
                    // 缓存API请求
                    // return $this->getCacheHandle()->set($this->getCacheKey(), $data, $this->getExpire());
                }
            } else {
                // 缓存普通Get请求
                // return $this->getCacheHandle()->set($this->getCacheKey(), $this->response->getData(), $this->getExpire());
            }

            if (!empty($content['data'] ?? '') && !empty($content['code'] ?? '') && $content['code'] == 200) {
                // 缓存API请求
                return $this->getCacheHandle()->set($this->getCacheKey(), $data, $this->getExpire());
            }

            // 缓存普通Get请求
            return $this->getCacheHandle()->set($this->getCacheKey(), $this->response->getData(), $this->getExpire());
        }

        // API 查询失败不缓存
        return $this->clearCache();
    }

    /**
     * 从缓存中获取数据
     *
     * @return \mark\response\Response
     */
    private function getCacheData(): \mark\response\Response {
        $data = '';
        try {
            if ($this->getCacheHandle()->has($this->getCacheKey())) {
                $data = $this->getCacheHandle()->get($this->getCacheKey());
            }
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
        }

        if (empty($data)) {
            throw new InvalidArgumentException('No Content', 204);
        }

        if (!is_array($data)) {
            // 返回普通请求缓存数据
            $response = \mark\response\Response::create($data);
        } elseif (!empty($data['code'] ?? '') && $data['code'] == 200) {
            // 返回API请求缓存数据
            $response = \mark\response\Response::create($data['data'] ?? '', 200, $data['status'] ?? '', $data['reason'] ?? '', $data['type'] ?? '');
        } else {
            throw new InvalidArgumentException('Response code must be 200', 416);
        }

        if (!empty($data['headers'] ?? '') && is_array($data['headers'])) {
            $response->addHeaders($data['headers'], true);
            // $response->setHeaders($data['headers']);
        }

        // $response->setHeader('source', 'cache');
        $response->addHeader('source', 'cache');
        $response->setRequestUrl($this->url);
        $response->setRequestMethod('get');

        return $response;
    }

    /**
     * 清空缓存
     *
     * @return false
     */
    public function clearCache(): bool {
        try {
            $this->getCacheHandle()->set($this->getCacheKey(), '', 1);
            return $this->getCacheHandle()->delete($this->getCacheKey());
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
        }

        return false;
    }

    /**
     * @return \mark\response\Response
     */
    public function getResponse(): \mark\response\Response {
        if (empty($this->response)) {
            $this->execute();
        }

        return $this->response;
    }

    /**
     * 获取数据
     *
     * @return array
     */
    public function getFormData(): array {
        return $this->formData;
    }

    /**
     * 获取请求响应头
     *
     * @param bool $complete
     *
     * @return array|string
     */
    public function getResponseHeader(bool $complete = false) {
        if (empty($this->response)) {
            try {
                $this->execute();
            } catch (ResponseException $e) {
            }
        }

        if (!empty($this->responseHeaderContent) && $complete) {
            $header = str_ireplace('=', ':', trim(str_ireplace("\r\n", ';', trim($this->responseHeaderContent))));
            $header_array = explode(';', trim($header));
            if (false == $header_array) {
                return $header ?: array();
            }

            $headers = [];
            foreach ($header_array as $index => $item) {
                $value = explode(':', $item);
                $key = trim($value[0] ?? $index);
                if (empty($key)) {
                    $headers[] = trim($value[1] ?? '');
                } elseif (strcasecmp($key, 'set-cookie') != 0) {
                    $headers[$key] = trim($value[1] ?? '');
                }
            }

            ksort($headers);
            return $headers;
        }

        return trim($this->responseHeaderContent);
    }

    /**
     * 注意：如果当该对象多次调用后，返回的结果为最后一次调用时请求的响应头大小
     *
     * @return int
     */
    public function getResponseHeaderSize(): int {
        if (empty($this->response)) {
            try {
                $this->execute();
            } catch (ResponseException $e) {
            }
        }

        return $this->responseHeaderSize;
    }

    /**
     * 获取一个cURL连接资源句柄的信息。
     *
     * @param null $option [optional]
     *
     * @return mixed (which correspond to opt):
     * @SEE CURLOPT_URL
     * @SEE CURLOPT_CONTENT_TYPE
     * @SEE CURLOPT_HTTP_CODE
     * @SEE CURLOPT_HEADER_SIZE
     * @SEE CURLOPT_REQUEST_SIZE
     * @SEE CURLOPT_FILETIME
     * @SEE CURLOPT_SSL_VERIFY_RESULT
     * @SEE CURLOPT_REDIRECT_COUNT
     * @SEE CURLOPT_TOTAL_TIME
     * @SEE CURLOPT_NAMELOOKUP_TIME
     * @SEE CURLOPT_CONNECT_TIME
     * @SEE CURLOPT_PRETRANSFER_TIME
     * @SEE CURLOPT_SIZE_UPLOAD
     * @SEE CURLOPT_SIZE_DOWNLOAD
     * @SEE CURLOPT_SPEED_DOWNLOAD
     * @SEE CURLOPT_SPEED_UPLOAD
     * @SEE CURLOPT_DOWNLOAD_CONTENT_LENGTH
     * @SEE CURLOPT_UPLOAD_CONTENT_LENGTH
     * @SEE CURLOPT_STARTTRANSFER_TIME
     * @SEE CURLOPT_REDIRECT_TIME
     */
    public function getInfo($option = null) {
        if (empty($this->response)) {
            try {
                $this->execute();
            } catch (ResponseException $e) {
            }
        }

        if ($option !== null && is_int($option)) {
            return curl_getinfo($this->getCurl(), $option);
        }

        return curl_getinfo($this->getCurl());
    }

    /**
     * Reset Curl
     *
     * @access public
     * @return $this
     */
    public function reset(): self {
        if (function_exists('curl_reset') && is_resource($this->curl)) {
            curl_reset($this->curl);
        } else {
            $this->curl = curl_init();
        }

        $this->initialize($this->url);

        return $this;
    }

    /**
     * 获取Curl的实例化对象
     *
     * @return \CurlHandle|false|resource
     */
    public function getCurl() {
        if (empty($this->curl)) {
            if (!empty($this->url)) {
                $this->curl = curl_init($this->url) or die('Curl初始化失败');
            } else {
                $this->curl = curl_init() or die('Curl初始化失败');
            }
        }

        return $this->curl;
    }

    /**
     * 返回一个保护当前会话最近一次错误的字符串。
     *
     * @return string
     */
    public function getCurlError(): string {
        if (empty($this->response)) {
            try {
                $this->execute();
            } catch (ResponseException $e) {
                return $e->getMessage();
            }
        }

        return curl_error($this->getCurl());
    }

    /**
     * 返回当前会话请求错误代码。
     *
     * @return array
     */
    public function getError(): array {
        if (empty($this->response)) {
            try {
                $this->execute();
            } catch (ResponseException $e) {
                return array('errmsg' => $e->getMessage(), 'errcode' => $e->getCode());
            }
        }

        return array('errno' => $this->errno, 'errmsg' => $this->errmsg);
    }

    /**
     * 注意：如果当该对象多次调用后，返回的结果为最后一次调用时请求的方法
     *
     * @param bool $complete
     *
     * @return string
     */
    public function getMethod(bool $complete = false): string {
        return $complete ? strtolower($this->method) : $this->method;
    }

    /**
     * 4、关闭cURL资源,并且释放系统资源
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * 关闭curl句柄
     */
    private function close(): void {
        if (!empty($this->outfile)) {
            fclose($this->outfile);
        }

        if (!empty($this->curl)) {
            curl_close($this->curl);
        }
    }

    /**
     * 设置中间传递数据
     *
     * @param string $key
     * @param        $value
     */
    public function __set(string $key, $value): void {
        $this->$key = $value;
    }

    /**
     * 获取中间传递数据的值
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get(string $key) {
        return $this->$key;
    }

    /**
     * 检测中间传递数据的值
     *
     * @param string $key
     *
     * @return bool
     */
    public function __isset(string $key): bool {
        return isset($key);
    }

    /**
     * @param $method
     * @param $params
     *
     * @return $this
     */
    public function __call($method, $params): self {
        $key = lcfirst(substr($method, 3));

        switch (substr($method, 0, 3)) {
            case 'get':
                return $this->$key;
            // not break;
            case 'set':
                $this->$key = $params[0];
                break;
            case 'uns':
                unset($this->$key);
                break;
            case 'has':
                // return isset($this->$key);
                break;
        }

        return $this;
    }

    /**
     * 调用静态方法
     *
     * @param $method
     * @param $params
     *
     * @return mixed
     */
    public static function __callStatic($method, $params) {
        try {
            switch (strtolower($method)) {
                case 'options':
                case 'head':
                case 'get':
                case 'post':
                case 'put':
                case 'delete':
                case 'trace':
                case 'connect':
                    return (new self())->$method($params);
                // not break;
            }

            return call_user_func_array([new self(), $method], $params);
        } catch (FunctionNotFoundException $e) {
            return call_user_func_array($method, $params);
        }
    }
}