<?php
declare (strict_types=1);

namespace mark\http\Client;

/**
 * The http\Client\Request class provides an HTTP message implementation tailored to represent a request message to be sent by the client.
 *
 * @See \mark\http\Client::enqueue().
 */
class Request extends \mark\http\Message {
    /**
     * Array of options for this request, which override client options.
     *
     * @var array
     */
    protected $options = null;

    /**
     * Create a new client request message to be enqueued and sent by http\Client.
     *
     * @param string             $method  The request method.
     * @param string             $url     The request URL.
     * @param array              $headers HTTP headers.
     * @param \mark\http\Message\Body $body    Request body.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\exception\UnexpectedValueException
     */
    public function __construct(string $method = null, string $url = null, array $headers = null, \mark\http\Message\Body $body = null) { }

    /**
     * Add querystring data.
     *
     * @See \mark\http\Client\Request::setQuery()
     * @See \mark\http\Message::setRequestUrl().
     *
     * @param mixed $query_data Additional querystring data.
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\http\exception\BadQueryStringException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addQuery($query_data) { }

    /**
     * Add specific SSL options.
     *
     * @See \mark\http\Client\Request::setSslOptions()
     * @See \mark\http\Client\Request::setOptions()
     * @See \mark\http\Client\Curl\$ssl options.
     *
     * @param array $ssl_options Add this SSL options.
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function addSslOptions(array $ssl_options = null) { }

    /**
     * Extract the currently set "Content-Type" header.
     *
     * @See \mark\http\Client\Request::setContentType().
     * @return string|null string the currently set content type.
     *         or NULL if no "Content-Type" header is set.
     */
    public function getContentType() { }

    /**
     * Get priorly set options.
     *
     * @See \mark\http\Client\Request::setOptions().
     * @return array options.
     */
    public function getOptions() { }

    /**
     * Retrieve the currently set querystring.
     *
     * @return string|null string the currently set querystring.
     *         or NULL if no querystring is set.
     */
    public function getQuery() { }

    /**
     * Retrieve priorly set SSL options.
     * See http\Client\Request::getOptions() and http\Client\Request::setSslOptions().
     *
     * @return array SSL options.
     */
    public function getSslOptions() { }

    /**
     * Set the MIME content type of the request message.
     *
     * @param string $content_type The MIME type used as "Content-Type".
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setContentType(string $content_type) { }

    /**
     * Set client options.
     *
     * @See \mark\http\Client::setOptions() and http\Client\Curl.
     * Request specific options override general options which were set in the client.
     * > ***NOTE:***
     * > Only options specified prior enqueueing a request are applied to the request.
     *
     * @param array $options The options to set.
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setOptions(array $options = null) { }

    /**
     * (Re)set the querystring.
     *
     * @See \mark\http\Client\Request::addQuery()
     * @See \mark\http\Message::setRequestUrl().
     *
     * @param mixed $query_data
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\http\exception\BadQueryStringException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setQuery($query_data) { }

    /**
     * Specifically set SSL options.
     *
     * @See \mark\http\Client\Request::setOptions()
     * @See \mark\http\Client\Curl\$ssl options.
     *
     * @param array $ssl_options Set SSL options to this array.
     *
     * @return \mark\http\Client\Request self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setSslOptions(array $ssl_options = null) { }
}