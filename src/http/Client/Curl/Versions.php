<?php
declare (strict_types=1);

namespace mark\http\Client\Curl;

/**
 * Class Versions
 *
 * @package mark\http\Client\Curl
 */
class Versions {
    /**
     * Version string of libcurl, e.g. "7.50.0".
     */
    public const CURL = '7.64.0';
    /**
     * Version string of the SSL/TLS library, e.g. "OpenSSL/1.0.2h".
     */
    public const SSL = 'OpenSSL/1.1.1b';
    /**
     * Version string of the zlib compression library, e.g. "1.2.8".
     */
    public const LIBZ = '1.2.11';
    /**
     * Version string of the c-ares library, e.g. "1.11.0".
     */
    public const ARES = null;
    /**
     * Version string of the IDN library, e.g. "1.32".
     */
    public const IDN = null;
}