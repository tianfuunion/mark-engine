<?php
declare (strict_types=1);

namespace mark\http\Client\Curl;

/**
 * CURL feature constants.
 * > ***NOTE:***
 * > These constants have been added in v2.6.0, resp. v3.1.0.
 *
 * @package mark\http\Client\Curl
 */
class Features {
    /**
     * Whether libcurl supports asynchronous domain name resolution.
     */
    public const ASYNCHDNS = 128;
    /**
     * Whether libcurl supports the Generic Security Services Application Program Interface. Available if libcurl is v7.38.0 or more recent.
     */
    public const GSSAPI = 131072;
    /**
     * Whether libcurl supports HTTP Generic Security Services negotiation.
     */
    public const GSSNEGOTIATE = 32;
    /**
     * Whether libcurl supports the HTTP/2 protocol. Available if libcurl is v7.33.0 or more recent.
     */
    public const HTTP2 = 65536;
    /**
     * Whether libcurl supports international domain names.
     */
    public const IDN = 1024;
    /**
     * Whether libcurl supports IPv6.
     */
    public const IPV6 = 1;
    /**
     * Whether libcurl supports the old Kerberos protocol.
     */
    public const KERBEROS4 = 2;
    /**
     * Whether libcurl supports the more recent Kerberos v5 protocol. Available if libcurl is v7.40.0 or more recent.
     */
    public const KERBEROS5 = 262144;
    /**
     * Whether libcurl supports large files.
     */
    public const LARGEFILE = 512;
    /**
     * Whether libcurl supports gzip/deflate compression.
     */
    public const LIBZ = 8;
    /**
     * Whether libcurl supports the NT Lan Manager authentication.
     */
    public const NTLM = 16;
    /**
     * Whether libcurl supports NTLM delegation to a winbind helper. Available if libcurl is v7.22.0 or more recent.
     */
    public const NTLM_WB = 32768;
    /**
     * Whether libcurl supports the Public Suffix List for cookie host handling. Available if libcurl is v7.47.0 or more recent.
     */
    public const PSL = 1048576;
    /**
     * Whether libcurl supports the Simple and Protected GSSAPI Negotiation Mechanism.
     */
    public const SPNEGO = 256;
    /**
     * Whether libcurl supports SSL/TLS protocols.
     */
    public const SSL = 4;
    /**
     * Whether libcurl supports the Security Support Provider Interface.
     */
    public const SSPI = 2048;
    /**
     * Whether libcurl supports TLS Secure Remote Password authentication. Available if libcurl is v7.21.4 or more recent.
     */
    public const TLSAUTH_SRP = 16384;
    /**
     * Whether libcurl supports connections to unix sockets. Available if libcurl is v7.40.0 or more recent.
     */
    public const UNIX_SOCKETS = 524288;
    /**
     * CURL version constants.
     * > ***NOTE:***
     * > These constants have been added in v2.6.0, resp. v3.1.0.
     */
}