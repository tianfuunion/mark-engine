<?php
declare (strict_types=1);

namespace mark\http\Client;

/**
 * The http\Client\Response class represents an HTTP message the client returns as answer from a server to an http\Client\Request.
 */
class Response extends \mark\http\Message {
    /**
     * Extract response cookies.
     * Parses any "Set-Cookie" response headers into an http\Cookie list.
     *
     * @See \mark\http\Cookie::__construct().
     *
     * @param int   $flags          Cookie parser flags.
     * @param array $allowed_extras List of keys treated as extras.
     *
     * @return array list of http\Cookie instances.
     */
    public function getCookies(int $flags = 0, array $allowed_extras = null) { }

    /**
     * Retrieve transfer related information after the request has completed.
     *
     * @See \mark\http\Client::getTransferInfo().
     *
     * @param string $name A key to retrieve out of the transfer info.
     *
     * @return object|mixed object stdClass instance with all transfer info if $name was not given.
     *         or mixed the specific transfer info for $name.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getTransferInfo(string $name = null) { }
}