<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The http extension's Exception interface.
 * Use it to catch any Exception thrown by pecl/http.
 * The individual exception classes extend their equally named native PHP extensions,
 * if such exist, and implement this empty interface.
 * For example the http\Exception\BadMethodCallException extends SPL's BadMethodCallException.
 */
interface Exception { }