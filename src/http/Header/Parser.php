<?php
declare (strict_types=1);

namespace mark\http\Header;

/**
 * The parser which is underlying http\Header and http\Message.
 * > ***NOTE:***
 * > This class has been added in v2.3.0.
 */
class Parser {
    /**
     * Finish up parser at end of (incomplete) input.
     */
    public const CLEANUP = 1;
    /**
     * Parse failure.
     */
    public const STATE_FAILURE = -1;
    /**
     * Expecting HTTP info (request/response line) or headers.
     */
    public const STATE_START = 0;
    /**
     * Expecting a key or already parsing a key.
     */
    public const STATE_KEY = 1;
    /**
     * Expecting a value or already parsing the value.
     */
    public const STATE_VALUE = 2;
    /**
     * At EOL of an header, checking whether a folded header line follows.
     */
    public const STATE_VALUE_EX = 3;
    /**
     * A header was completed.
     */
    public const STATE_HEADER_DONE = 4;
    /**
     * Finished parsing the headers.
     * > ***NOTE:***
     * > Most of this states won't be returned to the user, because the parser immediately jumps to the next expected state.
     */
    public const STATE_DONE = 5;

    /**
     * Retrieve the current state of the parser.
     * See http\Header\Parser::STATE_* constants.
     *
     * @return int http\Header\Parser::STATE_* constant.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getState() { }

    /**
     * Parse a string.
     *
     * @param string $data   The (part of the) header to parse.
     * @param int    $flags  Any combination of [parser flags](http/Header/Parser#Parser.flags:).
     * @param array &$header Successfully parsed headers.
     *
     * @return int http\Header\Parser::STATE_* constant.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function parse(string $data, int $flags, array &$header = null) { }

    /**
     * Parse a stream.
     *
     * @param resource $stream  The header stream to parse from.
     * @param int      $flags   Any combination of [parser flags](http/Header/Parser#Parser.flags:).
     * @param array &  $headers The headers parsed.
     *
     * @return int http\Header\Parser::STATE_* constant.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function stream($stream, int $flags, array &$headers) { }
}