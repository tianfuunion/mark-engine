<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class ServerException
 *
 * @description Exception when a server error is encountered (5xx codes)
 * @package     mark\http\exception
 */
class ServerException extends BadResponseException { }