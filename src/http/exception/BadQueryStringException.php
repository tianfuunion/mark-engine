<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A bad querystring was encountered.
 */
class BadQueryStringException extends \DomainException implements \mark\http\Exception { }