<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A bad HTTP header was encountered.
 */
class BadHeaderException extends \DomainException implements \mark\http\Exception { }