<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class RouteNotFoundException
 *
 * @description 未找到路由异常
 * @package     mark\http\exception
 */
class RouteNotFoundException extends HttpException {

    public function __construct() {
        parent::__construct('Route Not Found', 404);
    }

}