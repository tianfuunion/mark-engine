<?php
declare (strict_types=1);

namespace mark\http\exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ConnectException
 *
 * @description     Exception thrown when a connection cannot be established.
 * @description     Note that no response is present for a ConnectException
 * @package         mark\http\exception
 */
class ConnectException extends RequestException {
    /**
     * ConnectException constructor.
     *
     * @param                                    $message
     * @param \Psr\Http\Message\RequestInterface $request
     * @param \Exception|null                    $previous
     * @param array                              $handlerContext
     */
    public function __construct($message, RequestInterface $request, \Exception $previous = null, array $handlerContext = []) {
        parent::__construct($message, $request, null, $previous, $handlerContext);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    public function getResponse(): ?ResponseInterface {
        return parent::getResponse();
    }

    /**
     * @return bool
     */
    public function hasResponse(): bool {
        return parent::hasResponse();
    }
}