<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A generic runtime exception.
 */
class RuntimeException extends \RuntimeException implements \mark\http\Exception { }