<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A bad conversion (e.g. character conversion) was encountered.
 */
class BadConversionException extends \DomainException implements \mark\http\Exception { }