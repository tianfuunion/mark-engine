<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A bad HTTP message was encountered.
 */
class BadMessageException extends \DomainException implements \mark\http\Exception { }