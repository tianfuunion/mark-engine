<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class TransferException
 *
 * @description 传输异常
 * @package     mark\http\exception
 */
class TransferException extends \RuntimeException implements \mark\exception\HttpException { }