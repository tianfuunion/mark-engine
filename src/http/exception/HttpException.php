<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class HttpException
 *
 * @description HTTP异常
 * @package     mark\http\exception
 */
class HttpException extends \RuntimeException implements \mark\exception\HttpException {

    /**
     * @var array
     */
    private $headers;

    /**
     * HttpException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     * @param array           $headers
     */
    public function __construct($message = '', $code = 400, \Throwable $previous = null, array $headers = []) {
        $this->headers = $headers;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getHeaders(): array {
        return $this->headers;
    }

}