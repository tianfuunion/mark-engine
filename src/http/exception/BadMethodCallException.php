<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A method was called on an object, which was in an invalid or unexpected state.
 */
class BadMethodCallException extends \BadMethodCallException implements \mark\http\Exception { }