<?php
declare (strict_types=1);

namespace mark\http\exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class BadResponseException
 *
 * @description Exception when an HTTP error occurs (4xx or 5xx error)
 * @package     mark\http\exception
 */
class BadResponseException extends RequestException {

    /**
     * BadResponseException constructor.
     *
     * @param                                          $message
     * @param \Psr\Http\Message\RequestInterface       $request
     * @param \Psr\Http\Message\ResponseInterface|null $response
     * @param \Exception|null                          $previous
     * @param array                                    $handlerContext
     */
    public function __construct($message, RequestInterface $request, ResponseInterface $response = null, \Exception $previous = null, array $handlerContext = []) {
        if (null === $response) {
            @trigger_error('Instantiating the ' . __CLASS__ . ' class without a Response is deprecated since version 6.3 and will be removed in 7.0.', E_USER_DEPRECATED);
        }

        parent::__construct($message, $request, $response, $previous, $handlerContext);
    }
}