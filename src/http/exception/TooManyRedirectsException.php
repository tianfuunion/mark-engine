<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class TooManyRedirectsException
 *
 * @description 太多跳转异常
 * @package     mark\http\exception
 */
class TooManyRedirectsException extends RequestException { }