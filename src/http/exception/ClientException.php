<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * Class ClientException
 *
 * @description Exception when a client error is encountered (4xx codes)
 * @package     mark\http\exception
 */
class ClientException extends BadResponseException { }