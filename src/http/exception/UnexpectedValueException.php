<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * An unexpected value was encountered.
 */
class UnexpectedValueException extends \UnexpectedValueException implements \mark\http\Exception { }