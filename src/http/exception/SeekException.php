<?php
declare (strict_types=1);

namespace mark\http\exception;

use Psr\Http\Message\StreamInterface;

/**
 * Class SeekException
 *
 * @description Exception thrown when a seek fails on a stream.
 * @package     mark\http\exception
 */
class SeekException extends \RuntimeException implements \mark\exception\HttpException {

    /**
     * @var StreamInterface
     */
    private $stream;

    /**
     * SeekException constructor.
     *
     * @param StreamInterface $stream
     * @param int             $pos
     * @param string          $msg
     */
    public function __construct(StreamInterface $stream, $pos = 0, $msg = '') {
        $this->stream = $stream;

        parent::__construct($msg ?: 'Could not seek the stream to position ' . $pos);
    }

    /**
     * @return StreamInterface
     */
    public function getStream(): StreamInterface {
        return $this->stream;
    }
}