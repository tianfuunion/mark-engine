<?php
declare (strict_types=1);

namespace mark\http\exception;

/**
 * A bad HTTP URL was encountered.
 */
class BadUrlException extends \DomainException implements \mark\http\Exception { }