<?php
declare (strict_types=1);

namespace mark\http\exception;

use mark\response\Responsive;

/**
 * Class ResponseException
 *
 * @description HTTP响应异常类
 * @package     mark\http\exception
 */
class ResponseException extends HttpException {
    /**
     * @var array
     */
    private $responsive;

    /**
     * HttpException constructor.
     *
     * @param string                         $message
     * @param int                            $code
     * @param \Throwable|null                $previous
     * @param \mark\response\Responsive|null $responsive
     */
    public function __construct($message = '', $code = 406, \Throwable $previous = null, Responsive $responsive = null) {
        $this->responsive = $responsive;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return Responsive
     */
    public function getResponsive(): Responsive {
        return $this->responsive;
    }
}