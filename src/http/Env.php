<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The http\Env class provides static methods to manipulate and inspect the server's current request's HTTP environment.
 */
class Env {
    /**
     * Retrieve the current HTTP request's body.
     *
     * @param string $body_class_name A user class extending http\Message\Body.
     *
     * @return \mark\http\Message\Body instance representing the request body
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getRequestBody(string $body_class_name = null) { }

    /**
     * Retrieve one or all headers of the current HTTP request.
     *
     * @param string $header_name The key of a header to retrieve.
     *
     * @return string|null|array NULL if $header_name was not found
     *         or string the compound header when $header_name was found
     *         or array of all headers if $header_name was not specified
     */
    public function getRequestHeader(string $header_name = null) { }

    /**
     * Get the HTTP response code to send.
     *
     * @return int the HTTP response code.
     */
    public function getResponseCode() { }

    /**
     * Get one or all HTTP response headers to be sent.
     *
     * @param string $header_name The name of the response header to retrieve.
     *
     * @return string|array|null string the compound value of the response header to send
     *         or NULL if the header was not found
     *         or array of all response headers, if $header_name was not specified
     */
    public function getResponseHeader(string $header_name = null) { }

    /**
     * Retrieve a list of all known HTTP response status.
     *
     * @return array mapping of the form \[
     *   ...
     *   int $code => string $status
     *   ...
     *   \]
     */
    public function getResponseStatusForAllCodes() { }

    /**
     * Retrieve the string representation of specified HTTP response code.
     *
     * @param int $code The HTTP response code to get the string representation for.
     *
     * @return string the HTTP response status message (may be empty, if no message for this code was found)
     */
    public function getResponseStatusForCode(int $code) { }

    /**
     * Generic negotiator. For specific client negotiation see http\Env::negotiateContentType() and related methods.
     * > ***NOTE:***
     * > The first element of $supported serves as a default if no operand matches.
     *
     * @param string $params       HTTP header parameter's value to negotiate.
     * @param array  $supported    List of supported negotiation operands.
     * @param string $prim_typ_sep A "primary type separator", i.e. that would be a hyphen for content language negotiation (en-US, de-DE, etc.).
     * @param array &$result       Out parameter recording negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the closest match negotiated, or the default (first entry of $supported).
     */
    public function negotiate(string $params, array $supported, string $prim_typ_sep = null, array &$result = null) { }

    /**
     * Negotiate the client's preferred character set.
     * > ***NOTE:***
     * > The first element of $supported character sets serves as a default if no character set matches.
     *
     * @param array  $supported List of supported content character sets.
     * @param array &$result    Out parameter recording negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the negotiated character set.
     */
    public function negotiateCharset(array $supported, array &$result = null) { }

    /**
     * Negotiate the client's preferred MIME content type.
     * > ***NOTE:***
     * > The first element of $supported content types serves as a default if no content-type matches.
     *
     * @param array  $supported List of supported MIME content types.
     * @param array &$result    Out parameter recording negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the negotiated content type.
     */
    public function negotiateContentType(array $supported, array &$result = null) { }

    /**
     * Negotiate the client's preferred encoding.
     * > ***NOTE:***
     * > The first element of $supported encodings serves as a default if no encoding matches.
     *
     * @param array  $supported List of supported content encodings.
     * @param array &$result    Out parameter recording negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the negotiated encoding.
     */
    public function negotiateEncoding(array $supported, array &$result = null) { }

    /**
     * Negotiate the client's preferred language.
     * > ***NOTE:***
     * > The first element of $supported languages serves as a default if no language matches.
     *
     * @param array  $supported List of supported content languages.
     * @param array &$result    Out parameter recording negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the negotiated language.
     */
    public function negotiateLanguage(array $supported, array &$result = null) { }

    /**
     * Set the HTTP response code to send.
     *
     * @param int $code The HTTP response status code.
     *
     * @return bool Success.
     */
    public function setResponseCode(int $code) { }

    /**
     * Set a response header, either replacing a prior set header, or appending the new header value, depending on $replace.
     * If no $header_value is specified, or $header_value is NULL, then a previously set header with the same key will be deleted from the list.
     * If $response_code is not 0, the response status code is updated accordingly.
     *
     * @param string $header_name
     * @param mixed  $header_value
     * @param int    $response_code
     * @param bool   $replace
     *
     * @return bool Success.
     */
    public function setResponseHeader(string $header_name, $header_value = null, int $response_code = null, bool $replace = null) { }

}