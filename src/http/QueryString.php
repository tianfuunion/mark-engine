<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The \mark\http\QueryString class provides versatile facilities to retrieve, use and manipulate query strings and form data.
 */
class QueryString implements \Serializable, \ArrayAccess, \IteratorAggregate {
    /**
     * Cast requested value to bool.
     */
    public const TYPE_BOOL = 16;
    /**
     * Cast requested value to int.
     */
    public const TYPE_INT = 4;
    /**
     * Cast requested value to float.
     */
    public const TYPE_FLOAT = 5;
    /**
     * Cast requested value to string.
     */
    public const TYPE_STRING = 6;
    /**
     * Cast requested value to an array.
     */
    public const TYPE_ARRAY = 7;
    /**
     * Cast requested value to an object.
     */
    public const TYPE_OBJECT = 8;
    /**
     * The global instance. @See \mark\http\QueryString::getGlobalInstance().
     *
     * @var \mark\http\QueryString
     */
    private $instance = null;
    /**
     * The data.
     *
     * @var array
     */
    private $queryArray = null;

    /**
     * Create an independent querystring instance.
     *
     * @param mixed $params The query parameters to use or parse.
     *
     * @throws \mark\http\exception\BadQueryStringException
     */
    public function __construct($params = null) { }

    /**
     * Get the string representation of the querystring (x-www-form-urlencoded).
     *
     * @return string the x-www-form-urlencoded querystring.
     */
    public function __toString() { }

    /**
     * Retrieve an querystring value.
     *
     * @See \mark\http\QueryString::TYPE_* constants.
     *
     * @param string $name   The key to retrieve the value for.
     * @param mixed  $type   The type to cast the value to. @See \mark\http\QueryString::TYPE_* constants.
     * @param mixed  $defval The default value to return if the key $name does not exist.
     * @param bool   $delete Whether to delete the entry from the querystring after retrieval.
     *
     * @return \mark\http\QueryString|string|mixed \mark\http\QueryString if called without arguments.
     *         or string the whole querystring if $name is of zero length.
     *         or mixed $defval if the key $name does not exist.
     *         or mixed the querystring value cast to $type if $type was specified and the key $name exists.
     *         or string the querystring value if the key $name exists and $type is not specified or equals http\QueryString::TYPE_STRING.
     */
    public function get(string $name = null, $type = null, $defval = null, bool $delete = false) { }

    /**
     * Retrieve an array value with at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return array|mixed array the (casted) value.
     *         or mixed $defval if offset $name does not exist.
     */
    public function getArray(string $name, $defval = null, bool $delete = false) { }

    /**
     * Retrieve a boolean value at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return bool|mixed bool the (casted) value. or mixed $defval if offset $name does not exist.
     */
    public function getBool(string $name, $defval = null, bool $delete = false) { }

    /**
     * Retrieve a float value at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return float|mixed float the (casted) value. or mixed $defval if offset $name does not exist.
     */
    public function getFloat(string $name, $defval = null, bool $delete = false) { }

    /**
     * Retrieve the global querystring instance referencing $_GET.
     *
     * @return \mark\http\QueryString the http\QueryString::$instance
     * @throws \mark\http\exception\UnexpectedValueException
     */
    public function getGlobalInstance() { }

    /**
     * Retrieve a int value at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return int|mixed int the (casted) value.
     *         or mixed $defval if offset $name does not exist.
     */
    public function getInt(string $name, $defval = null, bool $delete = false) { }

    /**
     * Implements IteratorAggregate.
     *
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getIterator() { }

    /**
     * Retrieve a object value with at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return object|mixed object the (casted) value. or mixed $defval if offset $name does not exist.
     */
    public function getObject(string $name, $defval = null, bool $delete = false) { }

    /**
     * Retrieve a string value with at offset $name.
     *
     * @param string $name   The key to look up.
     * @param mixed  $defval The default value to return if the offset $name does not exist.
     * @param bool   $delete Whether to remove the key and value from the querystring after retrieval.
     *
     * @return string|mixed string the (casted) value. or mixed $defval if offset $name does not exist.
     */
    public function getString(string $name, $defval = null, bool $delete = false) { }

    /**
     * Set additional $params to a clone of this instance.
     *
     * @See \mark\http\QueryString::set().
     * > ***NOTE:***
     * > This method returns a clone (copy) of this instance.
     *
     * @param mixed $params Additional params as object, array or string to parse.
     *
     * @return \mark\http\QueryString clone.
     * @throws \mark\http\exception\BadQueryStringException
     */
    public function mod($params = null) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to look up.
     *
     * @return bool whether the key $name isset.
     */
    public function offsetExists($offset) { }

    /**
     * Implements ArrayAccess.
     *
     * @param mixed $offset The offset to look up.
     *
     * @return mixed|null mixed the value locate at offset $name. or NULL if key $name could not be found.
     */
    public function offsetGet($offset) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The key to set the value for.
     * @param mixed  $value  The data to place at offset $name.
     */
    public function offsetSet($offset, $value) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to look up.
     */
    public function offsetUnset($offset) { }

    /**
     * Implements Serializable.
     *
     * @See \mark\http\QueryString::toString().
     * @return string the x-www-form-urlencoded querystring.
     */
    public function serialize() { }

    /**
     * Set additional querystring entries.
     *
     * @param mixed $params Additional params as object, array or string to parse.
     *
     * @return \mark\http\QueryString self.
     */
    public function set($params) { }

    /**
     * Simply returns http\QueryString::$queryArray.
     *
     * @return array the $queryArray property.
     */
    public function toArray() { }

    /**
     * Get the string representation of the querystring (x-www-form-urlencoded).
     *
     * @return string the x-www-form-urlencoded querystring.
     */
    public function toString() { }

    /**
     * Implements Serializable.
     *
     * @param string $serialized The x-www-form-urlencoded querystring.
     *
     * @throws \mark\http\Exception
     */
    public function unserialize($serialized) { }

    /**
     * Translate character encodings of the querystring with ext/iconv.
     * > ***NOTE:***
     * > This method is only available when ext/iconv support was enabled at build time.
     *
     * @param string $from_enc The encoding to convert from.
     * @param string $to_enc   The encoding to convert to.
     *
     * @return \mark\http\QueryString self.
     * @throws \mark\http\exception\BadConversionException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function xlate(string $from_enc, string $to_enc) { }
}