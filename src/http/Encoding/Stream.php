<?php
declare (strict_types=1);

namespace mark\http\Encoding;

/**
 * Base class for encoding stream implementations.
 */
abstract class Stream {
    /**
     * Do no intermittent flushes.
     */
    public const FLUSH_NONE = 0;
    /**
     * Flush at appropriate transfer points.
     */
    public const FLUSH_SYNC = 1048576;
    /**
     * Flush at each IO operation.
     */
    public const FLUSH_FULL = 2097152;

    /**
     * Base constructor for encoding stream implementations.
     *
     * @param int $flags See http\Encoding\Stream and implementation specific constants.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\RuntimeException
     */
    public function __construct(int $flags = 0) { }

    /**
     * Check whether the encoding stream is already done.
     *
     * @return bool whether the encoding stream is completed.
     */
    public function done() { }

    /**
     * Finish and reset the encoding stream.
     * Returns any pending data.
     *
     * @return string any pending data.
     */
    public function finish() { }

    /**
     * Flush the encoding stream.
     * Returns any pending data.
     *
     * @return string any pending data.
     */
    public function flush() { }

    /**
     * Update the encoding stream with more input.
     *
     * @param string $data The data to pass through the stream.
     *
     * @return string processed data.
     */
    public function update(string $data) { }
}