<?php
declare (strict_types=1);

namespace mark\http\Encoding\Stream;

/**
 * A stream decoding data encoded with chunked transfer encoding.
 */
class Dechunk extends \mark\http\Encoding\Stream {
    /**
     * Decode chunked encoded data.
     *
     * @param string $data        The data to decode.
     * @param int &  $decoded_len Out parameter with the length of $data that's been decoded.
     *                            Should be ```strlen($data)``` if not truncated.
     *
     * @return string|false string the decoded data.
     *         or string the unencoded data.
     *         or string the truncated decoded data.
     *         or false if $data cannot be decoded.
     */
    public function decode(string $data, int &$decoded_len = 0) { }
}