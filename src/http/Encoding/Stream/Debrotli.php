<?php
declare (strict_types=1);

namespace mark\http\Encoding\Stream;

/**
 * A [brotli](https://brotli.org) decoding stream.
 * > ***NOTE:***
 * > This class has been added in v3.2.0.
 */
class Debrotli extends \mark\http\Encoding\Stream {
    /**
     * Decode brotli encoded data.
     *
     * @param string $data The data to uncompress.
     *
     * @return string the uncompressed data.
     */
    public function decode(string $data) { }
}