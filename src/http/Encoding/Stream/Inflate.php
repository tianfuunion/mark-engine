<?php
declare (strict_types=1);

namespace mark\http\Encoding\Stream;

/**
 * A inflate stream supporting deflate, zlib and gzip encodings.
 */
class Inflate extends \mark\http\Encoding\Stream {
    /**
     * Decode deflate/zlib/gzip encoded data.
     *
     * @param string $data The data to uncompress.
     *
     * @return string the uncompressed data.
     */
    public function decode(string $data) { }
}