<?php
declare (strict_types=1);

namespace mark\http\Encoding\Stream;

/**
 * A [brotli](https://brotli.org) encoding stream.
 * > ***NOTE:***
 * > This class has been added in v3.2.0.
 */
class Enbrotli extends \mark\http\Encoding\Stream {
    /**
     * Default compression level.
     */
    public const LEVEL_DEF = null;
    /**
     * Least compression level.
     */
    public const LEVEL_MIN = null;
    /**
     * Greatest compression level.
     */
    public const LEVEL_MAX = null;
    /**
     * Default window bits.
     */
    public const WBITS_DEF = null;
    /**
     * Minimum window bits.
     */
    public const WBITS_MIN = null;
    /**
     * Maximum window bits.
     */
    public const WBITS_MAX = null;
    /**
     * Default compression mode.
     */
    public const MODE_GENERIC = null;
    /**
     * Compression mode for UTF-8 formatted text.
     */
    public const MODE_TEXT = null;
    /**
     * Compression mode used in WOFF 2.0.
     */
    public const MODE_FONT = null;

    /**
     * Encode data with brotli encoding.
     *
     * @param string $data  The data to compress.
     * @param int    $flags Any compression tuning flags. See http\Encoding\Stream\Enbrotli and http\Encoding\Stream constants.
     *
     * @return string the compressed data.
     */
    public function encode(string $data, int $flags = 0) { }
}