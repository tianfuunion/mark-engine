<?php
declare (strict_types=1);

namespace mark\http\Encoding\Stream;

/**
 * A deflate stream supporting deflate, zlib and gzip encodings.
 */
class Deflate extends \mark\http\Encoding\Stream {
    /**
     * Gzip encoding. RFC1952
     */
    public const TYPE_GZIP = 16;
    /**
     * Zlib encoding. RFC1950
     */
    public const TYPE_ZLIB = 0;
    /**
     * Deflate encoding. RFC1951
     */
    public const TYPE_RAW = 32;
    /**
     * Default compression level.
     */
    public const LEVEL_DEF = 0;
    /**
     * Least compression level.
     */
    public const LEVEL_MIN = 1;
    /**
     * Greatest compression level.
     */
    public const LEVEL_MAX = 9;
    /**
     * Default compression strategy.
     */
    public const STRATEGY_DEF = 0;
    /**
     * Filtered compression strategy.
     */
    public const STRATEGY_FILT = 256;
    /**
     * Huffman strategy only.
     */
    public const STRATEGY_HUFF = 512;
    /**
     * Run-length encoding strategy.
     */
    public const STRATEGY_RLE = 768;
    /**
     * Encoding with fixed Huffman codes only.
     * > **A note on the compression strategy:**
     * >
     * > The strategy parameter is used to tune the compression algorithm.
     * >
     * > Use the value DEFAULT_STRATEGY for normal data, FILTERED for data produced by a filter (or predictor), HUFFMAN_ONLY to force Huffman encoding only (no string match), or RLE to limit match distances to one (run-length encoding).
     * >
     * > Filtered data consists mostly of small values with a somewhat random distribution. In this case, the compression algorithm is tuned to compress them better. The effect of FILTERED is to force more Huffman coding and less string matching; it is somewhat intermediate between DEFAULT_STRATEGY and HUFFMAN_ONLY.
     * >
     * > RLE is designed to be almost as fast as HUFFMAN_ONLY, but give better compression for PNG image data.
     * >
     * > FIXED prevents the use of dynamic Huffman codes, allowing for a simpler decoder for special applications.
     * >
     * > The strategy parameter only affects the compression ratio but not the correctness of the compressed output even if it is not set appropriately.
     * >
     * >_Source: [zlib Manual](http://www.zlib.net/manual.html)_
     */
    public const STRATEGY_FIXED = 1024;

    /**
     * Encode data with deflate/zlib/gzip encoding.
     *
     * @param string $data  The data to compress.
     * @param int    $flags Any compression tuning flags.
     *
     * @See \mark\http\Encoding\Stream\Deflate
     * @return string the compressed data.
     * @see \mark\http\Encoding\Stream constants.
     */
    public function encode(string $data, int $flags = 0) { }
}