<?php
declare (strict_types=1);

namespace mark\http\Env;

/**
 * The \mark\http\Env\Request class' instances represent the server's current HTTP request.
 *
 * @See \mark\http\Message for inherited members.
 */
class Request extends \mark\http\Message {
    /**
     * The request's query parameters. ($_GET)
     *
     * @var \mark\http\QueryString
     */
    protected $query = null;
    /**
     * The request's form parameters. ($_POST)
     *
     * @var \mark\http\QueryString
     */
    protected $form = null;
    /**
     * The request's form uploads. ($_FILES)
     *
     * @var array
     */
    protected $files = null;
    /**
     * The request's cookies. ($_COOKIE)
     *
     * @var array
     */
    protected $cookie = null;

    /**
     * Create an instance of the server's current HTTP request.
     * Upon construction, the http\Env\Request acquires http\QueryString instances of query parameters ($\_GET) and form parameters ($\_POST).
     * It also compiles an array of uploaded files ($\_FILES) more comprehensive than the original $\_FILES array, see http\Env\Request::getFiles() for that matter.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\UnexpectedValueException
     */
    public function __construct() { }

    /**
     * Retrieve an URL query value ($_GET).
     *
     * @See \mark\http\QueryString::get()
     * @See \mark\http\QueryString::TYPE_* constants.
     *
     * @param string $name   The key to retrieve the value for.
     * @param mixed  $type   The type to cast the value to. See http\QueryString::TYPE_* constants.
     * @param mixed  $defval The default value to return if the key $name does not exist.
     * @param bool   $delete Whether to delete the entry from the querystring after retrieval.
     *
     * @return \mark\http\QueryString|string|mixed \mark\http\QueryString if called without arguments.
     *         or string the whole querystring if $name is of zero length.
     *         or mixed $defval if the key $name does not exist.
     *         or mixed the querystring value cast to $type if $type was specified and the key $name exists.
     *         or string the querystring value if the key $name exists and $type is not specified or equals http\QueryString::TYPE_STRING.
     */
    public function getCookie(string $name = null, $type = null, $defval = null, bool $delete = false) { }

    /**
     * Retrieve the uploaded files list ($_FILES).
     *
     * @return array the consolidated upload files array.
     */
    public function getFiles() { }

    /**
     * Retrieve a form value ($_POST).
     *
     * @See \mark\http\QueryString::get()
     *
     * @param string $name   The key to retrieve the value for.
     * @param mixed  $type   The type to cast the value to. See http\QueryString::TYPE_* constants.
     * @param mixed  $defval The default value to return if the key $name does not exist.
     * @param bool   $delete Whether to delete the entry from the querystring after retrieval.
     *
     * @return \mark\http\QueryString|string|mixed \mark\http\QueryString if called without arguments.
     *         or string the whole querystring if $name is of zero length.
     *         or mixed $defval if the key $name does not exist.
     *         or mixed the querystring value cast to $type if $type was specified and the key $name exists.
     *         or string the querystring value if the key $name exists and $type is not specified or equals http\QueryString::TYPE_STRING.
     * @see \mark\http\QueryString::TYPE_* constants.
     */
    public function getForm(string $name = null, $type = null, $defval = null, bool $delete = false) { }

    /**
     * Retrieve an URL query value ($_GET).
     *
     * @See \mark\http\QueryString::get()
     * @See \mark\http\QueryString::TYPE_* constants.
     *
     * @param string $name   The key to retrieve the value for.
     * @param mixed  $type   The type to cast the value to. See http\QueryString::TYPE_* constants.
     * @param mixed  $defval The default value to return if the key $name does not exist.
     * @param bool   $delete Whether to delete the entry from the querystring after retrieval.
     *
     * @return \mark\http\QueryString|string|mixed \mark\http\QueryString if called without arguments.
     *         or string the whole querystring if $name is of zero length.
     *         or mixed $defval if the key $name does not exist.
     *         or mixed the querystring value cast to $type if $type was specified and the key $name exists.
     *         or string the querystring value if the key $name exists and $type is not specified or equals http\QueryString::TYPE_STRING.
     */
    public function getQuery(string $name = null, $type = null, $defval = null, bool $delete = false) { }
}