<?php
declare (strict_types=1);

namespace mark\http\Env;

/**
 * The http\Env\Response class' instances represent the server's current HTTP response.
 * See http\Message for inherited members.
 */
class Response extends \mark\http\Message {
    /**
     * Do not use content encoding.
     */
    public const CONTENT_ENCODING_NONE = 0;
    /**
     * Support "Accept-Encoding" requests with gzip and deflate encoding.
     */
    public const CONTENT_ENCODING_GZIP = 1;
    /**
     * No caching info available.
     */
    public const CACHE_NO = 0;
    /**
     * The cache was hit.
     */
    public const CACHE_HIT = 1;
    /**
     * The cache was missed.
     */
    public const CACHE_MISS = 2;
    /**
     * A request instance which overrides the environments default request.
     *
     * @var \mark\http\Env\Request
     */
    protected $request = null;
    /**
     * The response's MIME content type.
     *
     * @var string
     */
    protected $contentType = null;
    /**
     * The response's MIME content disposition.
     *
     * @var string
     */
    protected $contentDisposition = null;
    /**
     * See http\Env\Response::CONTENT_ENCODING_* constants.
     *
     * @var int
     */
    protected $contentEncoding = null;
    /**
     * How the client should treat this response in regards to caching.
     *
     * @var string
     */
    protected $cacheControl = null;
    /**
     * A custom ETag.
     *
     * @var string
     */
    protected $etag = null;
    /**
     * A "Last-Modified" time stamp.
     *
     * @var int
     */
    protected $lastModified = null;
    /**
     * Any throttling delay.
     *
     * @var int
     */
    protected $throttleDelay = null;
    /**
     * The chunk to send every $throttleDelay seconds.
     *
     * @var int
     */
    protected $throttleChunk = null;
    /**
     * The response's cookies.
     *
     * @var array
     */
    protected $cookies = null;

    /**
     * Create a new env response message instance.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\UnexpectedValueException
     */
    public function __construct() { }

    /**
     * Output buffer handler.
     * Appends output data to the body.
     *
     * @param string $data     The data output.
     * @param int    $ob_flags Output buffering flags passed from the output buffering control layer.
     *
     * @return bool success.
     */
    public function __invoke(string $data, int $ob_flags = 0) { }

    /**
     * Manually test the header $header_name of the environment's request for a cache hit.
     * http\Env\Response::send() checks that itself, though.
     *
     * @param string $header_name The request header to test.
     *
     * @return int a http\Env\Response::CACHE_* constant.
     */
    public function isCachedByEtag(string $header_name = 'If-None-Match') { }

    /**
     * Manually test the header $header_name of the environment's request for a cache hit.
     * http\Env\Response::send() checks that itself, though.
     *
     * @param string $header_name The request header to test.
     *
     * @return int a http\Env\Response::CACHE_* constant.
     */
    public function isCachedByLastModified(string $header_name = 'If-Modified-Since') { }

    /**
     * Send the response through the SAPI or $stream.
     * Flushes all output buffers.
     *
     * @param resource $stream A writable stream to send the response through.
     *
     * @return bool success.
     */
    public function send($stream = null) { }

    /**
     * Make suggestions to the client how it should cache the response.
     *
     * @param string $cache_control (A) "Cache-Control" header value(s).
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setCacheControl(string $cache_control) { }

    /**
     * Set the response's content disposition parameters.
     *
     * @param array $disposition_params MIME content disposition as http\Params array.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setContentDisposition(array $disposition_params) { }

    /**
     * Enable support for "Accept-Encoding" requests with deflate or gzip.
     * The response will be compressed if the client indicates support and wishes that.
     *
     * @param int $content_encoding See http\Env\Response::CONTENT_ENCODING_* constants.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setContentEncoding(int $content_encoding) { }

    /**
     * Set the MIME content type of the response.
     *
     * @param string $content_type The response's content type.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @return \mark\http\Env\Response self.
     */
    public function setContentType(string $content_type) { }

    /**
     * Add cookies to the response to send.
     *
     * @param mixed $cookie The cookie to send.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\http\exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setCookie($cookie) { }

    /**
     * Override the environment's request.
     *
     * @param \mark\http\Message $env_request The overriding request message.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setEnvRequest(\mark\http\Message $env_request) { }

    /**
     * Set a custom ETag.
     * > ***NOTE:***
     * > This will be used for caching and pre-condition checks.
     *
     * @param string $etag A ETag.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setEtag(string $etag) { }

    /**
     * Set a custom last modified time stamp.
     * > ***NOTE:***
     * > This will be used for caching and pre-condition checks.
     *
     * @param int $last_modified A unix timestamp.
     *
     * @return \mark\http\Env\Response self.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setLastModified(int $last_modified) { }

    /**
     * Enable throttling.
     * Send $chunk_size bytes every $delay seconds.
     * > ***NOTE:***
     * > If you need throttling by regular means, check for other options in your stack, because this method blocks the executing process/thread until the response has completely been sent.
     *
     * @param int   $chunk_size Bytes to send.
     * @param float $delay      Seconds to sleep.
     *
     * @return \mark\http\Env\Response self.
     */
    public function setThrottleRate(int $chunk_size, float $delay = 1) { }
}