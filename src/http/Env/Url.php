<?php
declare (strict_types=1);

namespace mark\http\Env;

/**
 * URL class using the HTTP environment by default.
 * > ***NOTE:***
 * > This class has been added in v3.0.0.
 * Always adds \mark\http\Url::FROM_ENV to the $flags constructor argument. See also \mark\http\Url.
 */
class Url extends \mark\http\Url { }