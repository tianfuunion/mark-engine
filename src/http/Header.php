<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The http\Header class provides methods to manipulate, match, negotiate and serialize HTTP headers.
 */
class Header implements \Serializable {
    /**
     * None of the following match constraints applies.
     */
    public const MATCH_LOOSE = 0;
    /**
     * Perform case sensitive matching.
     */
    public const MATCH_CASE = 1;
    /**
     * Match only on word boundaries (according by CType alpha-numeric).
     */
    public const MATCH_WORD = 16;
    /**
     * Match the complete string.
     */
    public const MATCH_FULL = 32;
    /**
     * Case sensitively match the full string (same as MATCH_CASE|MATCH_FULL).
     */
    public const MATCH_STRICT = 33;
    /**
     * The name of the HTTP header.
     *
     * @var string
     */
    public $name = null;
    /**
     * The value of the HTTP header.
     *
     * @var mixed
     */
    public $value = null;

    /**
     * Create an http\Header instance for use of simple matching or negotiation. If the value of the header is an array it may be compounded to a single comma separated string.
     *
     * @param string $name  The HTTP header name.
     * @param mixed  $value The value of the header.
     *                      # Throws:
     */
    public function __construct(string $name = null, $value = null) { }

    /**
     * String cast handler. Alias of http\Header::serialize().
     *
     * @return string the serialized form of the HTTP header (i.e. "Name: value").
     */
    public function __toString() { }

    /**
     * Create a parameter list out of the HTTP header value.
     *
     * @param mixed $ps    The parameter separator(s).
     * @param mixed $as    The argument separator(s).
     * @param mixed $vs    The value separator(s).
     * @param int   $flags The modus operandi. See http\Params constants.
     *
     * @return \mark\http\Params instance
     */
    public function getParams($ps = null, $as = null, $vs = null, int $flags = null) { }

    /**
     * Match the HTTP header's value against provided $value according to $flags.
     *
     * @param string $value The comparison value.
     * @param int    $flags The modus operandi. See http\Header constants.
     *
     * @return bool whether $value matches the header value according to $flags.
     */
    public function match(string $value, int $flags = null) { }

    /**
     * Negotiate the header's value against a list of supported values in $supported.
     * Negotiation operation is adopted according to the header name, i.e. if the
     * header being negotiated is Accept, then a slash is used as primary type
     * separator, and if the header is Accept-Language respectively, a hyphen is
     * used instead.
     * > ***NOTE:***
     * > The first element of $supported serves as a default if no operand matches.
     *
     * @param array  $supported The list of supported values to negotiate.
     * @param array &$result    Out parameter recording the negotiation results.
     *
     * @return string|null NULL if negotiation fails.
     *         or string the closest match negotiated, or the default (first entry of $supported).
     */
    public function negotiate(array $supported, array &$result = null) { }

    /**
     * Parse HTTP headers.
     * See also \mark\http\Header\Parser.
     *
     * @param string $header       The complete string of headers.
     * @param string $header_class A class extending \mark\http\Header.
     *
     * @return array|false array of parsed headers, where the elements are instances of $header_class if specified.
     *         or false if parsing fails.
     */
    public function parse(string $header, string $header_class = null) { }

    /**
     * Implements Serializable.
     *
     * @return string serialized representation of HTTP header (i.e. "Name: value")
     */
    public function serialize() { }

    /**
     * Convenience method. Alias of http\Header::serialize().
     *
     * @return string the serialized form of the HTTP header (i.e. "Name: value").
     */
    public function toString() { }

    /**
     * Implements Serializable.
     *
     * @param string $serialized The serialized HTTP header (i.e. "Name: value")
     */
    public function unserialize($serialized) { }
}