<?php
declare (strict_types=1);

namespace mark\http;

/**
 * The message class builds the foundation for any request and response message.
 * See \mark\http\Client\Request and \mark\http\Client\Response, as well as \mark\http\Env\Request and http\Env\Response.
 */
class Message implements \Countable, \Iterator {
    /**
     * No specific type of message.
     */
    public const TYPE_NONE = 0;
    /**
     * A request message.
     */
    public const TYPE_REQUEST = 1;
    /**
     * A response message.
     */
    public const TYPE_RESPONSE = 2;
    /**
     * The message type. See \mark\http\Message::TYPE_* constants.
     *
     * @var int
     */
    protected $type = \mark\http\Message::TYPE_NONE;
    /**
     * The message's body.
     *
     * @var \mark\http\Message\Body
     */
    protected $body = null;
    /**
     * The request method if the message is of type request.
     *
     * @var string
     */
    protected $requestMethod = '';
    /**
     * The request url if the message is of type request.
     *
     * @var string
     */
    protected $requestUrl = '';
    /**
     * The response status phrase if the message is of type response.
     *
     * @var string
     */
    protected $responseStatus = '';
    /**
     * The response code if the message is of type response.
     *
     * @var int
     */
    protected $responseCode = 0;
    /**
     * A custom HTTP protocol version.
     *
     * @var string
     */
    protected $httpVersion = '1.1';
    /**
     * Any message headers.
     *
     * @var array Map of all registered headers, as original name => array of values
     */
    protected $headers = [];
    /** @var array Map of lowercase header name => original name at registration */
    protected $headerNames = [];

    /**
     * Any parent message.
     *
     * @var \mark\http\Message
     */
    protected $parentMessage;

    /**
     * Create a new HTTP message.
     *
     * @param mixed $message Either a resource or a string, representing the HTTP message.
     * @param bool  $greedy  Whether to read from a $message resource until EOF.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\BadMessageException
     */
    public function __construct($message = null, bool $greedy = true) {
        $this->body = $message;
    }

    /**
     * Retrieve the message serialized to a string.
     * Alias of \mark\http\Message::toString().
     *
     * @return string the single serialized HTTP message.
     */
    public function __toString() { }

    /**
     * Append the data of $body to the message's body.
     * See \mark\http\Message::setBody() and \mark\http\Message\Body::append().
     *
     * @param \mark\http\Message\Body $body The message body to add.
     *
     * @return \mark\http\Message self.
     */
    public function addBody(\mark\http\Message\Body $body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Add an header, appending to already existing headers.
     * See \mark\http\Message::addHeaders() and \mark\http\Message::setHeader().
     *
     * @param string $name  The header name.
     * @param mixed  $value The header value.
     *
     * @return \mark\http\Message self.
     */
    public function addHeader(string $name, $value) {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * Add headers, optionally appending values, if header keys already exist.
     *
     * @See \mark\http\Message::addHeader()
     * @See \mark\http\Message::setHeaders().
     *
     * @param array $headers The HTTP headers to add.
     * @param bool  $append  Whether to append values for existing headers.
     *
     * @return \mark\http\Message self.
     */
    public function addHeaders(array $headers, bool $append = false) {
        $this->headers = $append ? array_merge($this->headers, $headers) : $headers;

        return $this;
    }

    /**
     * Implements Countable.
     *
     * @return int the count of messages in the chain above the current message.
     */
    public function count(): int { }

    /**
     * Implements iterator.
     * See \mark\http\Message::valid() and \mark\http\Message::rewind().
     *
     * @return \mark\http\Message the current message in the iterated message chain.
     */
    public function current(): mixed { }

    /**
     * Detach a clone of this message from any message chain.
     *
     * @return \mark\http\Message clone.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function detach() { }

    /**
     * Retrieve the message's body.
     *
     * @See \mark\http\Message::setBody().
     * @return \mark\http\Message\Body the message body.
     * @throws \mark\http\Exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getBody() { return $this->body; }

    /**
     * Retrieve all message headers.
     *
     * @See \mark\http\Message::setHeaders()
     * @See \mark\http\Message::getHeader().
     * @return array the message's headers.
     */
    public function getHeaders() { return $this->headers; }

    /**
     * Retrieve the HTTP protocol version of the message.
     *
     * @See \mark\http\Message::setHttpVersion().
     * @return string the HTTP protocol version, e.g. "1.0"; defaults to "1.1".
     */
    public function getHttpVersion() { return $this->httpVersion; }

    /**
     * Retrieve the first line of a request or response message.
     *
     * @See \mark\http\Message::setInfo and also:
     * @See \mark\http\Message::getType()
     * @See \mark\http\Message::getHttpVersion()
     * @See \mark\http\Message::getResponseCode()
     * @See \mark\http\Message::getResponseStatus()
     * @See \mark\http\Message::getRequestMethod()
     * @See \mark\http\Message::getRequestUrl()
     * @return string|null string the HTTP message information. or NULL if the message is neither of type request nor response.
     */
    public function getInfo() { }

    /**
     * Retrieve any parent message.
     * See http\Message::reverse().
     *
     * @return \mark\http\Message the parent message.
     * @throws \mark\http\Exception\BadMethodCallException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getParentMessage() { return $this->parentMessage; }

    /**
     * Retrieve the request method of the message.
     * See \mark\http\Message::setRequestMethod() and \mark\http\Message::getRequestUrl().
     *
     * @return string|false string the request method.
     *         or false if the message was not of type request.
     */
    public function getRequestMethod() { return $this->requestMethod; }

    /**
     * Retrieve the request URL of the message.
     * See \mark\http\Message::setRequestUrl().
     *
     * @return string|false string the request URL; usually the path and the querystring.
     *         or false if the message was not of type request.
     */
    public function getRequestUrl() { return $this->requestUrl; }

    /**
     * Retrieve the response code of the message.
     * See \mark\http\Message::setResponseCode() and \mark\http\Message::getResponseStatus().
     *
     * @return int|false int the response status code.
     *         or false if the message is not of type response.
     */
    public function getResponseCode() { return $this->responseCode; }

    /**
     * Retrieve the response status of the message.
     * See \mark\http\Message::setResponseStatus() and \mark\http\Message::getResponseCode().
     *
     * @return string|false string the response status phrase.
     *         or false if the message is not of type response.
     */
    public function getResponseStatus() { return $this->responseStatus; }

    /**
     * Retrieve the type of the message.
     *
     * @See \mark\http\Message::setType()
     * @See \mark\http\Message::getInfo().
     * @return int the message type. See \mark\http\Message::TYPE_* constants.
     */
    public function getType() { return $this->type; }

    /**
     * Check whether this message is a multipart message based on it's content type.
     * If the message is a multipart message and a reference $boundary is given, the boundary string of the multipart message will be stored in $boundary.
     *
     * @See \mark\http\Message::splitMultipartBody().
     *
     * @param string &$boundary A reference where the boundary string will be stored.
     *
     * @return bool whether this is a message with a multipart "Content-Type".
     */
    public function isMultipart(string &$boundary = null) { }

    /**
     * Implements Iterator.
     *
     * @See \mark\http\Message::current() and \mark\http\Message::rewind().
     * @return int a non-sequential integer key.
     */
    public function key(): mixed { }

    /**
     * Implements Iterator.
     *
     * @See \mark\http\Message::valid() and \mark\http\Message::rewind().
     */
    public function next(): void { }

    /**
     * Prepend message(s) $message to this message, or the top most message of this message chain.
     * > ***NOTE:***
     * > The message chains must not overlap.
     *
     * @param \mark\http\Message $message The message (chain) to prepend as parent messages.
     * @param bool               $top     Whether to prepend to the top-most parent message.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\Exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function prepend(\mark\http\Message $message, bool $top = true) { }

    /**
     * Reverse the message chain and return the former top-most message.
     * > ***NOTE:***
     * > Message chains are ordered in reverse-parsed order by default, i.e. the last parsed message is the message you'll receive from any call parsing HTTP messages.
     * >
     * > This call re-orders the messages of the chain and returns the message that was parsed first with any later parsed messages re-parentized.
     *
     * @return \mark\http\Message the other end of the message chain.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function reverse() { }

    /**
     * Implements Iterator.
     */
    public function rewind(): void { }

    /**
     * Implements Serializable.
     *
     * @return string the serialized HTTP message.
     */
    public function serialize() { }

    /**
     * Set the message's body.
     *
     * @See \mark\http\Message::getBody() and http\Message::addBody().
     *
     * @param \mark\http\Message\Body $body The new message body.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\Exception\UnexpectedValueException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setBody(\mark\http\Message\Body $body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Set a single header.
     *
     * @See \mark\http\Message::getHeader() and http\Message::addHeader().
     *      > ***NOTE:***
     *      > Prior to v2.5.6/v3.1.0 headers with the same name were merged into a single
     *      > header with values concatenated by comma.
     *
     * @param string $header The header's name.
     * @param mixed  $value  The header's value. Removes the header if NULL.
     *
     * @return \mark\http\Message self.
     */
    public function setHeader(string $header, $value = null) {
        if (is_scalar($value)) {
            $this->headers[$header] = $value;
        }

        return $this;
    }

    /**
     * Set the message headers.
     *
     * @See \mark\http\Message::getHeaders() and http\Message::addHeaders().
     *      > ***NOTE:***
     *      > Prior to v2.5.6/v3.1.0 headers with the same name were merged into a single
     *      > header with values concatenated by comma.
     *
     * @param array $headers The message's headers.
     *
     * @return \mark\http\Message null.
     */
    public function setHeaders(array $headers = array()) {
        $this->headers = array_merge($this->headers, $headers);

        return $this;
    }

    /**
     * Set the HTTP protocol version of the message.
     *
     * @See \mark\http\Message::getHttpVersion().
     *
     * @param string $http_version The protocol version, e.g. "1.1", optionally prefixed by "HTTP/".
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\exception\BadHeaderException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setHttpVersion(string $http_version) {
        $this->httpVersion = $http_version;

        return $this;
    }

    /**
     * Set the complete message info, i.e. type and response resp. request information, at once.
     *
     * @See \mark\http\Message::getInfo().
     *
     * @param string $http_info The message info (first line of an HTTP message).
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\exception\BadHeaderException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setInfo(string $http_info) { }

    /**
     * Set the request method of the message.
     *
     * @See \mark\http\Message::getRequestMethod() and http\Message::setRequestUrl().
     *
     * @param string $method The request method.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setRequestMethod(string $method) {
        $this->requestMethod = $method;

        return $this;
    }

    /**
     * Set the request URL of the message.
     *
     * @See \mark\http\Message::getRequestUrl() and http\Message::setRequestMethod().
     *
     * @param string $url The request URL.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\Exception\BadMethodCallException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setRequestUrl(string $url) {
        $this->requestUrl = $url;

        return $this;
    }

    /**
     * Set the response status code.
     *
     * @See \mark\http\Message::getResponseCode() and http\Message::setResponseStatus().
     *      > ***NOTE:***
     *      > This method also resets the response status phrase to the default for that code.
     *
     * @param int  $response_code The response code.
     * @param bool $strict        Whether to check that the response code is between 100 and 599 inclusive.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setResponseCode(int $response_code, bool $strict = true) {
        if ($strict && ($response_code < 100 || $response_code > 599)) {
            // throw new \mark\exception\InvalidArgumentException('that the response code is between 100 and 599 inclusive.', 416);
        }

        $this->responseCode = $response_code;
        $this->setHeader('Response-Code', $response_code);

        return $this;
    }

    /**
     * Set the response status phrase.
     *
     * @See \mark\http\Message::getResponseStatus() and http\Message::setResponseCode().
     *
     * @param string $status The status phrase.
     *
     * @return \mark\http\Message self.
     * @throws \mark\http\exception\BadMethodCallException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setResponseStatus(string $status) {
        $this->responseStatus = $status;
        $this->setHeader('Response-Status', $status);

        return $this;
    }

    /**
     * Set the message type and reset the message info.
     *
     * @See \mark\http\Message::getType()
     * @See \mark\http\Message::setInfo().
     * @See \mark\http\Message::TYPE_* constants.
     *
     * @param int $type The desired message type.
     *
     * @return \mark\http\Message self.
     */
    public function setType(int $type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Splits the body of a multipart message.
     *
     * @See \mark\http\Message::isMultipart()
     * @See \mark\http\Message\Body::addPart().
     * @return \mark\http\Message a message chain of all messages of the multipart body.
     * @throws \mark\http\Exception\BadMethodCallException
     * @throws \mark\http\exception\BadMessageException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function splitMultipartBody() { }

    /**
     * Stream the message through a callback.
     *
     * @param callable $callback The callback of the form function(\mark\http\Message $from, string $data).
     *
     * @return \mark\http\Message self.
     */
    public function toCallback(callable $callback) {
        $callback($this);

        return $this;
    }

    /**
     * Stream the message into stream $stream, starting from $offset, streaming $maxlen at most.
     *
     * @param resource $stream The resource to write to.
     *
     * @return \mark\http\Message self.
     */
    public function toStream($stream) { }

    /**
     * Retrieve the message serialized to a string.
     *
     * @param bool $include_parent Whether to include all parent messages.
     *
     * @return string the HTTP message chain serialized to a string.
     */
    public function toString(bool $include_parent = false) {
        return unserialize($this->body);
    }

    /**
     * Implements Serializable.
     *
     * @param string $serialized The serialized message.
     */
    public function unserialize($serialized) {
        unserialize($serialized);
    }

    /**
     * Implements Iterator.
     *
     * @See \mark\http\Message::current()
     * @See \mark\http\Message::rewind().
     * @return bool whether \mark\http\Message::current() would not return NULL.
     */
    public function valid(): bool { }
}