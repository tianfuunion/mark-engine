<?php
declare (strict_types=1);

namespace mark\http\response;

/**
 * Class HttpResponseHandler
 *
 * @package mark\http\response
 */
abstract class HttpResponseHandler implements HttpResponse {

    /**
     * @param string $response
     * @param int    $code
     * @param array  $headers
     */
    public function Handler(string $response, int $code, array $headers): void {
        if ($code === 200) {
            $this->onSuccess($response, $code, $headers);
        } else {
            $this->onFailure($response, $code, $headers);
        }
    }

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    abstract public function onSuccess($response, int $code, array $headers): void;

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    abstract public function onFailure($response, int $code, array $headers): void;

}