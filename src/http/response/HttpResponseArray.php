<?php
declare (strict_types=1);

namespace mark\http\response;

/**
 * Class HttpResponseArray
 *
 * @package mark\http\response
 */
abstract class  HttpResponseArray implements HttpResponse {

    /**
     * @param string $response
     * @param int    $code
     * @param array  $headers
     */
    public function Handler(string $response, int $code, array $headers): void {
        if ($code === 200) {
            $this->onSuccess(json_decode($response, true), $code, $headers);
        } else {
            $this->onFailure(json_decode($response, true), $code, $headers);
        }
    }

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    abstract public function onSuccess($response, int $code, array $headers): void;

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    abstract public function onFailure($response, int $code, array $headers): void;

}