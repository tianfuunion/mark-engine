<?php
declare (strict_types=1);

namespace mark\http\response;

/**
 * Interface HttpResponse
 *
 * @description 自定义响应接口
 * @package     mark\http\response
 */
interface HttpResponse {
    /**
     * @param $field
     */
    public function onStart($field): void;

    public function onProcess(): void;

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    public function onSuccess($response, int $code, array $headers): void;

    /**
     * @param       $response
     * @param int   $code
     * @param array $headers
     */
    public function onFailure($response, int $code, array $headers): void;

    public function onFinish(): void;
}