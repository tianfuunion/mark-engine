<?php
declare (strict_types=1);

namespace mark\http;

/**
 * Parse, interpret and compose HTTP (header) parameters.
 */
class Params implements \ArrayAccess {
    /**
     * The default parameter separator (",").
     */
    public const DEF_PARAM_SEP = ',';
    /**
     * The default argument separator (";").
     */
    public const DEF_ARG_SEP = ';';
    /**
     * The default value separator ("=").
     */
    public const DEF_VAL_SEP = '=';
    /**
     * Empty param separator to parse cookies.
     */
    public const COOKIE_PARAM_SEP = '';
    /**
     * Do not interpret the parsed parameters.
     */
    public const PARSE_RAW = 0;
    /**
     * Interpret input as default formatted parameters.
     */
    public const PARSE_DEFAULT = 17;
    /**
     * Parse backslash escaped (quoted) strings.
     */
    public const PARSE_ESCAPED = 1;
    /**
     * Urldecode single units of parameters, arguments and values.
     */
    public const PARSE_URLENCODED = 4;
    /**
     * Parse sub dimensions indicated by square brackets.
     */
    public const PARSE_DIMENSION = 8;
    /**
     * Parse URL querystring (same as http\Params::PARSE_URLENCODED|http\Params::PARSE_DIMENSION).
     */
    public const PARSE_QUERY = 12;
    /**
     * Parse [RFC5987](http://tools.ietf.org/html/rfc5987) style encoded character set and language information embedded in HTTP header params.
     */
    public const PARSE_RFC5987 = 16;
    /**
     * Parse [RFC5988](http://tools.ietf.org/html/rfc5988) (Web Linking) tags of Link headers.
     */
    public const PARSE_RFC5988 = 32;
    /**
     * The (parsed) parameters.
     *
     * @var array
     */
    public $params = null;
    /**
     * The parameter separator(s).
     *
     * @var array
     */
    public $param_sep = \mark\http\Params::DEF_PARAM_SEP;
    /**
     * The argument separator(s).
     *
     * @var array
     */
    public $arg_sep = \mark\http\Params::DEF_ARG_SEP;
    /**
     * The value separator(s).
     *
     * @var array
     */
    public $val_sep = \mark\http\Params::DEF_VAL_SEP;
    /**
     * The modus operandi of the parser. See http\Params::PARSE_* constants.
     *
     * @var int
     */
    public $flags = \mark\http\Params::PARSE_DEFAULT;

    /**
     * Instantiate a new HTTP (header) parameter set.
     *
     * @param mixed $params Pre-parsed parameters or a string to be parsed.
     * @param mixed $ps     The parameter separator(s).
     * @param mixed $as     The argument separator(s).
     * @param mixed $vs     The value separator(s).
     * @param int   $flags  The modus operandi. See http\Params::PARSE_* constants.
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\RuntimeException
     */
    public function __construct($params = null, $ps = null, $as = null, $vs = null, int $flags = null) { }

    /**
     * String cast handler. Alias of http\Params::toString().
     * Returns a stringified version of the parameters.
     *
     * @return string version of the parameters.
     */
    public function __toString() { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to look after.
     *
     * @return bool Existence.
     */
    public function offsetExists($offset) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to retrieve.
     *
     * @return mixed contents at offset.
     */
    public function offsetGet($offset) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to modify.
     * @param mixed  $value  The value to set.
     */
    public function offsetSet($offset, $value) { }

    /**
     * Implements ArrayAccess.
     *
     * @param string $offset The offset to delete.
     */
    public function offsetUnset($offset) { }

    /**
     * Convenience method that simply returns http\Params::$params.
     *
     * @return array of parameters.
     */
    public function toArray() { }

    /**
     * Returns a stringified version of the parameters.
     *
     * @return string version of the parameters.
     */
    public function toString() { }
}