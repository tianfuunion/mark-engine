<?php
declare (strict_types=1);

namespace mark\assets;

/**
 * Class Assets
 *
 * @package mark\assets
 */
final class Assets {

    /**
     * Assets constructor.
     */
    private function __construct() { }

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param false $origin
     *
     * @return static
     */
    public static function getInstance($origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private $uri = 'https://assets.tianfu.pro';
    private $pattern = 'release';

    /**
     * 设置资源来源
     *
     * @param string $uri
     *
     * @return $this
     */
    public function setOrigin(string $uri): self {
        if (!empty($uri)) {
            $this->uri = trim(trim($uri), '/');
        }

        return $this;
    }

    /**
     * 设置运行模式
     *
     * @param string $pattern [auto|origin|develop|debug|alpha|beta|release|stable|feature|hotfix]
     *
     * @return $this
     */
    final public function setPattern(string $pattern = 'release'): self {
        if (!empty($pattern)) {
            $this->pattern = $pattern;
        }

        return $this;
    }

    private $assets = array();

    public function getAssets(): array {
        return array_values(array_unique($this->assets, SORT_REGULAR));
    }

    /**
     * Env Style load for Thinkphp 6.0
     *
     * @param string $style
     * @param string $modifier
     * @param string $path
     * @param array  $attribute
     *
     * @return $this
     */
    public function style(string $style, $modifier = 'private', $path = '', $attribute = array()): self {
        return $this->assets($style, 'style', $modifier, $path, $attribute);
    }

    /**
     * Env Script load for Thinkphp 6.0
     *
     * @param string $script
     * @param string $modifier
     * @param string $path
     * @param array  $attribute
     *
     * @return $this
     */
    public function script(string $script, $modifier = 'private', $path = '', $attribute = array()): self {
        return $this->assets($script, 'script', $modifier, $path, $attribute);
    }

    /**
     * Assets load for Thinkphp 6.0
     *
     * @param string $asset          资源名称
     * @param string $type           资源类型   [style|script]
     * @param string $modifier       修饰符
     *                               open         开源库
     *                               private      私有库：第1方库
     *                               project      项目库：第2方库
     *                               protected    模块库：第2方库
     *                               public       公共库：第3方库
     * @param string $path           资源路径
     * @param array  $attribute      附加属性
     *
     * @return $this
     */
    public function assets(string $asset, $type = 'style', $modifier = 'private', $path = '', $attribute = array()): self {
        if (empty($asset)) {
            return $this;
        }

        switch ($type) {
            default:
            case 'style':
            case 'css':
                $type = 'style';
                $extension = 'css';
                break;
            case 'script':
            case 'js':
                $type = 'script';
                $extension = 'js';
                break;
        }

        $modifier = !empty($modifier) ? $modifier : 'private';

        $path = trim(trim($path), '/');

        $origin = $asset;
        switch ($this->pattern) {
            case 'auto':
            case 'origin':
            case 'develop':
            case 'debug':
            case 'alpha':
            case 'beta':
            case 'preview':
                // origin
                break;
            case 'testing':
            case 'release':
            case 'stable':
            case 'feature':
            case 'hotfix':
                $asset .= '.min';
                break;
            default:
                $this->pattern = 'auto';
                break;
        }

        $assets = array('asset' => $asset,
                        'type' => $type,
                        'modifier' => $modifier,
                        'uri' => $this->uri,
                        'assets' => DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $asset . '.' . $extension,
                        'origin' => DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $origin . '.' . $extension,
                        'attribute' => $attribute,
                        'version' => !empty($attribute['version'] ?? '') ? $attribute['version'] : \mark\Mark::VERSION,
                        'pattern' => $this->pattern);

        switch (strtolower($modifier)) {
            case 'open': // 开源的，或来自网络的
                $assets['assets'] = $this->uri . DIRECTORY_SEPARATOR . (!empty($path) ? $path . DIRECTORY_SEPARATOR : '') . $asset . '.' . $extension;
                $assets['origin'] = $this->uri . DIRECTORY_SEPARATOR . (!empty($path) ? $path . DIRECTORY_SEPARATOR : '') . $origin . '.' . $extension;
                break;
            case 'public': // 公共库
                $assets['assets'] = DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . $asset . '.' . $extension;
                $assets['origin'] = DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . $origin . '.' . $extension;
                break;
            case 'project': // 当前项目库
                $assets['assets'] = DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . $asset . '.' . $extension;
                $assets['origin'] = DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . $origin . '.' . $extension;
                break;
            case 'protected': // 当前模块库
                $assets['assets'] = DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . $asset . '.' . $extension;
                $assets['origin'] = DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . $origin . '.' . $extension;
                break;
            case 'private': // 当前操作库
            default:
                break;
        }

        $this->assets[] = $assets;

        return $this;
    }
}