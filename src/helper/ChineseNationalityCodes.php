<?php
declare (strict_types=1);

namespace mark\helper;

/**
 * Class ChineseNationalityCodes
 *
 * @description 中国56个民族-第七次全国人口普查民族代码表
 * @author      Mark<mark@tianfuunion.cn>
 * @link        https://www.nbyz.gov.cn/art/2020/9/27/art_1229107563_58678611.html
 * @package     mark\tool
 */
final class ChineseNationalityCodes {

    /**
     * 中国56个民族-代码表
     *
     * @var array
     */
    private static $CHINESE_NATIONALITY_CODE = array(
        1 => '汉族',
        2 => '蒙古族',
        3 => '回族',
        4 => '藏族',
        5 => '维吾尔族',
        6 => '苗族',
        7 => '彝族',
        8 => '壮族',
        9 => '布依族',
        10 => '朝鲜族',
        11 => '满族',
        12 => '侗族',
        13 => '瑶族',
        14 => '白族',
        15 => '土家族',
        16 => '哈尼族',
        17 => '哈萨克族',
        18 => '傣族',
        19 => '黎族',
        20 => '傈僳族',
        21 => '佤族',
        22 => '畲族',
        23 => '高山族',
        24 => '拉祜族',
        25 => '水族',
        26 => '东乡族',
        27 => '纳西族',
        28 => '景颇族',
        29 => '柯尔克孜族',
        30 => '撒拉族',
        31 => '达斡尔族',
        32 => '仫佬族',
        33 => '羌族',
        34 => '布朗族',
        35 => '撒拉族',
        36 => '毛难族',
        37 => '仡佬族',
        38 => '锡伯族',
        39 => '阿昌族',
        40 => '普米族',
        41 => '塔吉克族',
        42 => '怒族',
        43 => '乌孜别克族',
        44 => '俄罗斯族',
        45 => '鄂温克族',
        46 => '崩龙族',
        47 => '保安族',
        48 => '裕固族',
        49 => '京族',
        50 => '塔塔尔族',
        51 => '独龙族',
        52 => '鄂伦春族',
        53 => '德昂族',
        54 => '门巴族',
        55 => '珞巴族',
        56 => '基诺族',
        97 => '其他',// 其他  <====> 未定族称人口
        98 => '外国血统',// 外国血统 <====> 外国血统中国籍人士 <===> 入籍
    );

    /**
     * 查询民族代码列表
     *
     * @return array
     */
    public static function getCodeList(): array {
        return self::$CHINESE_NATIONALITY_CODE;
    }

    /**
     * 根据民族代码查询民族名称；
     *
     * @param int $code 民族代码 取值：1-56、97、98
     *
     * @return string 民族名称
     */
    public static function getName(int $code): string {
        return self::$CHINESE_NATIONALITY_CODE[$code] ?? '其他';
    }

    /**
     * 根据民族名称查询编码；
     * 民族：可以通过身份证读卡器查询到用户的民族名称，我们通过民族名称反查国家民族代码
     *
     * @param string $name 民族名称,eg: 汉族 或者 汉
     *
     * @return int 民族编码
     * @author mark
     * @since  1.0.0
     */
    public static function getCode(string $name): int {
        // 民族为空则返回其它
        if (empty($name) || strlen($name) <= 0) {
            return 97;
        }

        // 修正方案1：存在多种叫法
        if ('土' === $name) {
            $name = '土家';
        }

        // 修正方案2：如果民族后面没有【族】，我们默认加族
        $suffix = '族';
        if (count(explode($suffix, $name)) <= 1) {
            $name .= $suffix;
        }

        // 在列表中，则直接返回相应的值
        $code = array_search($name, self::$CHINESE_NATIONALITY_CODE, true);
        // $code = in_array($name, self::$CHINESE_NATIONALITY_CODE);
        if ($code !== false) {
            return $code;
        }

        // 外国血统、外国血统中国籍人士、入籍
        if ('外国血统中国籍人士' === $name || '外国血统' === $name || '入籍' === $name) {
            return 97;
        }

        // 其他、未定族称人口
        if ('未定族称人口' === $name || '其他' === $name || '其它' === $name) {
            return 98;
        }

        return 98;
    }

    /**
     * 测试示例
     */
    public static function example(): void {
        foreach (self::$CHINESE_NATIONALITY_CODE as $key => $value) {
            var_dump($key, $value);
        }

        echo PHP_EOL . '获取民族名称' . PHP_EOL;
        var_dump('汉族' . '=>' . self::getName(1));
        var_dump('哈萨克族' . '=>' . self::getName(17));
        var_dump('基诺族' . '=>' . self::getName(56));
        var_dump('其他' . '=>' . self::getName(97));
        var_dump('外国血统' . '=>' . self::getName(98));

        echo PHP_EOL . '获取民族代码' . PHP_EOL;
        var_dump('01' . '=>' . self::getCode('汉族'));
        var_dump('01' . '=>' . self::getCode('汉'));
        var_dump('17' . '=>' . self::getCode('哈萨克族'));
        var_dump('17' . '=>' . self::getCode('哈萨克'));
        var_dump('56' . '=>' . self::getCode('基诺族'));

        echo PHP_EOL . '其他、外国、未定民族' . PHP_EOL;
        var_dump('97' . '=>' . self::getCode('其他'));
        var_dump('97' . '=>' . self::getCode('未定族称人口'));

        var_dump('98' . '=>' . self::getCode('外国血统中国籍人士'));
        var_dump('98' . '=>' . self::getCode('入籍'));

        echo PHP_EOL . '身份证读卡器获取到的民族' . PHP_EOL;
        var_dump('26' . '=>' . self::getCode('东乡'));
        var_dump('12' . '=>' . self::getCode('侗'));
        var_dump('17' . '=>' . self::getCode('哈萨克族'));
        var_dump('05' . '=>' . self::getCode('维吾尔'));
        var_dump('97' . '=>' . self::getCode('其他'));
        var_dump('15' . '=>' . self::getCode('土家'));
        var_dump('15' . '=>' . self::getCode('土'));
    }
}