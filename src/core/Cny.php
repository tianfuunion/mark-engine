<?php
declare (strict_types=1);

namespace mark\core;

use mark\exception\FunctionNotFoundException;
use mark\exception\UnexpectedValueException;

/**
 * 人民币转换类
 *
 * @author     Mark<mark@tianfuunion.cn>
 * @site       https://www.tianfuunion.cn
 * @time       2020年10月12日 10:24:00 初始创建
 * @modifyTime 2020年11月17日 17:00:00 重写货币大写转换算法,如果随后扩展货币数值超过万亿，则直接添加货币单位并修改2.1限制条件即可
 * @version    1.0
 * @package    mark\core
 * 计数单位：零、壹、贰、叁、肆、伍、陆、柒、捌、玖。
 * 计量单位：拾、佰、仟、万、拾万、佰万、仟万、亿、拾亿、佰亿、仟亿、万亿。基于目前露出的单位暂时到万亿，随后根据公开信息扩展。
 * 货币单位：元、角、分、厘、毫。
 * 以「十」开头，如十五，十万，十亿等。两位数以上，在数字中部出现，则用「一十几」，如一百一十，一千零一十，一万零一十等。
 * 可以把自主权将由调用者，方便可调，区别于完整版。
 * 「二」和「两」的问题。两亿，两万，两千，两百，都可以，但是20只能是二十，200用二百也更好。22,2222,2222是「二十二亿两千二百二十二万两千二百二十二」
 * 关于「零」和「〇」的问题，数字中一律用「零」，只有页码、年代等编号中数的空位才能用「〇」。
 * 数位中间无论多少个0，都读成一个「零」。
 * 2014是「两千零一十四」，200014是「二十万零一十四」，201400是「二十万零一千四百」
 * 中文数字的读法
 * @link       https://jingyan.baidu.com/article/636f38bb3cfc88d6b946104b.html
 * 人民币写法参考：正确填写票据和结算凭证的基本规定
 * @link       https://bbs.chinaacc.com/forum-2-35/topic-1181907.html
 */
final class Cny {
    // 没有找到相关官方标准，暂时以最大9万亿为准
    public static $MAX_DIGITAL = '9999999999999.9999';
    public static $MAX_INTEGRAL = '9999999999999'; // 最大整数

    // 数字大写
    public static $CAPITALIZE_DIGITAL = array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');

    // 计量单位
    public static $MEASURE_UNIT = array('元', '拾', '佰', '仟', '万', '拾', '佰', '仟', '亿', '拾', '佰', '仟', '万', '拾');
    // 基本单位
    public static $BASIC_UNIT = array('角', '分', '厘', '毫');

    private function __construct() { }

    /**
     * 人民币转大写
     *
     * @param string $amount   货币数值
     * @param int    $scale    小数点后最多4位
     * @param bool   $complete 是否开启完整版
     *
     * @return string 大写的货币
     * @throws \mark\exception\FunctionNotFoundException
     * @throws \mark\exception\UnexpectedValueException
     * @example 123456789.0123 ￥壹亿贰仟叁佰肆拾伍万陆仟柒佰捌拾玖元 _ 零角壹分贰厘叁毫⊗
     */
    public static function toupper($amount = '', int $scale = 2, bool $complete = false): string {
        if (!extension_loaded('bcmath') || !function_exists('bccomp')) {
            throw new FunctionNotFoundException('PHP extension "bcmath" is required.', 'bccomp');
        }

        // 规范化输入数字的格式
        $amount = (string)$amount;

        // 删除逗号分隔符。
        $amount = str_replace('/,/g', '', $amount);

        // 判断输出的金额是否为数字或数字字符串
        if (!is_numeric($amount)) {
            throw new UnexpectedValueException('金额只能为数字');
        }

        // 金额数值不能超过9万亿及更高金额
        if (bccomp((string)$amount, self::$MAX_DIGITAL) === 1) {
            // return '金额过大，不能大于超过' . self::$MAX_DIGITAL;
            throw new UnexpectedValueException('金额过大，不能大于' . self::$MAX_DIGITAL);
        }

        // 0 <= 小数位数 <= 4
        if ($scale >= 4) {
            $scale = 4;
        }
        if ($scale <= 0) {
            $scale = 0;
        }

        // 金额为0，则直接输出"零元整"
        if (bccomp($amount, '0', $scale) === 0) {
            return '零元整';
        }

        $result = '';

        // 金额为负数
        if (strpos((string)$amount, '-') === 0) {
            $result = '负';
            $amount = substr($amount, 1);
        }

        // 非完整模式，则去除前导0
        if (!$complete) {
            $amount = ltrim($amount, '0');
            // $amount = str_replace('/^0 +/', '', $amount);
            // $amount = preg_replace('/(0+)(\d+)/i', "\$2", $amount);
        }

        // 1、截取小数点，将金额的数值字符串拆分成数组
        $parts = explode('.', $amount);

        $integer = $parts[0];
        if (count($parts) > 1) {
            $decimal = $parts[1];
            $decimal = substr((string)$decimal, 0, $scale);
        } else {
            $decimal = '';
        }

        //2、小数点前为整数
        if (!empty((string)$integer) && (int)$integer > 0) {
            // 2.1、将整数位的数值字符串拆分成数组并翻转顺序
            $arr = array();
            $reverse = array_reverse(str_split($integer));

            // 2.2、以数字为键，倒序数字所引为值，形成数组[值+单位]
            foreach ($reverse as $key => $value) {
                if ($complete) {
                    $arr[] = self::$CAPITALIZE_DIGITAL[$value] . self::$MEASURE_UNIT[$key];
                } elseif ((int)$value !== 0) {
                    $arr[] = self::$CAPITALIZE_DIGITAL[$value] . self::$MEASURE_UNIT[$key];
                } elseif ($key === 0) {
                    $arr[] = '元';
                } elseif ($key === 4) {
                    $arr[] = '万';
                } elseif ($key === 8) {
                    $arr[] = '亿';
                } elseif ($key === 12) {
                    $arr[] = '万';
                } else {
                    $arr[] = '0';
                }
            }

            // 2.3、再次翻转数组形成正序,并连接为字符串
            $result .= implode(array_reverse($arr));
        } else {
            $result .= '零元';
        }

        // 3、小数点后为小数,不能为空，0
        if (!empty($decimal) && bccomp($decimal, '0', $scale) !== 0) {
            // 3.1、小数不用翻转，直接取值即可
            $array = array();
            $split = str_split($decimal);
            foreach ($split as $index => $item) {
                // 超过指定小数位数直接返回
                if ($index >= $scale) {
                    break;
                }

                if ((int)$item !== 0 || $complete) {
                    $array[] = self::$CAPITALIZE_DIGITAL[$item] . self::$BASIC_UNIT[$index];
                } else {
                    $array[] = '0';
                }
            }

            $result .= implode($array);
        } else {
            $result .= '整';
        }

        if (!$complete) {
            // 去除中间多个零的情况
            $result = preg_replace(array('/(0)+/'), '零', $result);

            // 去除【万零亿】
            $result = preg_replace(array('/(万零亿)+/'), '万亿', $result);
            $result = preg_replace(array('/(万零万)+/'), '万', $result);
            $result = preg_replace(array('/(万零仟)+/'), '万', $result);
            $result = preg_replace(array('/(万零佰)+/'), '万', $result);
            $result = preg_replace(array('/(万零拾)+/'), '万', $result);

            $result = preg_replace(array('/(亿零万)+/'), '亿', $result);
            $result = preg_replace(array('/(亿零仟)+/'), '亿', $result);
            $result = preg_replace(array('/(亿零佰)+/'), '亿', $result);
            $result = preg_replace(array('/(亿零拾)+/'), '亿', $result);
            $result = preg_replace(array('/(亿零拾)+/'), '亿', $result);

            $result = preg_replace(array('/(仟零亿)+/'), '仟亿', $result);
            $result = preg_replace(array('/(仟零万)+/'), '仟万', $result);

            $result = preg_replace(array('/(佰零亿)+/'), '佰亿', $result);
            $result = preg_replace(array('/(佰零万)+/'), '佰万', $result);

            $result = preg_replace(array('/(拾零亿)+/'), '拾亿', $result);
            $result = preg_replace(array('/(拾零万)+/'), '拾万', $result);

            // 大于1元时去除0元
            if (bccomp((string)$amount, '1', $scale) === 1) {
                $result = preg_replace(array('/(零元)+/'), '元', $result);
            }
        }

        return $result;
    }

    /**
     * 人民币转换为小写
     *
     * @param string $currency
     *
     * @return float|int
     * @example 壹万贰仟叁佰肆拾伍亿陆仟柒佰捌拾玖万零壹佰贰拾叁元肆角伍分陆厘柒毫
     */
    public static function tolower(string $currency) {
        if (empty($currency)) {
            return 0;
        }

        $digital_string = 0000000000000.0000;

        // 1、以【亿_万_小数】分割字符串
        if (strpos('亿', $currency) !== false) {
            $haystack = explode('亿', $currency);
        } elseif (strpos('万', $currency) !== false) {
            $haystack = explode('万', $currency);
        } elseif (strpos('角', $currency) !== false) {
            $haystack = explode('角', $currency);
        } elseif (strpos('分', $currency) !== false) {
            $haystack = explode('分', $currency);
        } elseif (strpos('厘', $currency) !== false) {
            $haystack = explode('厘', $currency);
        } elseif (strpos('毫', $currency) !== false) {
            $haystack = explode('毫', $currency);
        } else {
            return 0;
        }

        // 2、根据【单位段】计算出当前数值的定位
        foreach ($haystack as $key => $item) {
            $pos = self::$MEASURE_UNIT[$item];
            switch ($item) {
                case 0:
                    // 3、根据定位，替换掉相应的数值
                    $digital_string = substr_replace($item, 0, 0, 1);
                    break;
            }
        }

        return $digital_string;
    }

    /**
     * 分割货币：通过千位分组来格式化数字
     *
     * @desc 千位分隔符：西方的习惯，人们在数字中加进一个符号，以免因数字位数太多而难以看出它的值。
     *
     * @param string $amount 货币数值
     * @param int    $offset 分位偏移量
     * @param string $symbol 分位符号
     *
     * @return string
     * @see  number_format()
     * @see  format()
     */
    public static function separator($amount = '', int $offset = 3, string $symbol = ','): string {
        // 1、校验数据
        if (!is_numeric($amount)) {
            return $amount;
        }

        // 2、数值打散为数组
        $parts = explode('.', $amount);

        // 3、分割出整数部件
        $integral = $parts[0];
        if (count($parts) > 1) {
            $decimal = $parts[1];
        } else {
            $decimal = '';
        }

        // 4、整数长度小于最小分位则直接返回
        if (strlen((string)$integral) <= $offset) {
            return $amount;
        }

        $result = '';

        // 5、将整数部分按照偏移量分割成数组，先反转数值，从后往前分割
        $reverse = str_split(strrev((string)$integral), $offset);
        foreach ($reverse as $index => $item) {
            // 5.1、连接分位符号
            $result .= $item . $symbol;
        }

        // 5.2、再次反转，以正常方向输出。
        $result = strrev($result);

        // 6、连接小数部分
        if (!empty($decimal)) {
            $result .= '.' . $decimal;
        }

        // 7、可能有前置符号，去除掉
        return trim($result, ',');
    }

    /**
     * @param        $number
     * @param int    $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     *
     * @return string
     */
    public static function format($number, $decimals = 0, $dec_point = '.', $thousands_sep = ','): string {
        return number_format($number, $decimals, $dec_point, $thousands_sep);
    }
}