<?php
declare (strict_types=1);

namespace mark\core;

/**
 * @description  PHP Math
 * 优化原方法中只能对小数点之后进行取舍的操作
 * @author       Mark<mark@tianfuunion.cn>
 * @site https://www.tianfuunion.cn
 * @time         2019年10月12日 10:24:00
 * @modifyTime   2020年02月28日 00:38:00
 * @modifyTime   2020年07月25日 17:00:00
 * @modifyTime   2020年11月17日 20:16:00
 * @modifyTime   2022年06月22日 15:15:00 用BcMath方法进行算法重构
 * @package      mark\core
 */
class Math {
    private function __construct() { }

    /**
     * 进一法取整
     *
     * @param     $value
     * @param int $precision
     *
     * @return string
     * @uses \ceil()
     */
    public static function ceil($value, $precision = 0): string {
        if ($precision == 0) {
            return (string)(int)ceil((float)$value);
        }
        $scale = 0;

        $strrchr = strrchr((string)$value, '.');
        if (!empty($strrchr) && $strrchr != false) {
            $substr = substr($strrchr, 1);
            if (!empty($substr) && $substr != false) {
                $scale = strlen($substr);
            }
        }

        $scale -= $precision;

        $point = bcpow('10', (string)$precision, $scale);
        $bcmul = (float)bcmul((string)$value, $point, $scale);
        $ceil = (string)(int)ceil((float)$bcmul);
        if (bccomp((string)$ceil, '0') === 0) {
            return '0';
        }

        return bcdiv((string)$ceil, $point, $precision) ?: '0';
    }

    /**
     * 舍去法取整
     *
     * @param     $value
     * @param int $precision
     *
     * @return string
     * @uses \floor()
     */
    public static function floor($value, $precision = 0): string {
        if ($precision == 0) {
            return (string)(int)floor((float)$value);
        }
        $scale = 0;

        $strrchr = strrchr((string)$value, '.');
        if (!empty($strrchr) && $strrchr != false) {
            $substr = substr($strrchr, 1);
            if (!empty($substr) && $substr != false) {
                $scale = strlen($substr);
            }
        }

        $scale -= $precision;

        $point = bcpow('10', (string)$precision, $scale);
        $bcmul = (float)bcmul((string)$value, $point, $scale);
        $floor = (int)floor((float)$bcmul);
        if (bccomp((string)$floor, '0') === 0) {
            return '0';
        }

        return bcdiv((string)$floor, $point, $precision) ?: '0';
    }

    /**
     * 对浮点数进行四舍五入
     *
     * @param     $value
     * @param int $precision
     *
     * @return string
     * @uses       \round()
     */
    public static function round($value, $precision = 0): string {
        if ($precision == 0) {
            return (string)(int)round((float)$value);
        }
        $scale = 0;

        $strrchr = strrchr((string)$value, '.');
        if (!empty($strrchr) && $strrchr != false) {
            $substr = substr($strrchr, 1);
            if (!empty($substr) && $substr != false) {
                $scale = strlen($substr);
            }
        }

        $scale -= $precision;

        $point = bcpow('10', (string)$precision, $scale);
        $bcmul = (float)bcmul((string)$value, $point, $scale);
        $round = (int)round((float)$bcmul);
        if (bccomp((string)$round, '0') === 0) {
            return '0';
        }

        return bcdiv((string)$round, $point, $precision) ?: '0';
    }

}