<?php
declare (strict_types=1);

namespace mark\convert;

use mark\exception\UnexpectedValueException;

/**
 * Class ArrayToXml
 *
 * @package mark\convert
 */
class ArrayToXml {
    protected $document;

    protected $replaceSpacesByUnderScoresInKeyNames = true;

    protected $addXmlDeclaration = true;

    protected $numericTagNamePrefix = 'numeric_';

    /**
     * ArrayToXml constructor.
     *
     * @param array  $array
     * @param string $rootElement
     * @param bool   $replaceSpacesByUnderScoresInKeyNames
     * @param null   $xmlEncoding
     * @param string $xmlVersion
     * @param array  $domProperties
     * @param null   $xmlStandalone
     *
     * @throws \DOMException
     * @throws \Exception
     */
    public function __construct(
        array $array, $rootElement = '',
        $replaceSpacesByUnderScoresInKeyNames = true,
        $xmlEncoding = 'UTF-8',
        $xmlVersion = '1.0',
        $domProperties = [],
        $xmlStandalone = null
    ) {
        $this->document = new \DOMDocument($xmlVersion, $xmlEncoding);

        if (!is_null($xmlStandalone)) {
            $this->document->xmlStandalone = $xmlStandalone;
        }

        if (!empty($domProperties)) {
            $this->setDomProperties($domProperties);
        }

        $this->replaceSpacesByUnderScoresInKeyNames = $replaceSpacesByUnderScoresInKeyNames;

        if ($this->isArrayAllKeySequential($array) && !empty($array)) {
            throw new \DOMException('Invalid Character Error');
        }

        $root = $this->createRootElement($rootElement);

        $this->document->appendChild($root);

        $this->convertElement($root, $array);
    }

    /**
     * @param string $prefix
     */
    public function setNumericTagNamePrefix(string $prefix): void {
        $this->numericTagNamePrefix = $prefix;
    }

    /**
     * @param array       $array
     * @param string      $rootElement
     * @param bool        $replaceSpacesByUnderScoresInKeyNames
     * @param string|null $xmlEncoding
     * @param string      $xmlVersion
     * @param array       $domProperties
     * @param bool|null   $xmlStandalone
     *
     * @return string
     * @throws \DOMException
     */
    public static function convert(
        array $array, $rootElement = '',
        bool $replaceSpacesByUnderScoresInKeyNames = true,
        string $xmlEncoding = 'UTF-8',
        string $xmlVersion = '1.0',
        array $domProperties = [],
        bool $xmlStandalone = null
    ): string {
        $converter = new static($array,
            $rootElement,
            $replaceSpacesByUnderScoresInKeyNames,
            $xmlEncoding,
            $xmlVersion,
            $domProperties,
            $xmlStandalone);

        return $converter->toXml();
    }

    public function toXml(): string {
        if ($this->addXmlDeclaration === false) {
            return $this->document->saveXML($this->document->documentElement);
        }

        return $this->document->saveXML();
    }

    public function toDom(): \DOMDocument {
        return $this->document;
    }

    /**
     * @param array $domProperties
     *
     * @throws \mark\exception\UnexpectedValueException
     */
    protected function ensureValidDomProperties(array $domProperties): void {
        foreach ($domProperties as $key => $value) {
            if (!property_exists($this->document, $key)) {
                throw new UnexpectedValueException($key . ' is not a valid property of DOMDocument');
            }
        }
    }

    /**
     * @param array $domProperties
     *
     * @return $this
     * @throws \mark\exception\UnexpectedValueException
     */
    public function setDomProperties(array $domProperties): self {
        $this->ensureValidDomProperties($domProperties);

        foreach ($domProperties as $key => $value) {
            $this->document->{$key} = $value;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function prettify(): self {
        $this->document->preserveWhiteSpace = false;
        $this->document->formatOutput = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function dropXmlDeclaration(): self {
        $this->addXmlDeclaration = false;

        return $this;
    }

    /**
     * @param \DOMElement $element
     * @param             $value
     */
    private function convertElement(\DOMElement $element, $value): void {
        $sequential = $this->isArrayAllKeySequential($value);

        if (!is_array($value)) {
            $value = htmlspecialchars((string)$value);
            $value = $this->removeControlCharacters($value);

            $element->nodeValue = $value;
            return;
        }

        foreach ($value as $key => $data) {
            if (!$sequential) {
                if (($key === '_attributes') || ($key === '@attributes')) {
                    $this->addAttributes($element, $data);
                } elseif ((($key === '_value') || ($key === '@value')) && is_string($data)) {
                    $element->nodeValue = htmlspecialchars($data);
                } elseif ((($key === '_cdata') || ($key === '@cdata')) && is_string($data)) {
                    $element->appendChild($this->document->createCDATASection($data));
                } elseif ((($key === '_mixed') || ($key === '@mixed')) && is_string($data)) {
                    $fragment = $this->document->createDocumentFragment();
                    $fragment->appendXML($data);
                    $element->appendChild($fragment);
                } elseif ($key === '__numeric') {
                    $this->addNumericNode($element, $data);
                } elseif (strpos($key, '__custom:') === 0) {
                    $this->addNode($element, explode(':', $key)[1], $data);
                } else {
                    $this->addNode($element, $key, $data);
                }
            } elseif (is_array($data)) {
                $this->addCollectionNode($element, $data);
            } else {
                $this->addSequentialNode($element, $data);
            }
        }
    }

    /**
     * @param \DOMElement $element
     * @param             $value
     */
    protected function addNumericNode(\DOMElement $element, $value): void {
        foreach ($value as $key => $item) {
            $this->convertElement($element, [$this->numericTagNamePrefix . $key => $item]);
        }
    }

    /**
     * @param \DOMElement $element
     * @param             $key
     * @param             $value
     */
    protected function addNode(\DOMElement $element, $key, $value): void {
        if ($this->replaceSpacesByUnderScoresInKeyNames) {
            $key = str_replace(' ', '_', $key);
        }

        $child = $this->document->createElement($key);
        $element->appendChild($child);
        $this->convertElement($child, $value);
    }

    /**
     * @param \DOMElement $element
     * @param             $value
     */
    protected function addCollectionNode(\DOMElement $element, $value): void {
        if ($element->childNodes->length === 0 && $element->attributes->length === 0) {
            $this->convertElement($element, $value);
            return;
        }

        $child = $this->document->createElement($element->tagName);
        $element->parentNode->appendChild($child);
        $this->convertElement($child, $value);
    }

    /**
     * @param \DOMElement $element
     * @param             $value
     */
    protected function addSequentialNode(\DOMElement $element, $value): void {
        if (empty($element->nodeValue) && !is_numeric($element->nodeValue)) {
            $element->nodeValue = htmlspecialchars($value);
            return;
        }

        $child = new \DOMElement($element->tagName);
        $child->nodeValue = htmlspecialchars($value);
        $element->parentNode->appendChild($child);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    protected function isArrayAllKeySequential($value): bool {
        if (!is_array($value)) {
            return false;
        }

        if (count($value) <= 0) {
            return true;
        }

        if (key($value) === '__numeric') {
            return false;
        }

        return array_unique(array_map('is_int', array_keys($value))) === [true];
    }

    /**
     * @param \DOMElement $element
     * @param array       $data
     */
    protected function addAttributes(\DOMElement $element, array $data): void {
        foreach ($data as $attrKey => $attrVal) {
            $element->setAttribute($attrKey, $attrVal);
        }
    }

    /**
     * @param $rootElement
     *
     * @return \DOMElement
     */
    protected function createRootElement($rootElement): \DOMElement {
        if (is_string($rootElement)) {
            $rootElementName = $rootElement ?: 'root';
            return $this->document->createElement($rootElementName);
        }

        $rootElementName = $rootElement['rootElementName'] ?? 'root';
        $element = $this->document->createElement($rootElementName);
        foreach ($rootElement as $key => $value) {
            if ($key !== '_attributes' && $key !== '@attributes') {
                continue;
            }

            $this->addAttributes($element, $value);
        }

        return $element;
    }

    protected function removeControlCharacters(string $value): string {
        return preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $value);
    }
}