<?php
declare (strict_types=1);

namespace mark\convert;

/**
 * Array XML View convert
 *
 * @package mark
 */
class XmlToArray {

    /**
     * @param $xml
     *
     * @return array|array[]|string[]
     */
    public static function xml_to_array($xml): array {
        $dom = new \DOMDocument();
        $dom->loadXML($xml);
        return self::domnode_to_array($dom->documentElement);
    }

    /**
     * @param $node
     *
     * @return array|array[]|mixed|string|string[]
     */
    private static function domnode_to_array($node) {
        $output = null;
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = self::domnode_to_array($child);
                    if (isset($child->tagName)) {
                        $t = $child->tagName;
                        if (!isset($output[(string)$t])) {
                            $output[$t] = null;
                        }
                        $output[(string)$t][] = $v;
                    } elseif ($v || $v == 0) {
                        $output = (string)$v;
                    }
                }
                if ($node->attributes->length && !is_array($output)) { //Has attributes but isn't an array
                    $output = array('@content' => $output);            //Change output into an array.
                }
                if (is_array($output)) {
                    if ($node->attributes->length) {
                        $a = array();
                        foreach ($node->attributes as $attrName => $attrNode) {
                            $a[$attrName] = (string)$attrNode->value;
                        }
                        $output['@attributes'] = $a;
                    }
                    foreach ($output as $t => $v) {
                        if (is_array($v) && count($v) == 1 && $t !== '@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }
        return $output;
    }

    /**
     * 简易数组转换为XML
     *
     * @param array $arr
     *
     * @return string
     */
    public static function simple_array_to_xml(array $arr): string {
        $xml = '<xml>';
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $xml .= '<' . $key . '>' . self::simple_array_to_xml($val) . '</' . $key . '>';
            } else {
                $xml .= '<' . $key . '>' . $val . '</' . $key . '>';
            }
        }
        $xml .= '</xml>';
        $xml = html_entity_decode($xml);
        return $xml;
    }

    /**
     * 转换函数
     *
     * @param array $arr
     * @param int   $depth
     *
     * @return string
     */
    public static function arr2xml(array $arr, int $depth = 0): string {
        if ($depth == 0) {
            $simxml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><root></root>');//创建simplexml对象
        } else {
            $simxml = new \SimpleXMLElement('<xml></xml>');//创建simplexml对象
        }
        //遍历数组，循环添加到root节点中
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $simxml->addChild((string)$k, self::arr2xml($v, $depth++));
            } else {
                $simxml->addChild((string)$k, (string)$v);
            }
        }
        //返回xml数据
        $xml = $simxml->saveXML();
        return html_entity_decode($xml);
    }

    /**
     * 简易XML转换为数组
     *
     * @param string $xml
     *
     * @return array
     */
    public static function simple_xml_to_array(string $xml): array {
        $xml = html_entity_decode($xml);
        $SimpleXMLElement = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $json = json_encode($SimpleXMLElement, JSON_UNESCAPED_UNICODE);
        return json_decode($json, true);
    }

    /**
     * @param array  $array
     * @param false  $eIsArray
     * @param string $root
     * @param string $encoding
     * @param string $version
     *
     * @return array|string
     */
    public static function array_to_xml_writer(array $array, $eIsArray = false, $root = 'root', $encoding = 'UTF-8', $version = '1.0') {
        $xml = new \XMLWriter();

        if (!$eIsArray) {
            $xml->openMemory();
            $xml->startDocument($version, $encoding);
            $xml->startElement($root);
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $xml->startElement($key);
                self::array_to_xml_writer($value, true);
                $xml->endElement();
                continue;
            }
            if (is_string($value) || is_int($value) || is_float($value) || is_bool($value)) {
                $xml->writeElement($key, $value);
            }
        }

        if (!$eIsArray) {
            $xml->endElement();
            return $xml->outputMemory();
        }

        return array();
    }

    /**
     * 类似 XPATH 的数组选择器
     *
     * @param $arr
     * @param $arrpath
     *
     * @return mixed
     */
    public static function xml_array_select($arr, $arrpath) {
        $arrpath = trim($arrpath, '/');
        if (!$arrpath) {
            return $arr;
        }

        $self = 'xml_array_select';
        $pos = strpos($arrpath, '/');
        $pos = $pos ?: strlen($arrpath);
        $curpath = substr($arrpath, 0, $pos);
        $next = substr($arrpath, $pos);

        if (preg_match("/\\[(\\d+)]$/", $curpath, $predicate)) {
            $curpath = substr($curpath, 0, strpos($curpath, '[' . $predicate[1] . ']'));
            $result = $arr[$curpath][$predicate[1]];
        } else {
            $result = $arr[$curpath];
        }

        if (is_array($arr) && !array_key_exists($curpath, $arr)) {
            die('key is not exists:' . $curpath);
        }

        return $self($result, $next);
    }

    /**
     * 如果输入的数组是全数字键，则将元素值依次传输到 $callback, 否则将自身传输给$callback
     *
     * @param $arr
     * @param $callback
     *
     * @return array
     */
    public static function xml_array_each($arr, $callback): array {
        if (func_num_args() < 2) {
            die('parameters error');
        }
        if (!is_array($arr)) {
            die('parameter 1 shuld be an array!');
        }
        if (!is_callable($callback)) {
            die('parameter 2 shuld be an function!');
        }

        $keys = array_keys($arr);
        $isok = true;
        foreach ($keys as $key) {
            if (!is_int($key)) {
                $isok = false;
                break;
            }
        }

        $result = array();
        if ($isok) {
            foreach ($arr as $val) {
                $result[] = $callback($val);
            }
        } else {
            $result[] = $callback($arr);
        }

        return $result;
    }

}