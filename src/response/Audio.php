<?php
declare (strict_types=1);

namespace mark\response;

use mark\exception\FileException;
use think\Cookie;
use think\Request;

/**
 * Class Audio
 *
 * @package mark\response
 */
class Audio extends Response {
    protected $expire = 360;
    protected $mimeType;
    private $isContent = false;

    /**
     * Image constructor.
     *
     * @param Cookie $cookie
     * @param string $data
     * @param int    $code
     */
    private function __construct(Cookie $cookie, $data = '', int $code = 200) {
        parent::__construct($data, $code, '', '', 'image');

        $this->cookie = $cookie;
    }

    /**
     * @param     $data
     * @param int $code
     *
     * @return static
     */
    public static function display($data, int $code = 200): self {
        $cookie = new Cookie(new Request());

        return new self($cookie, $data, $code);
    }

    /**
     * 处理数据
     *
     * @access protected
     *
     * @param mixed $content 要处理的数据
     *
     * @return mixed
     */
    protected function output($content) {
        if (empty($content) || !is_file($content)) {
            throw new FileException('file not exists:' . $content);
        }

        // ob_end_clean();
        $this->headers['Pragma'] = 'public';
        $this->headers['Content-Type'] = $this->getMimeType($content);
        $this->headers['Cache-control'] = 'max-age=' . $this->expire;
        $this->headers['Content-Length'] = $this->getImageLength($content);
        // $this->header['Content-Transfer-Encoding'] = 'binary';
        $this->headers['Expires'] = gmdate('D, d M Y H:i:s', time() + $this->expire) . ' GMT';
        $this->lastModified(gmdate('D, d M Y H:i:s', time()) . ' GMT');
        $this->contentType($this->getMimeType($content));

        return parent::output($content);
    }

    /**
     * 设置是否为内容 必须配合mimeType方法使用
     *
     * @param bool $content
     *
     * @return $this
     */
    public function isContent(bool $content = true): self {
        $this->isContent = $content;

        return $this;
    }

    /**
     * 设置有效期
     *
     * @param int $expire 有效期
     *
     * @return $this
     */
    public function expire(int $expire): self {
        $this->expire = $expire;

        return $this;
    }

    /**
     * 设置文件类型
     *
     * @param string $mimeType
     *
     * @return $this
     */
    public function setMimeType(string $mimeType): self {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * 获取文件类型信息
     *
     * @param \Imagick $imagick
     * @param string   $default
     *
     * @return string
     */
    protected function getMimeType(\Imagick $imagick, $default = 'audio/mp3'): string {
        if (empty($this->mimeType) && !empty($imagick) && $imagick instanceof \Imagick) {
            $this->mimeType = $imagick->getImageMimeType();
        }

        if (!empty($this->mimeType)) {
            return $this->mimeType;
        }

        return $default;
    }

    /**
     * 获取图片大小
     *
     * @param $imagick
     *
     * @return int
     */
    protected function getImageLength($imagick): int {
        if (!empty($imagick) && $imagick instanceof \Imagick) {
            return $imagick->getImageLength();
        }

        return 0;
    }
}