<?php
declare (strict_types=1);

namespace mark\response;

use think\Cookie;

/**
 * Json Response
 */
class Json extends Response {
    // 输出参数
    protected $options = [
        'json_encode_param' => JSON_UNESCAPED_UNICODE,
    ];

    protected $contentType = 'application/json';

    /**
     * Json constructor.
     *
     * @param \think\Cookie $cookie
     * @param string        $data
     * @param int           $code
     * @param string        $status
     * @param string        $msg
     *
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\BadMessageException
     */
    public function __construct(Cookie $cookie, $data = '', int $code = 200, string $status = '', string $msg = '') {
        parent::__construct($data, $code, $status, $msg, 'json');

        $this->contentType($this->contentType);
        $this->cookie = $cookie;
    }

    /**
     * 处理数据
     *
     * @access protected
     *
     * @param mixed $content 要处理的数据
     *
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     * @throws \Exception
     */
    protected function output($content): string {
        try {
            // 返回JSON数据格式到客户端 包含状态信息
            $content = json_encode($content, $this->options['json_encode_param']);
            if (false === $content) {
                throw new \mark\exception\InvalidArgumentException(json_last_error_msg());
            }

            return $content;
        } catch (\Exception $e) {
            if ($e->getPrevious()) {
                throw $e->getPrevious();
            }
            throw $e;
        }
    }

}