<?php
declare (strict_types=1);

namespace mark\response;

use mark\core\Container;
use mark\http\Client\Response as HttpClientResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class Response
 *
 * @description HTTP请求响应输出基础类
 * @package     mark\response
 */
class Response extends HttpClientResponse implements ResponseInterface {

    /** @var array Map of standard HTTP status code/reason phrases */
    public static $phrases = [
        // 1开头的状态码:消息
        100 => ['status' => 'Continue', 'reason' => '请继续发送请求', 'describe' => '继续。客户端应继续其请求'],
        101 => ['status' => 'Switching Protocols', 'reason' => '请使用新的HTTP版本协议', 'describe' => '切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，例如，切换到HTTP的新版本协议'],
        102 => ['status' => 'Processing', 'reason' => '请求持续处理中', 'describe' => '由WebDAV（RFC 2518）扩展的状态码，代表处理将被继续执行。'],
        103 => ['status' => 'Checkpoint ', 'reason' => '检查点', 'describe' => ''],

        // 2开头的状态码:成功
        200 => ['status' => 'OK', 'reason' => '请求成功', 'describe' => '一般用于GET与POST请求'],
        201 => ['status' => 'Created', 'reason' => '已创建', 'describe' => '请求成功并创建了新的资源'],
        202 => ['status' => 'Accepted', 'reason' => '已接受请求', 'describe' => '已接受。已经接受请求，但尚未处理完成'],
        203 => ['status' => 'Non-Authoritative Information', 'reason' => '信息未授权', 'describe' => '非授权信息。请求成功。但返回的meta信息不在原始的服务器，而是一个副本'],
        204 => ['status' => 'No Content', 'reason' => '未返回内容', 'describe' => '无内容。服务器成功处理，但未返回内容。在未更新网页的情况下，可确保浏览器继续显示当前文档'],
        205 => ['status' => 'Reset Content', 'reason' => '处理成功，请重置视图', 'describe' => '重置内容。服务器处理成功，用户终端（例如：浏览器）应重置文档视图。可通过此返回码清除浏览器的表单域'],
        206 => ['status' => 'Partial Content', 'reason' => '处理成功了部分请求', 'describe' => '部分内容。服务器成功处理了部分GET请求'],
        207 => ['status' => 'Multi-Status ', 'reason' => '多状态', 'describe' => '对于多个状态代码都可能合适的情况，传输有关多个资源的信息。'],
        208 => ['status' => 'Already Reported', 'reason' => '已报告', 'describe' => ''],

        // 3开头的状态码:重定向
        300 => ['status' => 'Multiple Choices', 'reason' => '请选择合适的操作', 'describe' => '多种选择。请求的资源可包括多个位置，相应可返回一个资源特征与地址的列表用于用户终端（例如：浏览器）选择'],
        301 => ['status' => 'Moved Permanently', 'reason' => '永久移动', 'describe' => '请求的资源已被永久的移动到新的位置。请求的资源已被永久的移动到新URI，返回信息会包括新的URI，浏览器会自动定向到新URI。今后任何新的请求都应使用新的URI代替'],
        302 => ['status' => 'Found', 'reason' => '临时移动', 'describe' => '请求的资源已被临时移动。与301类似。但资源只是临时被移动。客户端应继续使用原有URI'],
        303 => ['status' => 'See Other', 'reason' => '请求自动跟踪跳转', 'describe' => '查看其它地址。与301类似。使用GET和POST请求查看'],
        304 => ['status' => 'Not Modified', 'reason' => '请求内容未修改', 'describe' => '未修改。所请求的资源未修改，服务器返回此状态码时，不会返回任何资源。客户端通常会缓存访问过的资源，通过提供一个头信息指出客户端希望只返回在指定日期之后修改的资源'],
        305 => ['status' => 'Use Proxy', 'reason' => '请使用代理访问资源', 'describe' => '使用代理。所请求的资源必须通过代理访问'],
        306 => ['status' => 'Switch Proxy', 'reason' => '开关代理', 'describe' => '已经被废弃的HTTP状态码Unused'],
        307 => ['status' => 'Temporary Redirect', 'reason' => '请求临时重定向', 'describe' => '临时重定向。与302类似。使用GET请求重定向'],

        // 4开头的状态码:请求错误
        400 => ['status' => 'Bad Request', 'reason' => '请求参数错误', 'describe' => '客户端请求的语法错误，服务器无法理解'],
        401 => ['status' => 'Unauthorized', 'reason' => '用户未登录', 'describe' => '请求要求用户的身份认证'],
        402 => ['status' => 'Payment Required', 'reason' => '请求需要付费', 'describe' => '用于数字支付系统'],
        403 => ['status' => 'Forbidden', 'reason' => '服务器拒绝执行此请求', 'describe' => '服务器理解请求客户端的请求，但是拒绝执行此请求'],
        404 => ['status' => 'Not Found', 'reason' => '请求失败，你所请求的资源无法找到', 'describe' => '服务器无法根据客户端的请求找到资源（网页）。通过此代码，网站设计人员可设置"你所请求的资源无法找到"的个性页面'],
        405 => ['status' => 'Method Not Allowed', 'reason' => '请求中的方法已被禁止', 'describe' => '客户端请求中的方法被禁止'],
        406 => ['status' => 'Not Acceptable', 'reason' => '无法根据请求的内容特性完成请求', 'describe' => '服务器无法根据客户端请求的内容特性完成请求'],
        407 => ['status' => 'Proxy Authentication Required', 'reason' => '用户身份未经授权认证', 'describe' => '请求要求代理的身份认证，与401类似，但请求者应当使用代理进行授权'],
        408 => ['status' => 'Request Time-out', 'reason' => '请求超时，请重新发起请求', 'describe' => '服务器等待客户端发送的请求时间过长，超时'],
        409 => ['status' => 'Conflict', 'reason' => '请求无法完成，资源发生了冲突', 'describe' => '服务器完成客户端的PUT请求是可能返回此代码，服务器处理请求时发生了冲突'],
        410 => ['status' => 'Gone', 'reason' => '请求资源不可用', 'describe' => '客户端请求的资源已经不存在。410不同于404，如果资源以前有现在被永久删除了可使用410代码，网站设计人员可通过301代码指定资源的新位置'],
        411 => ['status' => 'Content Length Required', 'reason' => '无效的请求长度', 'describe' => '服务器无法处理客户端发送的不带Content-Length的请求信息'],
        412 => ['status' => 'Precondition Failed', 'reason' => '错误的请求条件', 'describe' => '客户端请求信息的先决条件错误'],
        413 => ['status' => 'Request Entity Too Large', 'reason' => '拒绝请求，负载过大', 'describe' => '请求实体大于服务器定义的限制。服务器可能会关闭连接，或在标头字段后返回重试Retry-After。'],
        414 => ['status' => 'Request-URI Too Large', 'reason' => '拒绝请求，Url过长', 'describe' => '请求的URI过长（URI通常为网址），服务器无法处理'],
        415 => ['status' => 'Unsupported Media Type', 'reason' => '无效的媒体格式', 'describe' => '服务器无法处理请求附带的媒体格式'],
        416 => ['status' => 'Requested range not satisfiable', 'reason' => '无效的请求范围', 'describe' => '客户端请求的范围无效'],
        417 => ['status' => 'Expectation Failed', 'reason' => 'Expect 的内容无法被满足', 'describe' => '服务器无法满足Expect的请求头信息；在请求头 Expect 中指定的预期内容无法被服务器满足。'],
        418 => ['status' => 'I`m a teapot', 'reason' => '', 'describe' => '我是一个茶壶'],
        419 => ['status' => 'User account is blocked', 'reason' => '用户账号被冻结', 'describe' => ''],
        421 => ['status' => 'Misdirected Request', 'reason' => '误导请求', 'describe' => '请求被指向到无法生成响应的服务器（比如由于连接重复使用）'],
        422 => ['status' => 'Unprocessable Entity', 'reason' => '无法处理的实体', 'describe' => '请求格式正确，但是由于含有语义错误，无法响应。（RFC 4918 WebDAV）'],
        423 => ['status' => 'Locked', 'reason' => '资源已被锁定', 'describe' => '当前资源被锁定。（RFC 4918 WebDAV）'],
        424 => ['status' => 'Failed Dependency', 'reason' => '请求失败，依赖错误', 'describe' => '由于之前的某个请求发生的错误，导致当前请求失败，例如 PROPPATCH。（RFC 4918 WebDAV）'],
        425 => ['status' => 'Too Early', 'reason' => '请求有风险，拒绝处理', 'describe' => '服务器不愿意冒风险来处理该请求，原因是处理该请求可能会被“重放”，从而造成潜在的重放攻击。（RFC 8470）'],
        426 => ['status' => 'Upgrade Required', 'reason' => '请使用加密协议访问', 'describe' => '客户端应当切换到TLS/1.0。（RFC 2817）'],
        428 => ['status' => 'Precondition Required', 'reason' => '所需的先决条件', 'describe' => '源服务器要求请求是有条件的'],
        429 => ['status' => 'Too Many Requests', 'reason' => '请求过于频繁', 'describe' => '用户在一定的时间内发送了太多请求（"请限制请求速率"）'],
        431 => ['status' => 'Request Header Fields Too Large', 'reason' => '请求头字段太大', 'describe' => ''],
        444 => ['status' => 'No Response', 'reason' => '请求无响应', 'describe' => ''],
        449 => ['status' => 'Please try again later', 'reason' => '请在执行完操作后进行重试。', 'describe' => '由微软扩展，代表请求应当在执行完适当的操作后进行重试。'],
        451 => ['status' => 'Unavailable For Legal Reasons', 'reason' => '由于法律原因无法使用', 'describe' => '用户代理请求了无法合法提供的资源，例如政府审查的网页。'],

        // 5开头的状态码:服务器错误
        500 => ['status' => 'Internal Server Error', 'reason' => '服务异常', 'describe' => '服务器内部错误，无法完成请求'],
        501 => ['status' => 'Not Implemented', 'reason' => '无法识别的请求', 'describe' => '服务器不支持请求的功能，无法完成请求'],
        502 => ['status' => 'Bad Gateway', 'reason' => '无效的网关请求', 'describe' => '充当网关或代理的服务器，从远端服务器接收到了一个无效的请求'],
        503 => ['status' => 'Service Unavailable', 'reason' => '系统维护，暂停服务', 'describe' => '由于超载或系统维护，服务器暂时的无法处理客户端的请求。延时的长度可包含在服务器的Retry-After头信息中'],
        504 => ['status' => 'Gateway Time-out', 'reason' => '网关请求超时，请稍后重试', 'describe' => '充当网关或代理的服务器，未及时从远端服务器获取请求'],
        505 => ['status' => 'HTTP Version not supported', 'reason' => '请求协议不支持', 'describe' => '服务器不支持请求的HTTP协议的版本，无法完成处理'],
        506 => ['status' => 'Service internal negotiation error', 'reason' => '服务内部配置错误', 'describe' => '所选的变体资源被配置为参与透明内容协商本身，因此不是协商过程中的适当终点。'],
        507 => ['status' => 'Insufficient Storage', 'reason' => '系统空间不足', 'describe' => '服务器无法存储完成请求所必须的内容。这个状况被认为是临时的。WebDAV (RFC 4918)'],
        508 => ['status' => 'Loop Detected', 'reason' => '检测到循环', 'describe' => '服务器在处理请求时检测到无限循环。'],
        509 => ['status' => 'Bandwidth Limit Exceeded', 'reason' => '服务器达到带宽限制', 'describe' => '服务器达到带宽限制。这不是一个官方的状态码，但是仍被广泛使用。'],
        510 => ['status' => 'Not Extended', 'reason' => '无效资源策略', 'describe' => '获取资源所需要的策略并没有没满足。（RFC 2774）'],
        511 => ['status' => 'Network Authentication Required', 'reason' => '网络需要身份验证', 'describe' => '指示客户端需要进行身份验证才能获得网络访问权限。']];

    /**
     * MIME type
     *
     * @alias media_type
     * @var array
     */
    public static $mime_type = [
        'suffix' => ['Content-Type' => 'application/json', 'mime' => 'application', 'type' => 'json']
    ];

    /**
     * @description 常见文件mime类型
     * @link        https://blog.csdn.net/walle2928/article/details/88050558
     * @var array
     */
    public static $mimetypes = [
        '123' => 'application/vnd.lotus-1-2-3',
        '3dml' => 'text/vnd.in3d.3dml',
        '3ds' => 'image/x-3ds',
        '3g2' => 'video/3gpp2',
        '3gp' => 'video/3gpp',
        '7z' => 'application/x-7z-compressed',
        'aab' => 'application/x-authorware-bin',
        'aac' => 'audio/x-aac',
        'aam' => 'application/x-authorware-map',
        'aas' => 'application/x-authorware-seg',
        'abs' => 'audio/x-mpeg',
        'abw' => 'application/x-abiword',
        'ac' => 'application/pkix-attr-cert',
        'acc' => 'application/vnd.americandynamics.acc',
        'ace' => 'application/x-ace-compressed',
        'acu' => 'application/vnd.acucobol',
        'acutc' => 'application/vnd.acucorp',
        'adp' => 'audio/adpcm',
        'aep' => 'application/vnd.audiograph',
        'afm' => 'application/x-font-type1',
        'afp' => 'application/vnd.ibm.modcap',
        'ahead' => 'application/vnd.ahead.space',
        'ai' => 'application/postscript',
        'aif' => 'audio/x-aiff',
        'aifc' => 'audio/x-aiff',
        'aiff' => 'audio/x-aiff',
        'aim' => 'application/x-aim',
        'air' => 'application/vnd.adobe.air-application-installer-package+zip',
        'ait' => 'application/vnd.dvb.ait',
        'ami' => 'application/vnd.amiga.ami',
        'anx' => 'application/annodex',
        'apk' => 'application/vnd.android.package-archive',
        'appcache' => 'text/cache-manifest',
        'application' => 'application/x-ms-application',
        'apr' => 'application/vnd.lotus-approach',
        'arc' => 'application/x-freearc',
        'art' => 'image/x-jg',
        'asc' => 'application/pgp-signature',
        'asf' => 'video/x-ms-asf',
        'asm' => 'text/x-asm',
        'aso' => 'application/vnd.accpac.simply.aso',
        'asx' => 'video/x-ms-asf',
        'atc' => 'application/vnd.acucorp',
        'atom' => 'application/atom+xml',
        'atomcat' => 'application/atomcat+xml',
        'atomsvc' => 'application/atomsvc+xml',
        'atx' => 'application/vnd.antix.game-component',
        'au' => 'audio/basic',
        'avi' => 'video/x-msvideo',
        'avx' => 'video/x-rad-screenplay',
        'aw' => 'application/applixware',
        'axa' => 'audio/annodex',
        'axv' => 'video/annodex',
        'azf' => 'application/vnd.airzip.filesecure.azf',
        'azs' => 'application/vnd.airzip.filesecure.azs',
        'azw' => 'application/vnd.amazon.ebook',
        'bat' => 'application/x-msdownload',
        'bcpio' => 'application/x-bcpio',
        'bdf' => 'application/x-font-bdf',
        'bdm' => 'application/vnd.syncml.dm+wbxml',
        'bed' => 'application/vnd.realvnc.bed',
        'bh2' => 'application/vnd.fujitsu.oasysprs',
        'bin' => 'application/octet-stream',
        'blb' => 'application/x-blorb',
        'blorb' => 'application/x-blorb',
        'bmi' => 'application/vnd.bmi',
        'bmp' => 'image/bmp',
        'body' => 'text/html',
        'book' => 'application/vnd.framemaker',
        'box' => 'application/vnd.previewsystems.box',
        'boz' => 'application/x-bzip2',
        'bpk' => 'application/octet-stream',
        'btif' => 'image/prs.btif',
        'bz' => 'application/x-bzip',
        'bz2' => 'application/x-bzip2',
        'c' => 'text/x-c',
        'c11amc' => 'application/vnd.cluetrust.cartomobile-config',
        'c11amz' => 'application/vnd.cluetrust.cartomobile-config-pkg',
        'c4d' => 'application/vnd.clonk.c4group',
        'c4f' => 'application/vnd.clonk.c4group',
        'c4g' => 'application/vnd.clonk.c4group',
        'c4p' => 'application/vnd.clonk.c4group',
        'c4u' => 'application/vnd.clonk.c4group',
        'cab' => 'application/vnd.ms-cab-compressed',
        'caf' => 'audio/x-caf',
        'cap' => 'application/vnd.tcpdump.pcap',
        'car' => 'application/vnd.curl.car',
        'cat' => 'application/vnd.ms-pki.seccat',
        'cb7' => 'application/x-cbr',
        'cba' => 'application/x-cbr',
        'cbr' => 'application/x-cbr',
        'cbt' => 'application/x-cbr',
        'cbz' => 'application/x-cbr',
        'cc' => 'text/x-c',
        'cct' => 'application/x-director',
        'ccxml' => 'application/ccxml+xml',
        'cdbcmsg' => 'application/vnd.contact.cmsg',
        'cdf' => 'application/x-cdf',
        'cdkey' => 'application/vnd.mediastation.cdkey',
        'cdmia' => 'application/cdmi-capability',
        'cdmic' => 'application/cdmi-container',
        'cdmid' => 'application/cdmi-domain',
        'cdmio' => 'application/cdmi-object',
        'cdmiq' => 'application/cdmi-queue',
        'cdx' => 'chemical/x-cdx',
        'cdxml' => 'application/vnd.chemdraw+xml',
        'cdy' => 'application/vnd.cinderella',
        'cer' => 'application/pkix-cert',
        'cfs' => 'application/x-cfs-compressed',
        'cgm' => 'image/cgm',
        'chat' => 'application/x-chat',
        'chm' => 'application/vnd.ms-htmlhelp',
        'chrt' => 'application/vnd.kde.kchart',
        'cif' => 'chemical/x-cif',
        'cii' => 'application/vnd.anser-web-certificate-issue-initiation',
        'cil' => 'application/vnd.ms-artgalry',
        'cla' => 'application/vnd.claymore',
        'class' => 'application/java',
        'clkk' => 'application/vnd.crick.clicker.keyboard',
        'clkp' => 'application/vnd.crick.clicker.palette',
        'clkt' => 'application/vnd.crick.clicker.template',
        'clkw' => 'application/vnd.crick.clicker.wordbank',
        'clkx' => 'application/vnd.crick.clicker',
        'clp' => 'application/x-msclip',
        'cmc' => 'application/vnd.cosmocaller',
        'cmdf' => 'chemical/x-cmdf',
        'cml' => 'chemical/x-cml',
        'cmp' => 'application/vnd.yellowriver-custom-menu',
        'cmx' => 'image/x-cmx',
        'cod' => 'application/vnd.rim.cod',
        'com' => 'application/x-msdownload',
        'conf' => 'text/plain',
        'cpio' => 'application/x-cpio',
        'cpp' => 'text/x-c',
        'cpt' => 'application/mac-compactpro',
        'crd' => 'application/x-mscardfile',
        'crl' => 'application/pkix-crl',
        'crt' => 'application/x-x509-ca-cert',
        'cryptonote' => 'application/vnd.rig.cryptonote',
        'csh' => 'application/x-csh',
        'csml' => 'chemical/x-csml',
        'csp' => 'application/vnd.commonspace',
        'css' => 'text/css',
        'cst' => 'application/x-director',
        'csv' => 'text/csv',
        'cu' => 'application/cu-seeme',
        'curl' => 'text/vnd.curl',
        'cww' => 'application/prs.cww',
        'cxt' => 'application/x-director',
        'cxx' => 'text/x-c',
        'dae' => 'model/vnd.collada+xml',
        'daf' => 'application/vnd.mobius.daf',
        'dart' => 'application/vnd.dart',
        'dataless' => 'application/vnd.fdsn.seed',
        'davmount' => 'application/davmount+xml',
        'dbk' => 'application/docbook+xml',
        'dcr' => 'application/x-director',
        'dcurl' => 'text/vnd.curl.dcurl',
        'dd2' => 'application/vnd.oma.dd2+xml',
        'ddd' => 'application/vnd.fujixerox.ddd',
        'deb' => 'application/x-debian-package',
        'def' => 'text/plain',
        'deploy' => 'application/octet-stream',
        'der' => 'application/x-x509-ca-cert',
        'dfac' => 'application/vnd.dreamfactory',
        'dgc' => 'application/x-dgc-compressed',
        'dib' => 'image/bmp',
        'dic' => 'text/x-c',
        'dir' => 'application/x-director',
        'dis' => 'application/vnd.mobius.dis',
        'dist' => 'application/octet-stream',
        'distz' => 'application/octet-stream',
        'djv' => 'image/vnd.djvu',
        'djvu' => 'image/vnd.djvu',
        'dll' => 'application/x-msdownload',
        'dmg' => 'application/x-apple-diskimage',
        'dmp' => 'application/vnd.tcpdump.pcap',
        'dms' => 'application/octet-stream',
        'dna' => 'application/vnd.dna',
        'doc' => 'application/msword',
        'docm' => 'application/vnd.ms-word.document.macroenabled.12',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'dot' => 'application/msword',
        'dotm' => 'application/vnd.ms-word.template.macroenabled.12',
        'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        'dp' => 'application/vnd.osgi.dp',
        'dpg' => 'application/vnd.dpgraph',
        'dra' => 'audio/vnd.dra',
        'dsc' => 'text/prs.lines.tag',
        'dssc' => 'application/dssc+der',
        'dtb' => 'application/x-dtbook+xml',
        'dtd' => 'application/xml-dtd',
        'dts' => 'audio/vnd.dts',
        'dtshd' => 'audio/vnd.dts.hd',
        'dump' => 'application/octet-stream',
        'dv' => 'video/x-dv',
        'dvb' => 'video/vnd.dvb.file',
        'dvi' => 'application/x-dvi',
        'dwf' => 'model/vnd.dwf',
        'dwg' => 'image/vnd.dwg',
        'dxf' => 'image/vnd.dxf',
        'dxp' => 'application/vnd.spotfire.dxp',
        'dxr' => 'application/x-director',
        'ecelp4800' => 'audio/vnd.nuera.ecelp4800',
        'ecelp7470' => 'audio/vnd.nuera.ecelp7470',
        'ecelp9600' => 'audio/vnd.nuera.ecelp9600',
        'ecma' => 'application/ecmascript',
        'edm' => 'application/vnd.novadigm.edm',
        'edx' => 'application/vnd.novadigm.edx',
        'efif' => 'application/vnd.picsel',
        'ei6' => 'application/vnd.pg.osasli',
        'elc' => 'application/octet-stream',
        'emf' => 'application/x-msmetafile',
        'eml' => 'message/rfc822',
        'emma' => 'application/emma+xml',
        'emz' => 'application/x-msmetafile',
        'eol' => 'audio/vnd.digital-winds',
        'eot' => 'application/vnd.ms-fontobject',
        'eps' => 'application/postscript',
        'epub' => 'application/epub+zip',
        'es3' => 'application/vnd.eszigno3+xml',
        'esa' => 'application/vnd.osgi.subsystem',
        'esf' => 'application/vnd.epson.esf',
        'et3' => 'application/vnd.eszigno3+xml',
        'etx' => 'text/x-setext',
        'eva' => 'application/x-eva',
        'evy' => 'application/x-envoy',
        'exe' => 'application/octet-stream',
        'exi' => 'application/exi',
        'ext' => 'application/vnd.novadigm.ext',
        'ez' => 'application/andrew-inset',
        'ez2' => 'application/vnd.ezpix-album',
        'ez3' => 'application/vnd.ezpix-package',
        'f' => 'text/x-fortran',
        'f4v' => 'video/x-f4v',
        'f77' => 'text/x-fortran',
        'f90' => 'text/x-fortran',
        'fbs' => 'image/vnd.fastbidsheet',
        'fcdt' => 'application/vnd.adobe.formscentral.fcdt',
        'fcs' => 'application/vnd.isac.fcs',
        'fdf' => 'application/vnd.fdf',
        'fe_launch' => 'application/vnd.denovo.fcselayout-link',
        'fg5' => 'application/vnd.fujitsu.oasysgp',
        'fgd' => 'application/x-director',
        'fh' => 'image/x-freehand',
        'fh4' => 'image/x-freehand',
        'fh5' => 'image/x-freehand',
        'fh7' => 'image/x-freehand',
        'fhc' => 'image/x-freehand',
        'fig' => 'application/x-xfig',
        'flac' => 'audio/flac',
        'fli' => 'video/x-fli',
        'flo' => 'application/vnd.micrografx.flo',
        'flv' => 'video/x-flv',
        'flw' => 'application/vnd.kde.kivio',
        'flx' => 'text/vnd.fmi.flexstor',
        'fly' => 'text/vnd.fly',
        'fm' => 'application/vnd.framemaker',
        'fnc' => 'application/vnd.frogans.fnc',
        'for' => 'text/x-fortran',
        'fpx' => 'image/vnd.fpx',
        'frame' => 'application/vnd.framemaker',
        'fsc' => 'application/vnd.fsc.weblaunch',
        'fst' => 'image/vnd.fst',
        'ftc' => 'application/vnd.fluxtime.clip',
        'fti' => 'application/vnd.anser-web-funds-transfer-initiation',
        'fvt' => 'video/vnd.fvt',
        'fxp' => 'application/vnd.adobe.fxp',
        'fxpl' => 'application/vnd.adobe.fxp',
        'fzs' => 'application/vnd.fuzzysheet',
        'g2w' => 'application/vnd.geoplan',
        'g3' => 'image/g3fax',
        'g3w' => 'application/vnd.geospace',
        'gac' => 'application/vnd.groove-account',
        'gam' => 'application/x-tads',
        'gbr' => 'application/rpki-ghostbusters',
        'gca' => 'application/x-gca-compressed',
        'gdl' => 'model/vnd.gdl',
        'geo' => 'application/vnd.dynageo',
        'gex' => 'application/vnd.geometry-explorer',
        'ggb' => 'application/vnd.geogebra.file',
        'ggt' => 'application/vnd.geogebra.tool',
        'ghf' => 'application/vnd.groove-help',
        'gif' => 'image/gif',
        'gim' => 'application/vnd.groove-identity-message',
        'gml' => 'application/gml+xml',
        'gmx' => 'application/vnd.gmx',
        'gnumeric' => 'application/x-gnumeric',
        'gph' => 'application/vnd.flographit',
        'gpx' => 'application/gpx+xml',
        'gqf' => 'application/vnd.grafeq',
        'gqs' => 'application/vnd.grafeq',
        'gram' => 'application/srgs',
        'gramps' => 'application/x-gramps-xml',
        'gre' => 'application/vnd.geometry-explorer',
        'grv' => 'application/vnd.groove-injector',
        'grxml' => 'application/srgs+xml',
        'gsf' => 'application/x-font-ghostscript',
        'gtar' => 'application/x-gtar',
        'gtm' => 'application/vnd.groove-tool-message',
        'gtw' => 'model/vnd.gtw',
        'gv' => 'text/vnd.graphviz',
        'gxf' => 'application/gxf',
        'gxt' => 'application/vnd.geonext',
        'gz' => 'application/gzip',
        'h' => 'text/x-c',
        'h261' => 'video/h261',
        'h263' => 'video/h263',
        'h264' => 'video/h264',
        'hal' => 'application/vnd.hal+xml',
        'hbci' => 'application/vnd.hbci',
        'hdf' => 'application/x-hdf',
        'hh' => 'text/x-c',
        'hlp' => 'application/winhlp',
        'hpgl' => 'application/vnd.hp-hpgl',
        'hpid' => 'application/vnd.hp-hpid',
        'hps' => 'application/vnd.hp-hps',
        'hqx' => 'application/mac-binhex40',
        'htc' => 'text/x-component',
        'htke' => 'application/vnd.kenameaapp',
        'htm' => 'text/html',
        'html' => 'text/html',
        'hvd' => 'application/vnd.yamaha.hv-dic',
        'hvp' => 'application/vnd.yamaha.hv-voice',
        'hvs' => 'application/vnd.yamaha.hv-script',
        'i2g' => 'application/vnd.intergeo',
        'icc' => 'application/vnd.iccprofile',
        'ice' => 'x-conference/x-cooltalk',
        'icm' => 'application/vnd.iccprofile',
        'ico' => 'image/x-icon',
        'ics' => 'text/calendar',
        'ief' => 'image/ief',
        'ifb' => 'text/calendar',
        'ifm' => 'application/vnd.shana.informed.formdata',
        'iges' => 'model/iges',
        'igl' => 'application/vnd.igloader',
        'igm' => 'application/vnd.insors.igm',
        'igs' => 'model/iges',
        'igx' => 'application/vnd.micrografx.igx',
        'iif' => 'application/vnd.shana.informed.interchange',
        'imp' => 'application/vnd.accpac.simply.imp',
        'ims' => 'application/vnd.ms-ims',
        'in' => 'text/plain',
        'ini' => 'text/plain',
        'ink' => 'application/inkml+xml',
        'inkml' => 'application/inkml+xml',
        'install' => 'application/x-install-instructions',
        'iota' => 'application/vnd.astraea-software.iota',
        'ipfix' => 'application/ipfix',
        'ipk' => 'application/vnd.shana.informed.package',
        'irm' => 'application/vnd.ibm.rights-management',
        'irp' => 'application/vnd.irepository.package+xml',
        'iso' => 'application/x-iso9660-image',
        'itp' => 'application/vnd.shana.informed.formtemplate',
        'ivp' => 'application/vnd.immervision-ivp',
        'ivu' => 'application/vnd.immervision-ivu',
        'jad' => 'text/vnd.sun.j2me.app-descriptor',
        'jam' => 'application/vnd.jam',
        'jar' => 'application/java-archive',
        'java' => 'text/x-java-source',
        'jisp' => 'application/vnd.jisp',
        'jlt' => 'application/vnd.hp-jlyt',
        'jnlp' => 'application/x-java-jnlp-file',
        'joda' => 'application/vnd.joost.joda-archive',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'jpgm' => 'video/jpm',
        'jpgv' => 'video/jpeg',
        'jpm' => 'video/jpm',
        'js' => 'application/javascript',
        'jsf' => 'text/plain',
        'json' => 'application/json',
        'jsonml' => 'application/jsonml+json',
        'jspf' => 'text/plain',
        'kar' => 'audio/midi',
        'karbon' => 'application/vnd.kde.karbon',
        'kfo' => 'application/vnd.kde.kformula',
        'kia' => 'application/vnd.kidspiration',
        'kml' => 'application/vnd.google-earth.kml+xml',
        'kmz' => 'application/vnd.google-earth.kmz',
        'kne' => 'application/vnd.kinar',
        'knp' => 'application/vnd.kinar',
        'kon' => 'application/vnd.kde.kontour',
        'kpr' => 'application/vnd.kde.kpresenter',
        'kpt' => 'application/vnd.kde.kpresenter',
        'kpxx' => 'application/vnd.ds-keypoint',
        'ksp' => 'application/vnd.kde.kspread',
        'ktr' => 'application/vnd.kahootz',
        'ktx' => 'image/ktx',
        'ktz' => 'application/vnd.kahootz',
        'kwd' => 'application/vnd.kde.kword',
        'kwt' => 'application/vnd.kde.kword',
        'lasxml' => 'application/vnd.las.las+xml',
        'latex' => 'application/x-latex',
        'lbd' => 'application/vnd.llamagraphics.life-balance.desktop',
        'lbe' => 'application/vnd.llamagraphics.life-balance.exchange+xml',
        'les' => 'application/vnd.hhe.lesson-player',
        'lha' => 'application/x-lzh-compressed',
        'link66' => 'application/vnd.route66.link66+xml',
        'list' => 'text/plain',
        'list3820' => 'application/vnd.ibm.modcap',
        'listafp' => 'application/vnd.ibm.modcap',
        'lnk' => 'application/x-ms-shortcut',
        'log' => 'text/plain',
        'lostxml' => 'application/lost+xml',
        'lrf' => 'application/octet-stream',
        'lrm' => 'application/vnd.ms-lrm',
        'ltf' => 'application/vnd.frogans.ltf',
        'lvp' => 'audio/vnd.lucent.voice',
        'lwp' => 'application/vnd.lotus-wordpro',
        'lzh' => 'application/x-lzh-compressed',
        'm13' => 'application/x-msmediaview',
        'm14' => 'application/x-msmediaview',
        'm1v' => 'video/mpeg',
        'm21' => 'application/mp21',
        'm2a' => 'audio/mpeg',
        'm2v' => 'video/mpeg',
        'm3a' => 'audio/mpeg',
        'm3u' => 'audio/x-mpegurl',
        'm3u8' => 'application/vnd.apple.mpegurl',
        'm4a' => 'audio/mp4',
        'm4b' => 'audio/mp4',
        'm4r' => 'audio/mp4',
        'm4u' => 'video/vnd.mpegurl',
        'm4v' => 'video/mp4',
        'ma' => 'application/mathematica',
        'mac' => 'image/x-macpaint',
        'mads' => 'application/mads+xml',
        'mag' => 'application/vnd.ecowin.chart',
        'maker' => 'application/vnd.framemaker',
        'man' => 'text/troff',
        'mar' => 'application/octet-stream',
        'mathml' => 'application/mathml+xml',
        'mb' => 'application/mathematica',
        'mbk' => 'application/vnd.mobius.mbk',
        'mbox' => 'application/mbox',
        'mc1' => 'application/vnd.medcalcdata',
        'mcd' => 'application/vnd.mcd',
        'mcurl' => 'text/vnd.curl.mcurl',
        'mdb' => 'application/x-msaccess',
        'mdi' => 'image/vnd.ms-modi',
        'me' => 'text/troff',
        'mesh' => 'model/mesh',
        'meta4' => 'application/metalink4+xml',
        'metalink' => 'application/metalink+xml',
        'mets' => 'application/mets+xml',
        'mfm' => 'application/vnd.mfmp',
        'mft' => 'application/rpki-manifest',
        'mgp' => 'application/vnd.osgeo.mapguide.package',
        'mgz' => 'application/vnd.proteus.magazine',
        'mid' => 'audio/midi',
        'midi' => 'audio/midi',
        'mie' => 'application/x-mie',
        'mif' => 'application/x-mif',
        'mime' => 'message/rfc822',
        'mj2' => 'video/mj2',
        'mjp2' => 'video/mj2',
        'mk3d' => 'video/x-matroska',
        'mka' => 'audio/x-matroska',
        'mks' => 'video/x-matroska',
        'mkv' => 'video/x-matroska',
        'mlp' => 'application/vnd.dolby.mlp',
        'mmd' => 'application/vnd.chipnuts.karaoke-mmd',
        'mmf' => 'application/vnd.smaf',
        'mmr' => 'image/vnd.fujixerox.edmics-mmr',
        'mng' => 'video/x-mng',
        'mny' => 'application/x-msmoney',
        'mobi' => 'application/x-mobipocket-ebook',
        'mods' => 'application/mods+xml',
        'mov' => 'video/quicktime',
        'movie' => 'video/x-sgi-movie',
        'mp1' => 'audio/mpeg',
        'mp2' => 'audio/mpeg',
        'mp21' => 'application/mp21',
        'mp2a' => 'audio/mpeg',
        'mp3' => 'audio/mpeg',
        'mp4' => 'video/mp4',
        'mp4a' => 'audio/mp4',
        'mp4s' => 'application/mp4',
        'mp4v' => 'video/mp4',
        'mpa' => 'audio/mpeg',
        'mpc' => 'application/vnd.mophun.certificate',
        'mpe' => 'video/mpeg',
        'mpeg' => 'video/mpeg',
        'mpega' => 'audio/x-mpeg',
        'mpg' => 'video/mpeg',
        'mpg4' => 'video/mp4',
        'mpga' => 'audio/mpeg',
        'mpkg' => 'application/vnd.apple.installer+xml',
        'mpm' => 'application/vnd.blueice.multipass',
        'mpn' => 'application/vnd.mophun.application',
        'mpp' => 'application/vnd.ms-project',
        'mpt' => 'application/vnd.ms-project',
        'mpv2' => 'video/mpeg2',
        'mpy' => 'application/vnd.ibm.minipay',
        'mqy' => 'application/vnd.mobius.mqy',
        'mrc' => 'application/marc',
        'mrcx' => 'application/marcxml+xml',
        'ms' => 'text/troff',
        'mscml' => 'application/mediaservercontrol+xml',
        'mseed' => 'application/vnd.fdsn.mseed',
        'mseq' => 'application/vnd.mseq',
        'msf' => 'application/vnd.epson.msf',
        'msh' => 'model/mesh',
        'msi' => 'application/x-msdownload',
        'msl' => 'application/vnd.mobius.msl',
        'msty' => 'application/vnd.muvee.style',
        'mts' => 'model/vnd.mts',
        'mus' => 'application/vnd.musician',
        'musicxml' => 'application/vnd.recordare.musicxml+xml',
        'mvb' => 'application/x-msmediaview',
        'mwf' => 'application/vnd.mfer',
        'mxf' => 'application/mxf',
        'mxl' => 'application/vnd.recordare.musicxml',
        'mxml' => 'application/xv+xml',
        'mxs' => 'application/vnd.triscape.mxs',
        'mxu' => 'video/vnd.mpegurl',
        'n-gage' => 'application/vnd.nokia.n-gage.symbian.install',
        'n3' => 'text/n3',
        'nb' => 'application/mathematica',
        'nbp' => 'application/vnd.wolfram.player',
        'nc' => 'application/x-netcdf',
        'ncx' => 'application/x-dtbncx+xml',
        'nfo' => 'text/x-nfo',
        'ngdat' => 'application/vnd.nokia.n-gage.data',
        'nitf' => 'application/vnd.nitf',
        'nlu' => 'application/vnd.neurolanguage.nlu',
        'nml' => 'application/vnd.enliven',
        'nnd' => 'application/vnd.noblenet-directory',
        'nns' => 'application/vnd.noblenet-sealer',
        'nnw' => 'application/vnd.noblenet-web',
        'npx' => 'image/vnd.net-fpx',
        'nsc' => 'application/x-conference',
        'nsf' => 'application/vnd.lotus-notes',
        'ntf' => 'application/vnd.nitf',
        'nzb' => 'application/x-nzb',
        'oa2' => 'application/vnd.fujitsu.oasys2',
        'oa3' => 'application/vnd.fujitsu.oasys3',
        'oas' => 'application/vnd.fujitsu.oasys',
        'obd' => 'application/x-msbinder',
        'obj' => 'application/x-tgif',
        'oda' => 'application/oda',
        'odb' => 'application/vnd.oasis.opendocument.database',
        'odc' => 'application/vnd.oasis.opendocument.chart',
        'odf' => 'application/vnd.oasis.opendocument.formula',
        'odft' => 'application/vnd.oasis.opendocument.formula-template',
        'odg' => 'application/vnd.oasis.opendocument.graphics',
        'odi' => 'application/vnd.oasis.opendocument.image',
        'odm' => 'application/vnd.oasis.opendocument.text-master',
        'odp' => 'application/vnd.oasis.opendocument.presentation',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        'odt' => 'application/vnd.oasis.opendocument.text',
        'oga' => 'audio/ogg',
        'ogg' => 'audio/ogg',
        'ogv' => 'video/ogg',
        'ogx' => 'application/ogg',
        'omdoc' => 'application/omdoc+xml',
        'onepkg' => 'application/onenote',
        'onetmp' => 'application/onenote',
        'onetoc' => 'application/onenote',
        'onetoc2' => 'application/onenote',
        'opf' => 'application/oebps-package+xml',
        'opml' => 'text/x-opml',
        'oprc' => 'application/vnd.palm',
        'org' => 'application/vnd.lotus-organizer',
        'osf' => 'application/vnd.yamaha.openscoreformat',
        'osfpvg' => 'application/vnd.yamaha.openscoreformat.osfpvg+xml',
        'otc' => 'application/vnd.oasis.opendocument.chart-template',
        'otf' => 'font/otf',
        'otg' => 'application/vnd.oasis.opendocument.graphics-template',
        'oth' => 'application/vnd.oasis.opendocument.text-web',
        'oti' => 'application/vnd.oasis.opendocument.image-template',
        'otp' => 'application/vnd.oasis.opendocument.presentation-template',
        'ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
        'ott' => 'application/vnd.oasis.opendocument.text-template',
        'oxps' => 'application/oxps',
        'oxt' => 'application/vnd.openofficeorg.extension',
        'p' => 'text/x-pascal',
        'p10' => 'application/pkcs10',
        'p12' => 'application/x-pkcs12',
        'p7b' => 'application/x-pkcs7-certificates',
        'p7c' => 'application/pkcs7-mime',
        'p7m' => 'application/pkcs7-mime',
        'p7r' => 'application/x-pkcs7-certreqresp',
        'p7s' => 'application/pkcs7-signature',
        'p8' => 'application/pkcs8',
        'pas' => 'text/x-pascal',
        'paw' => 'application/vnd.pawaafile',
        'pbd' => 'application/vnd.powerbuilder6',
        'pbm' => 'image/x-portable-bitmap',
        'pcap' => 'application/vnd.tcpdump.pcap',
        'pcf' => 'application/x-font-pcf',
        'pcl' => 'application/vnd.hp-pcl',
        'pclxl' => 'application/vnd.hp-pclxl',
        'pct' => 'image/pict',
        'pcurl' => 'application/vnd.curl.pcurl',
        'pcx' => 'image/x-pcx',
        'pdb' => 'application/vnd.palm',
        'pdf' => 'application/pdf',
        'pfa' => 'application/x-font-type1',
        'pfb' => 'application/x-font-type1',
        'pfm' => 'application/x-font-type1',
        'pfr' => 'application/font-tdpfr',
        'pfx' => 'application/x-pkcs12',
        'pgm' => 'image/x-portable-graymap',
        'pgn' => 'application/x-chess-pgn',
        'pgp' => 'application/pgp-encrypted',
        'pic' => 'image/pict',
        'pict' => 'image/pict',
        'pkg' => 'application/octet-stream',
        'pki' => 'application/pkixcmp',
        'pkipath' => 'application/pkix-pkipath',
        'plb' => 'application/vnd.3gpp.pic-bw-large',
        'plc' => 'application/vnd.mobius.plc',
        'plf' => 'application/vnd.pocketlearn',
        'pls' => 'audio/x-scpls',
        'pml' => 'application/vnd.ctc-posml',
        'png' => 'image/png',
        'pnm' => 'image/x-portable-anymap',
        'pnt' => 'image/x-macpaint',
        'portpkg' => 'application/vnd.macports.portpkg',
        'pot' => 'application/vnd.ms-powerpoint',
        'potm' => 'application/vnd.ms-powerpoint.template.macroenabled.12',
        'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
        'ppam' => 'application/vnd.ms-powerpoint.addin.macroenabled.12',
        'ppd' => 'application/vnd.cups-ppd',
        'ppm' => 'image/x-portable-pixmap',
        'pps' => 'application/vnd.ms-powerpoint',
        'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
        'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'ppt' => 'application/vnd.ms-powerpoint',
        'pptm' => 'application/vnd.ms-powerpoint.presentation.macroenabled.12',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'pqa' => 'application/vnd.palm',
        'prc' => 'application/x-mobipocket-ebook',
        'pre' => 'application/vnd.lotus-freelance',
        'prf' => 'application/pics-rules',
        'ps' => 'application/postscript',
        'psb' => 'application/vnd.3gpp.pic-bw-small',
        'psd' => 'image/vnd.adobe.photoshop',
        'psf' => 'application/x-font-linux-psf',
        'pskcxml' => 'application/pskc+xml',
        'ptid' => 'application/vnd.pvi.ptid1',
        'pub' => 'application/x-mspublisher',
        'pvb' => 'application/vnd.3gpp.pic-bw-var',
        'pwn' => 'application/vnd.3m.post-it-notes',
        'pya' => 'audio/vnd.ms-playready.media.pya',
        'pyv' => 'video/vnd.ms-playready.media.pyv',
        'qam' => 'application/vnd.epson.quickanime',
        'qbo' => 'application/vnd.intu.qbo',
        'qfx' => 'application/vnd.intu.qfx',
        'qps' => 'application/vnd.publishare-delta-tree',
        'qt' => 'video/quicktime',
        'qti' => 'image/x-quicktime',
        'qtif' => 'image/x-quicktime',
        'qwd' => 'application/vnd.quark.quarkxpress',
        'qwt' => 'application/vnd.quark.quarkxpress',
        'qxb' => 'application/vnd.quark.quarkxpress',
        'qxd' => 'application/vnd.quark.quarkxpress',
        'qxl' => 'application/vnd.quark.quarkxpress',
        'qxt' => 'application/vnd.quark.quarkxpress',
        'ra' => 'audio/x-pn-realaudio',
        'ram' => 'audio/x-pn-realaudio',
        'rar' => 'application/x-rar-compressed',
        'ras' => 'image/x-cmu-raster',
        'rcprofile' => 'application/vnd.ipunplugged.rcprofile',
        'rdf' => 'application/rdf+xml',
        'rdz' => 'application/vnd.data-vision.rdz',
        'rep' => 'application/vnd.businessobjects',
        'res' => 'application/x-dtbresource+xml',
        'rgb' => 'image/x-rgb',
        'rif' => 'application/reginfo+xml',
        'rip' => 'audio/vnd.rip',
        'ris' => 'application/x-research-info-systems',
        'rl' => 'application/resource-lists+xml',
        'rlc' => 'image/vnd.fujixerox.edmics-rlc',
        'rld' => 'application/resource-lists-diff+xml',
        'rm' => 'application/vnd.rn-realmedia',
        'rmi' => 'audio/midi',
        'rmp' => 'audio/x-pn-realaudio-plugin',
        'rms' => 'application/vnd.jcp.javame.midlet-rms',
        'rmvb' => 'application/vnd.rn-realmedia-vbr',
        'rnc' => 'application/relax-ng-compact-syntax',
        'roa' => 'application/rpki-roa',
        'roff' => 'text/troff',
        'rp9' => 'application/vnd.cloanto.rp9',
        'rpss' => 'application/vnd.nokia.radio-presets',
        'rpst' => 'application/vnd.nokia.radio-preset',
        'rq' => 'application/sparql-query',
        'rs' => 'application/rls-services+xml',
        'rsd' => 'application/rsd+xml',
        'rss' => 'application/rss+xml',
        'rtf' => 'application/rtf',
        'rtx' => 'text/richtext',
        's' => 'text/x-asm',
        's3m' => 'audio/s3m',
        'saf' => 'application/vnd.yamaha.smaf-audio',
        'sbml' => 'application/sbml+xml',
        'sc' => 'application/vnd.ibm.secure-container',
        'scd' => 'application/x-msschedule',
        'scm' => 'application/vnd.lotus-screencam',
        'scq' => 'application/scvp-cv-request',
        'scs' => 'application/scvp-cv-response',
        'scurl' => 'text/vnd.curl.scurl',
        'sda' => 'application/vnd.stardivision.draw',
        'sdc' => 'application/vnd.stardivision.calc',
        'sdd' => 'application/vnd.stardivision.impress',
        'sdkd' => 'application/vnd.solent.sdkm+xml',
        'sdkm' => 'application/vnd.solent.sdkm+xml',
        'sdp' => 'application/sdp',
        'sdw' => 'application/vnd.stardivision.writer',
        'see' => 'application/vnd.seemail',
        'seed' => 'application/vnd.fdsn.seed',
        'sema' => 'application/vnd.sema',
        'semd' => 'application/vnd.semd',
        'semf' => 'application/vnd.semf',
        'ser' => 'application/java-serialized-object',
        'setpay' => 'application/set-payment-initiation',
        'setreg' => 'application/set-registration-initiation',
        'sfd-hdstx' => 'application/vnd.hydrostatix.sof-data',
        'sfs' => 'application/vnd.spotfire.sfs',
        'sfv' => 'text/x-sfv',
        'sgi' => 'image/sgi',
        'sgl' => 'application/vnd.stardivision.writer-global',
        'sgm' => 'text/sgml',
        'sgml' => 'text/sgml',
        'sh' => 'application/x-sh',
        'shar' => 'application/x-shar',
        'shf' => 'application/shf+xml',
        'sid' => 'image/x-mrsid-image',
        'sig' => 'application/pgp-signature',
        'sil' => 'audio/silk',
        'silo' => 'model/mesh',
        'sis' => 'application/vnd.symbian.install',
        'sisx' => 'application/vnd.symbian.install',
        'sit' => 'application/x-stuffit',
        'sitx' => 'application/x-stuffitx',
        'skd' => 'application/vnd.koan',
        'skm' => 'application/vnd.koan',
        'skp' => 'application/vnd.koan',
        'skt' => 'application/vnd.koan',
        'sldm' => 'application/vnd.ms-powerpoint.slide.macroenabled.12',
        'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
        'slt' => 'application/vnd.epson.salt',
        'sm' => 'application/vnd.stepmania.stepchart',
        'smf' => 'application/vnd.stardivision.math',
        'smi' => 'application/smil+xml',
        'smil' => 'application/smil+xml',
        'smv' => 'video/x-smv',
        'smzip' => 'application/vnd.stepmania.package',
        'snd' => 'audio/basic',
        'snf' => 'application/x-font-snf',
        'so' => 'application/octet-stream',
        'spc' => 'application/x-pkcs7-certificates',
        'spf' => 'application/vnd.yamaha.smaf-phrase',
        'spl' => 'application/x-futuresplash',
        'spot' => 'text/vnd.in3d.spot',
        'spp' => 'application/scvp-vp-response',
        'spq' => 'application/scvp-vp-request',
        'spx' => 'audio/ogg',
        'sql' => 'application/x-sql',
        'src' => 'application/x-wais-source',
        'srt' => 'application/x-subrip',
        'sru' => 'application/sru+xml',
        'srx' => 'application/sparql-results+xml',
        'ssdl' => 'application/ssdl+xml',
        'sse' => 'application/vnd.kodak-descriptor',
        'ssf' => 'application/vnd.epson.ssf',
        'ssml' => 'application/ssml+xml',
        'st' => 'application/vnd.sailingtracker.track',
        'stc' => 'application/vnd.sun.xml.calc.template',
        'std' => 'application/vnd.sun.xml.draw.template',
        'stf' => 'application/vnd.wt.stf',
        'sti' => 'application/vnd.sun.xml.impress.template',
        'stk' => 'application/hyperstudio',
        'stl' => 'application/vnd.ms-pki.stl',
        'str' => 'application/vnd.pg.format',
        'stw' => 'application/vnd.sun.xml.writer.template',
        'sub' => 'text/vnd.dvb.subtitle',
        'sus' => 'application/vnd.sus-calendar',
        'susp' => 'application/vnd.sus-calendar',
        'sv4cpio' => 'application/x-sv4cpio',
        'sv4crc' => 'application/x-sv4crc',
        'svc' => 'application/vnd.dvb.service',
        'svd' => 'application/vnd.svd',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        'swa' => 'application/x-director',
        'swf' => 'application/x-shockwave-flash',
        'swi' => 'application/vnd.aristanetworks.swi',
        'sxc' => 'application/vnd.sun.xml.calc',
        'sxd' => 'application/vnd.sun.xml.draw',
        'sxg' => 'application/vnd.sun.xml.writer.global',
        'sxi' => 'application/vnd.sun.xml.impress',
        'sxm' => 'application/vnd.sun.xml.math',
        'sxw' => 'application/vnd.sun.xml.writer',
        't' => 'text/troff',
        't3' => 'application/x-t3vm-image',
        'taglet' => 'application/vnd.mynfc',
        'tao' => 'application/vnd.tao.intent-module-archive',
        'tar' => 'application/x-tar',
        'tcap' => 'application/vnd.3gpp2.tcap',
        'tcl' => 'application/x-tcl',
        'teacher' => 'application/vnd.smart.teacher',
        'tei' => 'application/tei+xml',
        'teicorpus' => 'application/tei+xml',
        'tex' => 'application/x-tex',
        'texi' => 'application/x-texinfo',
        'texinfo' => 'application/x-texinfo',
        'text' => 'text/plain',
        'tfi' => 'application/thraud+xml',
        'tfm' => 'application/x-tex-tfm',
        'tga' => 'image/x-tga',
        'thmx' => 'application/vnd.ms-officetheme',
        'tif' => 'image/tiff',
        'tiff' => 'image/tiff',
        'tmo' => 'application/vnd.tmobile-livetv',
        'torrent' => 'application/x-bittorrent',
        'tpl' => 'application/vnd.groove-tool-template',
        'tpt' => 'application/vnd.trid.tpt',
        'tr' => 'text/troff',
        'tra' => 'application/vnd.trueapp',
        'trm' => 'application/x-msterminal',
        'tsd' => 'application/timestamped-data',
        'tsv' => 'text/tab-separated-values',
        'ttc' => 'font/collection',
        'ttf' => 'application/x-font-ttf',
        'ttl' => 'text/turtle',
        'twd' => 'application/vnd.simtech-mindmapper',
        'twds' => 'application/vnd.simtech-mindmapper',
        'txd' => 'application/vnd.genomatix.tuxedo',
        'txf' => 'application/vnd.mobius.txf',
        'txt' => 'text/plain',
        'u32' => 'application/x-authorware-bin',
        'udeb' => 'application/x-debian-package',
        'ufd' => 'application/vnd.ufdl',
        'ufdl' => 'application/vnd.ufdl',
        'ulw' => 'audio/basic',
        'ulx' => 'application/x-glulx',
        'umj' => 'application/vnd.umajin',
        'unityweb' => 'application/vnd.unity',
        'uoml' => 'application/vnd.uoml+xml',
        'uri' => 'text/uri-list',
        'uris' => 'text/uri-list',
        'urls' => 'text/uri-list',
        'ustar' => 'application/x-ustar',
        'utz' => 'application/vnd.uiq.theme',
        'uu' => 'text/x-uuencode',
        'uva' => 'audio/vnd.dece.audio',
        'uvd' => 'application/vnd.dece.data',
        'uvf' => 'application/vnd.dece.data',
        'uvg' => 'image/vnd.dece.graphic',
        'uvh' => 'video/vnd.dece.hd',
        'uvi' => 'image/vnd.dece.graphic',
        'uvm' => 'video/vnd.dece.mobile',
        'uvp' => 'video/vnd.dece.pd',
        'uvs' => 'video/vnd.dece.sd',
        'uvt' => 'application/vnd.dece.ttml+xml',
        'uvu' => 'video/vnd.uvvu.mp4',
        'uvv' => 'video/vnd.dece.video',
        'uvva' => 'audio/vnd.dece.audio',
        'uvvd' => 'application/vnd.dece.data',
        'uvvf' => 'application/vnd.dece.data',
        'uvvg' => 'image/vnd.dece.graphic',
        'uvvh' => 'video/vnd.dece.hd',
        'uvvi' => 'image/vnd.dece.graphic',
        'uvvm' => 'video/vnd.dece.mobile',
        'uvvp' => 'video/vnd.dece.pd',
        'uvvs' => 'video/vnd.dece.sd',
        'uvvt' => 'application/vnd.dece.ttml+xml',
        'uvvu' => 'video/vnd.uvvu.mp4',
        'uvvv' => 'video/vnd.dece.video',
        'uvvx' => 'application/vnd.dece.unspecified',
        'uvvz' => 'application/vnd.dece.zip',
        'uvx' => 'application/vnd.dece.unspecified',
        'uvz' => 'application/vnd.dece.zip',
        'vcard' => 'text/vcard',
        'vcd' => 'application/x-cdlink',
        'vcf' => 'text/x-vcard',
        'vcg' => 'application/vnd.groove-vcard',
        'vcs' => 'text/x-vcalendar',
        'vcx' => 'application/vnd.vcx',
        'vis' => 'application/vnd.visionary',
        'viv' => 'video/vnd.vivo',
        'vob' => 'video/x-ms-vob',
        'vor' => 'application/vnd.stardivision.writer',
        'vox' => 'application/x-authorware-bin',
        'vrml' => 'model/vrml',
        'vsd' => 'application/vnd.visio',
        'vsf' => 'application/vnd.vsf',
        'vss' => 'application/vnd.visio',
        'vst' => 'application/vnd.visio',
        'vsw' => 'application/vnd.visio',
        'vtu' => 'model/vnd.vtu',
        'vxml' => 'application/voicexml+xml',
        'w3d' => 'application/x-director',
        'wad' => 'application/x-doom',
        'wav' => 'audio/x-wav',
        'wax' => 'audio/x-ms-wax',
        'wbmp' => 'image/vnd.wap.wbmp',
        'wbs' => 'application/vnd.criticaltools.wbs+xml',
        'wbxml' => 'application/vnd.wap.wbxml',
        'wcm' => 'application/vnd.ms-works',
        'wdb' => 'application/vnd.ms-works',
        'wdp' => 'image/vnd.ms-photo',
        'weba' => 'audio/webm',
        'webm' => 'video/webm',
        'webp' => 'image/webp',
        'wg' => 'application/vnd.pmi.widget',
        'wgt' => 'application/widget',
        'wks' => 'application/vnd.ms-works',
        'wm' => 'video/x-ms-wm',
        'wma' => 'audio/x-ms-wma',
        'wmd' => 'application/x-ms-wmd',
        'wmf' => 'application/x-msmetafile',
        'wml' => 'text/vnd.wap.wml',
        'wmlc' => 'application/vnd.wap.wmlc',
        'wmls' => 'text/vnd.wap.wmlscript',
        'wmlsc' => 'application/vnd.wap.wmlscriptc',
        'wmv' => 'video/x-ms-wmv',
        'wmx' => 'video/x-ms-wmx',
        'wmz' => 'application/x-msmetafile',
        'woff' => 'application/x-font-woff',
        'woff2' => 'font/woff2',
        'wpd' => 'application/vnd.wordperfect',
        'wpl' => 'application/vnd.ms-wpl',
        'wps' => 'application/vnd.ms-works',
        'wqd' => 'application/vnd.wqd',
        'wri' => 'application/x-mswrite',
        'wrl' => 'model/vrml',
        'wsdl' => 'application/wsdl+xml',
        'wspolicy' => 'application/wspolicy+xml',
        'wtb' => 'application/vnd.webturbo',
        'wvx' => 'video/x-ms-wvx',
        'x32' => 'application/x-authorware-bin',
        'x3d' => 'model/x3d+xml',
        'x3db' => 'model/x3d+binary',
        'x3dbz' => 'model/x3d+binary',
        'x3dv' => 'model/x3d+vrml',
        'x3dvz' => 'model/x3d+vrml',
        'x3dz' => 'model/x3d+xml',
        'xaml' => 'application/xaml+xml',
        'xap' => 'application/x-silverlight-app',
        'xar' => 'application/vnd.xara',
        'xbap' => 'application/x-ms-xbap',
        'xbd' => 'application/vnd.fujixerox.docuworks.binder',
        'xbm' => 'image/x-xbitmap',
        'xdf' => 'application/xcap-diff+xml',
        'xdm' => 'application/vnd.syncml.dm+xml',
        'xdp' => 'application/vnd.adobe.xdp+xml',
        'xdssc' => 'application/dssc+xml',
        'xdw' => 'application/vnd.fujixerox.docuworks',
        'xenc' => 'application/xenc+xml',
        'xer' => 'application/patch-ops-error+xml',
        'xfdf' => 'application/vnd.adobe.xfdf',
        'xfdl' => 'application/vnd.xfdl',
        'xht' => 'application/xhtml+xml',
        'xhtml' => 'application/xhtml+xml',
        'xhvml' => 'application/xv+xml',
        'xif' => 'image/vnd.xiff',
        'xla' => 'application/vnd.ms-excel',
        'xlam' => 'application/vnd.ms-excel.addin.macroenabled.12',
        'xlc' => 'application/vnd.ms-excel',
        'xlf' => 'application/x-xliff+xml',
        'xlm' => 'application/vnd.ms-excel',
        'xls' => 'application/vnd.ms-excel',
        'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroenabled.12',
        'xlsm' => 'application/vnd.ms-excel.sheet.macroenabled.12',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'xlt' => 'application/vnd.ms-excel',
        'xltm' => 'application/vnd.ms-excel.template.macroenabled.12',
        'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        'xlw' => 'application/vnd.ms-excel',
        'xm' => 'audio/xm',
        'xml' => 'application/xml',
        'xo' => 'application/vnd.olpc-sugar',
        'xop' => 'application/xop+xml',
        'xpi' => 'application/x-xpinstall',
        'xpl' => 'application/xproc+xml',
        'xpm' => 'image/x-xpixmap',
        'xpr' => 'application/vnd.is-xpr',
        'xps' => 'application/vnd.ms-xpsdocument',
        'xpw' => 'application/vnd.intercon.formnet',
        'xpx' => 'application/vnd.intercon.formnet',
        'xsl' => 'application/xml',
        'xslt' => 'application/xslt+xml',
        'xsm' => 'application/vnd.syncml+xml',
        'xspf' => 'application/xspf+xml',
        'xul' => 'application/vnd.mozilla.xul+xml',
        'xvm' => 'application/xv+xml',
        'xvml' => 'application/xv+xml',
        'xwd' => 'image/x-xwindowdump',
        'xyz' => 'chemical/x-xyz',
        'xz' => 'application/x-xz',
        'yaml' => 'text/yaml',
        'yang' => 'application/yang',
        'yin' => 'application/yin+xml',
        'yml' => 'text/yaml',
        'z' => 'application/x-compress',
        'Z' => 'application/x-compress',
        'z1' => 'application/x-zmachine',
        'z2' => 'application/x-zmachine',
        'z3' => 'application/x-zmachine',
        'z4' => 'application/x-zmachine',
        'z5' => 'application/x-zmachine',
        'z6' => 'application/x-zmachine',
        'z7' => 'application/x-zmachine',
        'z8' => 'application/x-zmachine',
        'zaz' => 'application/vnd.zzazz.deck+xml',
        'zip' => 'application/zip',
        'zir' => 'application/vnd.zul',
        'zirz' => 'application/vnd.zul',
        'zmm' => 'application/vnd.handheld-entertainment+xml'];

    /**
     * 当前contentType
     *
     * @var string
     */
    protected $contentType = 'text/html';

    /**
     * 响应类型
     *
     * @var string
     */
    protected $responseType = 'html';

    /**
     * 字符集
     *
     * @var string
     */
    protected $charset = 'utf-8';

    /**
     * @var string
     */
    protected $responseReason = '';

    /**
     * 是否允许请求缓存
     *
     * @var bool
     */
    protected $allowCache = true;

    /**
     * 输出参数
     *
     * @var array
     */
    protected $options = [];

    /**
     * 输出内容
     *
     * @var string
     */
    protected $content = null;

    /**
     * 原始数据
     *
     * @var mixed
     */
    protected $data;

    /**
     * Cookie对象
     *
     * @var mixed
     */
    protected $cookie;

    /**
     * Session对象
     *
     * @var mixed
     */
    protected $session;

    /**
     * Create a new env response message instance.
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return $this
     * @throws \mark\http\exception\BadMessageException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setResponse(Response $response): self {
        parent::__construct($response->getBody());
        // if ($this->body->isSeekable()) {$this->body->seek(0);}

        $this->initialize($response->getData(), $response->getCode(), $response->getStatus(), $response->getReason(), $response->getResponseType());
        $this->addHeaders($response->getHeaders(), true);

        return $this;
    }

    /**
     * Create a new HTTP message.
     *
     * @param string $content
     * @param int    $code
     * @param string $status
     * @param string $reason
     * @param string $type
     *
     * @throws \mark\http\exception\BadMessageException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function __construct($content = '', int $code = 200, string $status = '', string $reason = '', string $type = '') {
        parent::__construct($content);

        // $this->headers = new ResponseHeaderBag($headers);
        if (!isset($this->headers['cache-control'])) {
            // $this->set('Cache-Control', '');
            $this->addHeader('Cache-Control', 'public');
        }

        $this->initialize($content, $code, $status, $reason, $type);
    }

    /**
     * 初始化
     *
     * @access protected
     *
     * @param string $content
     * @param int    $code 状态码
     * @param string $status
     * @param string $reason
     * @param string $type
     *
     * @throws \mark\exception\InvalidArgumentException
     */
    protected function initialize($content = '', int $code = 200, string $status = '', string $reason = '', string $type = '') {
        // $this->content($content);
        $this->setData($content);
        $this->setResponseCode($code);

        if (!empty($status)) {
            $this->setResponseStatus((string)$status);
        } elseif (!empty(self::$phrases[$this->responseCode]['status'] ?? '')) {
            $this->setResponseStatus(self::$phrases[$this->responseCode]['status']);
        }

        if (!empty($reason)) {
            $this->responseReason = $reason;
        } elseif (!empty(self::$phrases[$this->responseCode]['reason'] ?? '')) {
            $this->responseReason = self::$phrases[$this->responseCode]['reason'];
        }

        $this->setHeader('Response-Reason', $this->responseReason);

        $this->contentType($type);
    }

    /**
     * 输出数据设置
     *
     * @access public
     *
     * @param mixed $data 输出数据
     *
     * @return $this
     */
    public function setData($data): self {
        $this->data = $data;
        $this->content($data);

        return $this;
    }

    /**
     * @param $data
     *
     * @return $this
     * @uses \mark\response\Responsive::setData()
     */
    public function data($data): self {
        return $this->setData($data);
    }

    /**
     * 获取原始数据
     *
     * @access public
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }

    /**
     * 创建Response对象
     *
     * @access public
     *
     * @param mixed  $data 输出数据
     * @param int    $code 状态码
     * @param string $status
     * @param string $reason
     * @param string $type 输出类型
     *
     * @return static
     */
    public static function create($data = '', int $code = 200, string $status = '', string $reason = '', string $type = ''): self {
        return new self($data, $code, $status, $reason, $type);
    }

    /**
     * @param string $data
     * @param int    $code
     * @param string $status
     * @param string $reason
     * @param string $type
     *
     * @return static
     */
    public static function invoke($data = '', int $code = 200, string $status = '', string $reason = '', string $type = ''): self {
        $class = false !== strpos($type, '\\') ? $type : '\\mark\\response\\' . ucfirst(strtolower($type));
        return Container::getInstance()->invokeClass($class, [$data, $code, $status, $reason]);
    }

    /**
     * 设置Session对象
     *
     * @access public
     *
     * @param mixed $session Session对象
     *
     * @return $this
     */
    public function setSession($session): self {
        $this->session = $session;

        return $this;
    }

    /**
     * Send the response through the SAPI or $stream.
     * Flushes all output buffers.
     *
     * @description 发送数据到客户端
     * @access      public
     *
     * @param resource $stream A writable stream to send the response through.
     *
     * @return bool success.
     * @throws \mark\exception\InvalidArgumentException
     */
    public function send($stream = null): bool {
        // 处理输出数据
        $data = $this->getContent();

        if (!headers_sent()) {
            if (!empty($this->headers)) {
                // 发送状态码
                http_response_code($this->responseCode);
                // 发送头部信息
                foreach ($this->headers as $name => $val) {
                    header($name . (!is_null($val) ? ':' . $val : ''));
                }
            }

            if ($this->cookie) {
                $this->cookie->save();
            }
        }

        $this->sendData($data);

        if (function_exists('fastcgi_finish_request')) {
            // 提高页面响应
            fastcgi_finish_request();
        }

        // return parent::send($stream);
        return true;
    }

    /**
     * 处理数据
     *
     * @access protected
     *
     * @param mixed $content 要处理的数据
     *
     * @return mixed
     */
    protected function output($content) {
        return $content;
    }

    /**
     * 输出数据
     *
     * @access protected
     *
     * @param string $data 要处理的数据
     *
     * @return void
     */
    protected function sendData(string $data): void {
        echo $data;
    }

    /**
     * 输出的参数
     *
     * @access public
     *
     * @param mixed $options 输出参数
     *
     * @return $this
     */
    public function options(array $options = []): self {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    /**
     * 获取响应数据集合
     *
     * @return array
     */
    public function getOrigin(): array {
        return array(
            'data' => $this->toArray() ?: $this->getData(),
            'code' => $this->getCode(),
            'status' => $this->getStatus(),
            'reason' => $this->getReason(),
            'type' => $this->getResponseType(),
            'headers' => $this->getHeaders(),
            'options' => $this->options);
    }

    /**
     * 是否允许请求缓存
     *
     * @access public
     *
     * @param bool $cache 允许请求缓存
     *
     * @return $this
     */
    public function allowCache(bool $cache): self {
        $this->allowCache = $cache;

        return $this;
    }

    /**
     * 是否允许请求缓存
     *
     * @access public
     * @return bool
     */
    public function isAllowCache(): bool {
        return $this->allowCache;
    }

    /**
     * 设置Cookie
     *
     * @access public
     *
     * @param string $name   cookie名称
     * @param string $value  cookie值
     * @param mixed  $option 可选参数
     *
     * @return $this
     */
    public function cookie(string $name, string $value, $option = null): self {
        $this->cookie->set($name, $value, $option);

        return $this;
    }

    /**
     * 设置页面输出内容
     *
     * @access public
     *
     * @param mixed $content 输出数据
     *
     * @return $this
     * @throws \mark\exception\InvalidArgumentException
     */
    public function content($content): self {
        if (is_array($content)) {
            $content = json_encode($content, JSON_UNESCAPED_UNICODE);
        } elseif (is_object($content)) {
            $content = json_encode($content, JSON_UNESCAPED_UNICODE);
        } elseif (null !== $content && !is_string($content) && !is_numeric($content) && !is_callable([$content, '__toString',])) {
            throw new \mark\exception\InvalidArgumentException(sprintf('variable type error： %s', gettype($content)));
        }

        $this->content = (string)$content;

        return $this;
    }

    /**
     * 通过回调函数修正响应输出，以便适配各种框架输出
     *
     * @param callable $callback
     *
     * @return mixed
     */
    public function setCallback(callable $callback) {
        if (!empty($callback) && is_callable($callback)) {
            return call_user_func($callback, $this);
        }

        return $callback($this);
    }

    /**
     * Set a custom last modified time stamp.
     *
     * @note This will be used for caching and pre-condition checks.
     *
     * @param int $last_modified A unix timestamp.
     *
     * @return $this
     */
    public function setLastModified(int $last_modified): self {
        $this->addHeader('Last-Modified', $last_modified);

        return $this;
    }

    /**
     * @param string $last_modified
     *
     * @return $this
     */
    public function lastModified(string $last_modified): self {
        if (is_numeric($last_modified)) {
            return $this->setLastModified($last_modified);
        }

        return $this->setLastModified(time() + 86400);
    }

    /**
     * Expires
     *
     * @access public
     *
     * @param string $time
     *
     * @return $this
     */
    public function expires(string $time): self {
        $this->addHeader('Expires', $time);

        return $this;
    }

    /**
     * ETag
     *
     * @access public
     *
     * @param string $eTag
     *
     * @return $this
     */
    public function eTag(string $eTag): self {
        $this->addHeader('ETag', $eTag);

        return $this;
    }

    /**
     * 页面缓存控制
     *
     * @access public
     *
     * @param string $cache 状态码
     *
     * @return $this
     */
    public function cacheControl(string $cache): self {
        $this->addHeader('Cache-control', $cache);

        return $this;
    }

    /**
     * 页面输出类型
     *
     * @access public
     *
     * @param string $contentType 输出类型
     * @param string $charset     输出编码
     *
     * @return $this
     */
    public function contentType(string $contentType, string $charset = 'utf-8'): self {
        if (empty($contentType)) {
            return $this;
        }

        $type = strtolower($contentType);

        $mimes = array('suffix' => array('mimetypes' => 'application/json', 'mime' => 'application', 'type' => 'json'));

        if (!empty(self::$mimetypes[$type] ?? '')) {
            $this->contentType = self::$mimetypes[$type];
            $this->responseType = $type;
            $this->setHeader('Response-Type', $type);
        } else {
            $flip = array_flip(self::$mimetypes);
            if (!empty($flip[$type] ?? '')) {
                $this->contentType = $flip[$type];
                $this->responseType = $flip[$type];
                $this->setHeader('Response-Type', $flip[$type]);
            }
        }

        $this->type = 1;
        $this->addHeader('Content-Type', $this->contentType);
        $this->addHeader('Charset', $charset);

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setResponseType($type = ''): self {
        $this->contentType($type);

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseType(): string {
        return strtolower($this->responseType);
    }

    /**
     * 获取输出数据
     *
     * @access public
     * @return string
     * @throws \mark\exception\InvalidArgumentException
     */
    public function getContent(): string {
        if (null == $this->content) {
            $content = $this->output($this->data);

            if (null !== $content && !is_string($content) && !is_numeric($content) && !is_callable([$content, '__toString',])) {
                throw new \mark\exception\InvalidArgumentException(sprintf('variable type error： %s', gettype($content)));
            }

            $this->content = (string)$content;
        }

        return $this->content;
    }

    /**
     * 是否为空
     *
     * @access public
     * @return bool
     */
    public function isEmpty(): bool {
        return empty($this->data);
    }

    /**
     * 转换当前数据集为数组
     *
     * @return array
     */
    public function toArray(): array {
        $content = $this->getData();

        if (is_array($content) || empty($content)) {
            return $content ?: array();
        }

        if (is_string($content)) {
            $decode = json_decode($content, true);
            if ($decode != false) {
                return $decode;
            }
        }

        if (is_object($content)) {
            $encode = json_encode($content, JSON_UNESCAPED_UNICODE);
            if ($encode != false) {
                $decode = json_decode($encode, true);
                if ($decode != false) {
                    return $decode;
                }
            }
        }

        $decode = json_decode($content, true);
        if ($decode != false) {
            return $decode;
        }

        return array();
    }

    /**
     * @return object
     */
    public function toObject(): object {
        $content = $this->getData();

        if (is_object($content) || empty($content)) {
            return $content ?: new \stdClass();
        }

        if (is_string($content)) {
            $decode = json_decode($content);
            if ($decode != false) {
                return $decode;
            }
        }

        if (is_array($content)) {
            $encode = json_encode($content, JSON_UNESCAPED_UNICODE);
            if ($encode != false) {
                $decode = json_decode($encode);
                if ($decode != false) {
                    return $decode;
                }
            }
        }

        $decode = json_decode($content);
        if ($decode != false) {
            return $decode;
        }

        return new \stdClass();
    }

    /**
     * Api编码
     *
     * @param \mark\response\Response|null $response
     *
     * @return \mark\response\Response
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\BadMessageException
     */
    public function toApi(Response $response = null) {
        if ($response == null) {
            $response = $this;
        }

        $data = array('data' => $response->getData(),
                      'code' => $response->getResponseCode(),
                      'status' => $response->getResponseStatus(),
                      'reason' => $response->getResponseReason(),
                      'type' => $response->getResponseType(),
                      'headers' => $response->getHeaders());

        return new self($data, 200, '', '', $response->getResponseType());
    }

    /**
     * @param \mark\response\Response|null $response
     *
     * @return $this
     * @uses \mark\response\Response::api_decode();
     */
    public function formApi(Response $response = null): self {
        return $this->api_decode($response);
    }

    /**
     * Api解码
     *
     * @param \mark\response\Response|null $response
     *
     * @return $this
     * @throws \mark\http\exception\BadMessageException
     * @throws \mark\exception\InvalidArgumentException
     */
    public function api_decode(Response $response = null): self {
        if (!($response instanceof Response)) {
            $response = $this;
        }

        if ($response->getResponseCode() != 200 || $response->isEmpty()) {
            return $response;
        }

        $origin = $response->toArray();
        $new = new self($origin['data'] ?? '', $origin['code'] ?? 404, $origin['status'] ?? '', $origin['reason'] ?? '', $origin['type'] ?? '');
        $new->addHeaders($response->getHeaders(), true);
        if (!empty($origin['headers'] ?? '') && is_array($origin['headers'])) {
            $new->addHeaders($origin['headers'], true);
        }

        return $new;
    }

    /**
     * 转换当前数据集为JSON字符串
     *
     * @access public
     *
     * @param integer $options json参数
     *
     * @return string
     * @uses   \json_encode()
     */
    public function toJson(int $options = JSON_UNESCAPED_UNICODE): string {
        $encode = json_encode($this->toArray(), $options);
        if ($encode != false && json_last_error() == JSON_ERROR_NONE) {
            return $encode;
        }

        if (is_string($this->data)) {
            return $this->data;
        }

        return '';
    }

    public function toJsonp() { }

    public function toXml() { }

    public function toHtml() { }

    public function toImage() { }

    public function toFile() { }

    /**
     * Retrieve the message serialized to a string.
     * Alias of \mark\http\Message::toString().
     *
     * @return string the single serialized HTTP message.
     */
    public function __toString() {
        return $this->toJson();
    }

    /**
     * 响应允许跨域
     *
     * @param string $origin
     * @param array  $domain
     * @param string $methods
     *
     * @return $this
     */
    public function cross($origin = '', $domain = array(), $methods = 'GET, POST'): self {
        if (empty($origin) || empty($domain)) {
            return $this;
        }

        $host = parse_url($origin, PHP_URL_HOST);
        if (empty($host) || $host === false || !in_array($host, $domain, true)) {
            return $this;
        }

        $scheme = parse_url($origin, PHP_URL_SCHEME);
        // $this->addHeader("Access-Control-Allow-Origin", "*");
        $this->addHeader('Access-Control-Allow-Origin', $scheme . '://' . $host);
        $this->addHeader('Access-Control-Allow-Credentials', 'true'); // 设置是否允许发送 cookies
        $this->addHeader('Access-Control-Expose-Headers', '*');
        $this->addHeader('Access-Control-Allow-Methods', $methods);
        // $this->addHeader("Access-Control-Allow-Headers", "*");
        $this->addHeader('Access-Control-Allow-Headers', 'X-Requested-With,X_Requested_With,Content-Type,token');

        return $this;
    }

    /**
     * 设置HTTP状态
     *
     * @access public
     *
     * @param int  $code 状态码
     * @param bool $strict
     *
     * @return $this
     */
    public function setCode(int $code, bool $strict = true): self {
        return $this->setResponseCode($code, $strict);
    }

    /**
     * 获取状态码
     *
     * @access public
     * @return int
     */
    public function getCode(): int {
        return $this->getResponseCode();
    }

    /**
     * @return int
     */
    public function getStatusCode(): int {
        return $this->getResponseCode();
    }

    /**
     * @param int    $code
     * @param string $reasonPhrase
     *
     * @return $this
     */
    public function withStatus($code, $reasonPhrase = ''): self {
        $this->setResponseCode($code);
        $this->setResponseStatus($reasonPhrase);

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->getResponseStatus();
    }

    /**
     * @param string $reason
     *
     * @return $this
     */
    public function setResponseReason(string $reason = ''): self {
        $this->responseReason = $reason;

        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string {
        return $this->getReasonPhrase();
    }

    /**
     * @return string
     */
    public function getResponseReason(): string {
        return $this->getReasonPhrase();
    }

    /**
     * Gets the response reason phrase associated with the status code.
     * Because a reason phrase is not a required element in a response
     * status line, the reason phrase value MAY be null. Implementations MAY
     * choose to return the default RFC 7231 recommended reason phrase (or those
     * listed in the IANA HTTP Status Code Registry) for the response's
     * status code.
     *
     * @link http://tools.ietf.org/html/rfc7231#section-6
     * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @return string Reason phrase; must return an empty string if none present.
     */
    public function getReasonPhrase() {
        return $this->responseReason;
    }

    /**========== trait MessageTrait ==========**/

    /** @var StreamInterface|null */
    private $stream;

    /**
     * @return string
     */
    public function getProtocolVersion(): string {
        return parent::getHttpVersion();
    }

    /**
     * @param string $version
     *
     * @return $this
     */
    public function withProtocolVersion($version): self {
        if (parent::getHttpVersion() == $version) {
            return $this;
        }

        $this->httpVersion = $version;
        $new = clone $this;
        $new->httpVersion = $version;
        return $new;
    }

    /**
     * @param array $headers
     *
     * @return $this
     * @throws \mark\exception\InvalidArgumentException
     */
    public function setHeaders(array $headers = array()) {
        // $this->headerNames = $this->headers = [];
        foreach ($headers as $name => $value) {
            if (is_int($name)) {
                // Numeric array keys are converted to int by PHP but having a header name '123' is not forbidden by the spec
                // and also allowed in withHeader(). So we need to cast it to string again for the following assertion to pass.
                $name = (string)$name;
            }
            $this->assertHeader($name);
            $value = $this->normalizeHeaderValue($value);
            $normalized = strtolower($name);
            if (isset($this->headerNames[$normalized])) {
                $name = $this->headerNames[$normalized];
                // $this->headers[$name] = array_merge($this->headers[$name], $value);
                // $this->setHeader($name, array_merge($this->headers[$name], $value));
                $this->setHeader($name, $value);
            } else {
                $this->headerNames[$normalized] = $name;
                $this->setHeader($name, $value);
            }
        }

        parent::setHeaders($headers);

        return $this;
    }

    /**
     * Retrieve a single header, optionally hydrated into a http\Header extending class.
     *
     * @param string $name The header's name.
     *
     * @return string[] mixed the header value if $into_class is NULL. or \mark\http\Header descendant.
     */
    public function getHeader($name) {
        $header = strtolower($name);

        if (!empty($header)) {
            if (!empty($this->headers[$header] ?? '')) {
                return $this->headers[$header] ?: null;
            }

            if (isset($this->headerNames[$header])) {
                $header = $this->headerNames[$header];
                return $this->headers[$header] ?: null;
            }
        }

        return null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasHeader($name) {
        return isset($this->headerNames[strtolower($name)]);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function getHeaderLine($name) {
        return implode(', ', $this->getHeader($name));
    }

    /**
     * @param string          $name
     * @param string|string[] $value
     *
     * @return \mark\response\Response
     * @throws \mark\exception\InvalidArgumentException
     */
    public function withHeader($name, $value) {
        $this->assertHeader($name);
        $value = $this->normalizeHeaderValue($value);
        $normalized = strtolower($name);

        $new = clone $this;
        if (isset($new->headerNames[$normalized])) {
            unset($new->headers[$new->headerNames[$normalized]]);
        }
        $new->headerNames[$normalized] = $name;
        $new->headers[$name] = $value;

        return $new;
    }

    /**
     * @param string          $name
     * @param string|string[] $value
     *
     * @return \mark\response\Response
     * @throws \mark\exception\InvalidArgumentException
     */
    public function withAddedHeader($name, $value) {
        $this->assertHeader($name);
        $value = $this->normalizeHeaderValue($value);
        $normalized = strtolower($name);

        $new = clone $this;
        if (isset($new->headerNames[$normalized])) {
            $header = $this->headerNames[$normalized];
            $new->headers[$header] = array_merge($this->headers[$header], $value);
        } else {
            $new->headerNames[$normalized] = $name;
            $new->headers[$name] = $value;
        }

        return $new;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function withoutHeader($name): self {
        $normalized = strtolower($name);

        if (!isset($this->headerNames[$normalized])) {
            return $this;
        }

        $header = $this->headerNames[$normalized];

        $new = clone $this;
        unset($new->headers[$header], $new->headerNames[$normalized]);

        return $new;
    }

    /**
     * @return \mark\http\Message\Body|\Psr\Http\Message\StreamInterface|null
     */
    public function getBody() {
        if ($this->stream) {
            return $this->stream;
        }
        // $this->stream = \GuzzleHttp\Psr7\Utils::streamFor($this->content);

        return parent::getBody();
    }

    /**
     * @param \Psr\Http\Message\StreamInterface $body
     *
     * @return $this|\mark\response\Response
     */
    public function withBody(StreamInterface $body) {
        if ($body === $this->stream) {
            return $this;
        }

        $new = clone $this;
        $new->stream = $body;
        return $new;
    }

    /**
     * @param $value
     *
     * @return string[]
     * @throws \mark\exception\InvalidArgumentException
     */
    private function normalizeHeaderValue($value) {
        if (!is_array($value)) {
            return $this->trimHeaderValues([$value]);
        }

        if (count($value) === 0) {
            throw new \mark\exception\InvalidArgumentException('Header value can not be an empty array.');
        }

        return $this->trimHeaderValues($value);
    }

    /**
     * Trims whitespace from the header values.
     * Spaces and tabs ought to be excluded by parsers when extracting the field value from a header field.
     * header-field = field-name ":" OWS field-value OWS
     * OWS          = *( SP / HTAB )
     *
     * @param string[] $values Header values
     *
     * @return string[] Trimmed header values
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function trimHeaderValues(array $values) {
        return array_map(function ($value) {
            if (!is_scalar($value) && null !== $value) {
                throw new \mark\exception\InvalidArgumentException(sprintf(
                    'Header value must be scalar or null but %s provided.',
                    is_object($value) ? get_class($value) : gettype($value)));
            }

            return trim((string)$value, " \t");
        }, array_values($values));
    }

    /**
     * @param $header
     *
     * @throws \mark\exception\InvalidArgumentException
     */
    private function assertHeader($header) {
        if (!is_string($header)) {
            throw new \mark\exception\InvalidArgumentException(sprintf(
                'Header name must be a string but %s provided.',
                is_object($header) ? get_class($header) : gettype($header)));
        }

        if ($header === '') {
            throw new \mark\exception\InvalidArgumentException('Header name can not be empty.');
        }
    }

    /**
     * @param $name
     * @param $arguments
     */
    public static function __callStatic($name, $arguments) {

    }
}