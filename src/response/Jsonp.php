<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2021 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace mark\response;

use mark\request\Request;
use think\Cookie;

/**
 * Jsonp Response
 */
class Jsonp extends Response {
    // 输出参数
    protected $options = [
        'var_jsonp_handler' => 'callback',
        'default_jsonp_handler' => 'jsonpReturn',
        'json_encode_param' => JSON_UNESCAPED_UNICODE,
    ];

    protected $contentType = 'application/javascript';

    protected $request;

    /**
     * Jsonp constructor.
     *
     * @param \think\Cookie         $cookie
     * @param \mark\request\Request $request
     * @param string                $data
     * @param int                   $code
     */
    public function __construct(Cookie $cookie, Request $request, $data = '', int $code = 200) {
        parent::__construct($data, $code, '', '', 'jsonp');
        $this->contentType($this->contentType);

        $this->cookie = $cookie;
        $this->request = $request;
    }

    /**
     * 处理数据
     *
     * @access protected
     *
     * @param mixed $content 要处理的数据
     *
     * @return string
     * @throws \Exception
     */
    protected function output($content): string {
        try {
            // 返回JSON数据格式到客户端 包含状态信息 [当url_common_param为false时是无法获取到$_GET的数据的，故使用Request来获取<xiaobo.sun@qq.com>]
            $varJsonpHandler = $this->request->param($this->options['var_jsonp_handler'], '');
            $handler = !empty($varJsonpHandler) ? $varJsonpHandler : $this->options['default_jsonp_handler'];

            $content = json_encode($content, $this->options['json_encode_param']);
            if (false === $content) {
                throw new \mark\exception\InvalidArgumentException(json_last_error_msg());
            }

            $content = $handler . '(' . $content . ');';

            return $content;
        } catch (\Exception $e) {
            if ($e->getPrevious()) {
                throw $e->getPrevious();
            }
            throw $e;
        }
    }
}