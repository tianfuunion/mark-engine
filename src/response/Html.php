<?php
declare (strict_types=1);

namespace mark\response;

use think\Cookie;

/**
 * Class Html
 *
 * @description  Html Response
 * @package      mark\response
 */
class Html extends Response {
    /**
     * 输出type
     *
     * @var string
     */
    protected $contentType = 'text/html';

    /**
     * Html constructor.
     *
     * @param \think\Cookie $cookie
     * @param string        $data
     * @param int           $code
     */
    public function __construct(Cookie $cookie, $data = '', int $code = 200) {
        parent::__construct($data, $code, '', '', 'html');

        $this->contentType($this->contentType);
        $this->cookie = $cookie;
    }
}