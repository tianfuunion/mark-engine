<?php
declare (strict_types=1);

namespace mark\response;

use mark\exception\FunctionNotFoundException;
use mark\Mark;
use think\facade\View;

/**
 * Class Responsive Response
 *
 * @author  Mark<mark@tianfuunion.cn>
 * @package mark\response
 */
class Responsive extends Response {
    /**
     * 自适应响应输出
     *
     * @since   http://tools.jb51.net/table/http_status_code
     * @version HTTP Status Code 2.0
     *
     * @param mixed  $data   响应数据
     * @param int    $code   响应码
     * @param string $status 响应状态
     * @param string $reason 响应描述
     * @param string $type   自定义响应类别
     *
     * @return static
     */
    public static function display($data, int $code = 200, string $status = '', string $reason = '', string $type = ''): self {
        return new self($data, $code, $status, $reason, $type);
    }

    /**
     * 消息提醒的消息提示框
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function info($reason = '消息提醒', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'info');

        return self::handle($reason, 'info', $location, $timeout, $handle);
    }

    /**
     * 操作成功的消息提示框
     *
     * @param string $reason   用示输出提示消息
     * @param string $location 设置跳转的新位置
     * @param int    $timeout  设置跳转的时间,单位：秒
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function success($reason = '操作成功', $location = '', $timeout = 1, $handle = ''): \think\response\View {
        View::assign('sign', true);                    //如果成功 $sign=true
        View::assign('type', 'success');

        return self::handle($reason, 'success', $location, $timeout, $handle);
    }

    /**
     * 普通警告的消息提示框
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function warning($reason = '普通警告', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'warning');

        return self::handle($reason, 'warning', $location, $timeout, $handle);
    }

    /**
     * 操作失败的消息提示框
     *
     * @param string $reason   用示输出提示消息
     * @param string $location 设置跳转的新位置
     * @param int    $timeout  设置跳转的时间,单位：秒
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function error($reason = '操作失败', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'error');

        return self::handle($reason, 'error', $location, $timeout, $handle);
    }

    /**
     * 等待消息提示框
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function waiting($reason = '等待处理', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'waiting');

        return self::handle($reason, 'waiting', $location, $timeout, $handle);
    }

    /**
     * 用于在控制器中进行位置重定向
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function redirect($reason = '立即跳转', $location = '', $timeout = 1, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'redirect');

        return self::handle($reason, 'redirect', $location, $timeout, $handle);
    }

    /**
     * 取消操作消息提示框
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function cancel($reason = '取消操作', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'cancel');

        return self::handle($reason, 'cancel', $location, $timeout, $handle);
    }

    /**
     * 加载中
     *
     * @param string $reason
     * @param string $location
     * @param int    $timeout
     * @param string $handle
     *
     * @return \think\response\View
     */
    public static function loading($reason = '加载中', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        View::assign('sign', false);            //如果失败 $sign=false
        View::assign('type', 'loading');

        return self::handle($reason, 'loading', $location, $timeout, $handle);
    }

    /**
     * API响应输出
     *
     * @param string $data
     * @param int    $code
     * @param string $status
     * @param string $reason
     * @param string $type
     *
     * @return static
     */
    public static function api($data = '', $code = 200, $status = '', $reason = '', $type = 'json'): self {
        $response = new self($data, $code, $status, $reason, $type);
        return $response->toApi();
    }

    /**
     * Api编码
     *
     * @param \mark\response\Response|null $response
     *
     * @return \mark\response\Responsive
     * @throws \mark\exception\InvalidArgumentException
     * @throws \mark\http\exception\BadMessageException
     */
    public function toApi(Response $response = null) {
        if ($response == null) {
            $response = $this;
        }

        $data = array('data' => $response->getData(),
                      'code' => $response->getResponseCode(),
                      'status' => $response->getResponseStatus(),
                      'reason' => $response->getResponseReason(),
                      'type' => $response->getResponseType(),
                      'headers' => $response->getHeaders());

        return new self($data, 200, '', '', $response->getResponseType());
    }

    /**
     * 温馨提示
     *
     * @param string $reason   用示输出提示消息
     * @param string $type     提示消息类型
     * @param string $location 设置跳转的新位置
     * @param int    $timeout  设置跳转的时间,单位：秒
     * @param string $handle
     *
     * @return \think\response\View
     */
    private static function handle($reason = '温馨提示', $type = '', $location = '', $timeout = 3, $handle = ''): \think\response\View {
        $location = rtrim($location, '/');

        View::assign('settitle', $reason);
        View::assign('reason', $reason);
        View::assign('type', $type);
        View::assign('timeout', $timeout);
        View::assign('location', $location);
        View::assign('handle', $handle);
        View::assign('response', array('reason' => $reason, 'type' => $type, 'location' => $location, 'timeout' => $timeout, 'handle' => $handle));

        $template = function_exists('config') ? config('app.exception_tpl') : '';
        if (!empty($template) && file_exists($template)) {
            return view($template);
        }

        View::assign('assets_path', (new Mark())->getAssetsPath());
        return view((new Mark())->getAssetsPath() . 'view/exception.htm');
    }

    /**
     * 转换为ThinkPHP6 响应输出
     *
     * @param \mark\response\Response|null $response
     *
     * @return \think\Response
     */
    public function toThinkResponse(\mark\response\Response $response = null): \think\Response {
        if ($response == null) {
            return self::thinkResponse($this);
        }

        return self::thinkResponse($response);
    }

    /**
     * ThinkPHP6 响应输出
     *
     * @param \mark\response\Response $response
     * @param string                  $accept Header Accetp
     *
     * @return \think\Response
     */
    public static function thinkResponse(\mark\response\Response $response, $accept = ''): \think\Response {
        $type = $response->getResponseType();

        if (is_pjax() || $type == 'pjax') {
            if (function_exists('jsonp')) {
                return jsonp($response->toArray() ?: $response->getData(), $response->getResponseCode(), $response->getHeaders());
            }
        }

        if (is_json($accept) || $type == 'json' || is_ajax() || $type == 'ajax') {
            if (function_exists('json')) {
                return json($response->toArray() ?: $response->getData(), $response->getResponseCode(), $response->getHeaders());
            }
        }

        if (is_post() || $type == 'post') {
            if (function_exists('response')) {
                return response($response->toArray() ?: $response->getData(), $response->getResponseCode(), $response->getHeaders(), $response->getResponseType());
            }
        }

        if (is_get() || $type == 'get') {
            if ($response->getResponseCode() == 301 || $response->getResponseCode() == 302) {
                if (function_exists('redirect')) {
                    return redirect($response->getData(), $response->getResponseCode());
                }
            }

            if (function_exists('view')) {
                $template = '';
                if (function_exists('config')) {
                    $template = config('app.exception_tpl', config('view.exception_tpl'));
                }

                if (empty($template) || !file_exists($template)) {
                    $template = (new Mark())->getAssetsPath() . 'view/exception.htm';
                }

                return view($template, array(
                    'response' => array('data' => $response->toArray(),
                                        'code' => $response->getResponseCode(),
                                        'status' => $response->getResponseStatus(),
                                        'reason' => $response->getResponseReason(),
                                        'type' => $response->getResponseType(),
                                        'headers' => $response->getHeaders(),
                                        'origin' => $response,
                                        'response' => $response)))
                    // ->code($response->getCode())
                    ->header($response->getHeaders());
            }
        }

        if (!function_exists('response')) {
            throw new FunctionNotFoundException('response is not function', 'response');
        }

        // @note 可能存在 response 函数不存在的情况
        return response($response->toArray() ?: $response->getData(), $response->getResponseCode(), $response->getHeaders(), $response->getResponseType());
    }

    /**
     * ThinkPHP6 响应输出回调
     *
     * @return \Closure
     */
    public static function closureResponse(): \Closure {
        return function (\mark\response\Response $response) {
            return self::thinkResponse($response);
        };
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments) {
        return parent::$name($arguments);
    }
}