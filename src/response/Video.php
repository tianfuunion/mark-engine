<?php
declare (strict_types=1);

namespace mark\response;

use mark\exception\FileException;
use think\Cookie;
use think\Request;

/**
 * Class Video
 *
 * @package mark\response
 */
class Video extends Response {
    protected $expire = 360;
    protected $mimeType;
    private $isContent = false;

    /**
     * Image constructor.
     *
     * @param Cookie $cookie
     * @param string $data
     * @param int    $code
     */
    private function __construct(Cookie $cookie, $data = '', int $code = 200) {
        parent::__construct($data, $code, '', '', 'image');

        $this->cookie = $cookie;
    }

    /**
     * @param     $data
     * @param int $code
     *
     * @return static
     */
    public static function display($data, int $code = 200): self {
        $cookie = new Cookie(new Request());

        return new self($cookie, $data, $code);
    }

    /**
     * 处理数据
     *
     * @access protected
     *
     * @param mixed $content 要处理的数据
     *
     * @return mixed
     */
    protected function output($content) {
        if (empty($content) || !is_file($content)) {
            throw new FileException('file not exists:' . $content);
        }

        ob_clean();
        // ob_end_clean();

        $this->headers['Pragma'] = 'public';
        $this->headers['Content-Type'] = $this->getMimeType($content);
        $this->headers['Cache-control'] = 'max-age=' . $this->expire;

        // $this->header['Content-Transfer-Encoding'] = 'binary';
        $this->headers['Expires'] = gmdate('D, d M Y H:i:s', time() + $this->expire) . ' GMT';
        $this->lastModified(gmdate('D, d M Y H:i:s', time()) . ' GMT');
        $this->contentType($this->getMimeType($content));

        $size = filesize($content);                                                // The size of the file

        // Check if it's a HTTP range request
        if (isset($_SERVER['HTTP_RANGE'])) {
            // Parse the range header to get the byte offset
            $ranges = array_map(
                'intval', // Parse the parts into integer
                explode(
                    '-', // The range separator
                    substr($_SERVER['HTTP_RANGE'], 6) // Skip the `bytes=` part of the header
                )
            );

            // If the last range param is empty, it means the EOF (End of File)
            if (!$ranges[1]) {
                $ranges[1] = $size - 1;
            }

            // Send the appropriate headers
            header('HTTP/1.1 206 Partial Content');
            header('Accept-Ranges: bytes');
            header('Content-Length: ' . ($ranges[1] - $ranges[0]));           // The size of the range

            // Send the ranges we offered
            header(
                sprintf(
                    'Content-Range: bytes %d-%d/%d', // The header format
                    $ranges[0],                      // The start range
                    $ranges[1],                      // The end range
                    $size // Total size of the file
                )
            );

            // It's time to output the file
            $f = fopen($content, 'rb');                                       // Open the file in binary mode
            $chunkSize = 8192;                                                // The size of each chunk to output

            // Seek to the requested start range
            fseek($f, $ranges[0]);

            // Start outputting the data
            while (true) {
                // Check if we have outputted all the data requested
                if (ftell($f) >= $ranges[1]) {
                    break;
                }

                // Output the data
                return fread($f, $chunkSize);
            }
        } else {
            // It's not a range request, output the file anyway
            header('Content-Length: ' . $size);

            // Read the file
            return @readfile($content);
        }

        return parent::output($content);
    }

    /**
     * 设置是否为内容 必须配合mimeType方法使用
     *
     * @param bool $content
     *
     * @return $this
     */
    public function isContent(bool $content = true): self {
        $this->isContent = $content;

        return $this;
    }

    /**
     * 设置有效期
     *
     * @param int $expire 有效期
     *
     * @return $this
     */
    public function expire(int $expire): self {
        $this->expire = $expire;

        return $this;
    }

    /**
     * 设置文件类型
     *
     * @param string $mimeType
     *
     * @return $this
     */
    public function setMimeType(string $mimeType): self {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * 获取文件类型信息
     *
     * @access public
     *
     * @param string $filename 文件名
     * @param string $default
     *
     * @return string
     */
    protected function getMimeType(string $filename, $default = 'video/mp4'): string {
        if (!empty($this->mimeType)) {
            return $this->mimeType;
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        return finfo_file($finfo, $filename) ?: $default;
    }

    /**
     * Flush the buffer immediately
     *
     * @return void
     * @link https://php.net/manual/en/language.oop5.decon.php
     */
    public function __destruct() {
        @ob_flush();
        flush();
    }

}