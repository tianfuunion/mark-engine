<?php
declare (strict_types=1);

namespace mark\cache;

/**
 * Class CacheHandle
 *
 * @author      Mark<mark@tianfuunion.cn>
 * @time        2021年04月01日 15:07:00
 * @package     mark\auth\tool
 */
final class CacheHandle implements \Psr\SimpleCache\CacheInterface {

    /**
     * 缓存驱动句柄
     *
     * @var \Psr\SimpleCache\CacheInterface
     */
    private $cache_handle;

    /**
     * 缓存读取次数
     *
     * @var int
     */
    protected $readTimes = 0;

    /**
     * 缓存写入次数
     *
     * @var int
     */
    protected $writeTimes = 0;

    /**
     * 缓存配置参数
     *
     * @var array
     */
    protected $options = [
        'expire' => 0,
        'prefix' => '',
        'serialize' => [],
    ];

    /**
     * 缓存标签
     *
     * @var array
     */
    protected $tag = [];

    /**
     * CacheHandle constructor.
     * PHP 5 allows developers to declare constructor methods for classes.
     * Classes which have a constructor method call this method on each newly-created object,
     * so it is suitable for any initialization that the object may need before it is used.
     * Note: Parent constructors are not called implicitly if the child class defines a constructor.
     * In order to run a parent constructor, a call to parent::__construct() within the child constructor is required.
     * param [ mixed $args [, $... ]]
     *
     * @link https://php.net/manual/en/language.oop5.decon.php
     *
     * @param \Psr\SimpleCache\CacheInterface|null $cache_handle
     * @param array                                $options
     */
    public function __construct(\Psr\SimpleCache\CacheInterface $cache_handle = null, array $options = []) {
        if (!empty($cache_handle) && $cache_handle instanceof \Psr\SimpleCache\CacheInterface) {
            $this->cache_handle = $cache_handle;
        }

        if (!empty($options)) {
            $this->options = array_merge($this->options, $options);
        }
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     * @param mixed  $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if the $key string is not a legal value.
     */
    public function get($key, $default = null): mixed {
        if (empty($this->cache_handle)) {
            return $default;
        }

        $key = $this->getCacheKey((string)$key);

        $this->readTimes++;
        try {
            $value = $this->cache_handle->get($key, $default);

            if (false === $value || is_null($value)) {
                return $default;
            }

            return $this->unserialize($value);
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            return $default;
        }
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string                 $key   The key of the item to store.
     * @param mixed                  $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item.
     *                                      If no value is sent and the driver supports TTL then the library may set a default value for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if the $key string is not a legal value.
     */
    public function set($key, $value, $ttl = null): bool {
        if (empty($this->cache_handle)) {
            return false;
        }

        if (is_null($ttl)) {
            $ttl = $this->options['expire'];
        }

        $key = $this->getCacheKey((string)$key);
        $ttl = $this->getExpireTime($ttl);
        $value = $this->serialize($value);
        $this->writeTimes++;

        try {
            return $this->cache_handle->set($key, $value, $ttl);
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * 自增缓存（针对数值缓存）
     *
     * @access public
     *
     * @param string $key
     * @param int    $step 步长
     *
     * @return false|int
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function inc(string $key, int $step = 1) {
        $value = $this->get($key);

        $this->writeTimes++;
        if (empty($value)) {
            return $this->set($key, $step);
        }

        return $this->set($key, $value + $step);
    }

    /**
     * 自减缓存（针对数值缓存）
     *
     * @access public
     *
     * @param string $key
     * @param int    $step 步长
     *
     * @return false|int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function dec(string $key, int $step = 1) {
        $value = $this->get($key);

        $this->writeTimes++;
        if (empty($value)) {
            return $this->set($key, $step);
        }

        return $this->set($key, $value - $step);
    }

    /**
     * 追加（数组）缓存
     *
     * @access public
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function append(string $key, $value): bool {
        return $this->push($key, $value);
    }

    /**
     * 追加（数组）缓存
     *
     * @access public
     *
     * @param string $key
     * @param        $value
     * @param null   $ttl
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     * throws \mark\exception\InvalidArgumentException
     */
    public function push(string $key, $value, $ttl = null): bool {
        $key = $this->getCacheKey((string)$key);

        $item = $this->get($key, []);
        if (!is_array($item)) {
            // throw new \mark\exception\InvalidArgumentException('only array cache can be push');
            return false;
        }

        $item[] = $value;
        if (count($item) > 1000) {
            array_shift($item);
        }
        $item = array_unique($item, SORT_REGULAR);

        return $this->set($key, $item, $ttl);
    }

    /**
     * 把数组中的最后一个元素删除
     *
     * @param string $key
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function pop(string $key): bool {
        $before = $this->get($key);

        if (empty($before)) {
            return false;
        }

        array_pop($before);
        return $this->set($key, $before);
    }

    /**
     * 读取缓存并删除
     *
     * @access public
     *
     * @param string $key
     *
     * @return mixed
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function pull(string $key) {
        $result = $this->get($key, false);
        if ($result) {
            $this->delete($key);
        }

        return $result;
    }

    /**
     * 移除指定元素
     *
     * @param $key
     * @param $value
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function remove($key, $value): bool {
        $before = $this->get($key);
        switch (strtolower(gettype($before))) {
            case 'string':
            case 'boolean':
            case 'integer':
            case 'double':
            case 'float':

            case 'resource':
            case 'null':
            case 'unknown type':
                // 基本类型：则直接删除
                return $this->delete($key);
            // not break;
            case 'array':
            case 'object':
                // 复合类型：则删除指定值
                return $this->splice($key, $value);
            // not break;
            default:
                break;
        }

        return false;
    }

    /**
     * 移除指定元素
     *
     * @param $key
     * @param $value
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function splice($key, $value): bool {
        $before = $this->get($key);
        if (empty($before)) {
            return false;
        }

        $offset = array_search($value, $before);
        if (!is_numeric($offset)) {
            return false;
        }

        $splice = array_splice($before, (int)$offset, 1);
        if (empty($splice)) {
            return $this->set($key, $before);
        }

        unset($before[$offset]);
        return $this->set($key, array_values($before));
    }

    /**
     * 把数组中的第一个元素删除
     *
     * @param string $key
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function shift(string $key): bool {
        $before = $this->get($key);
        if (empty($before)) {
            return false;
        }

        array_shift($before);
        return $this->set($key, $before);
    }

    /**
     * 在数组的前端添加一个或多个元素
     *
     * @param $key
     * @param $value
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function unshift($key, $value): bool {
        $before = $this->get($key);
        if (empty($before)) {
            return false;
        }

        array_unshift($before, $value);
        return $this->set($key, $before);
    }

    /**
     * 删除缓存
     *
     * @access public
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if the $key string is not a legal value.
     */
    public function delete($key): bool {
        if (empty($this->cache_handle)) {
            return false;
        }

        $key = $this->getCacheKey((string)$key);

        $this->writeTimes++;
        try {
            $this->cache_handle->set($key, '', 1);
            return $this->cache_handle->delete($key);
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * 清除缓存
     *
     * @access public
     * Wipes clean the entire cache's keys.
     * @return bool True on success and false on failure.
     */
    public function clear(): bool {
        if (empty($this->cache_handle)) {
            return false;
        }

        $this->writeTimes++;
        return $this->cache_handle->clear();
    }

    /**
     * 读取缓存
     *
     * @access public
     * Obtains multiple cache items by their unique keys.
     *
     * @param iterable $keys    A list of keys that can obtained in a single operation.
     * @param mixed    $default 默认值 Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if $keys is neither an array nor a Traversable, or if any of the $keys are not a legal value.
     */
    public function getMultiple($keys, $default = null): iterable {
        if (!empty($this->cache_handle)) {
            try {
                return $this->cache_handle->getMultiple($keys, $default);
            } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            }
        }

        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        if (!empty($result)) {
            return $result;
        }

        return $default;
    }

    /**
     * 写入缓存
     *
     * @access public
     * Persists a set of key => value pairs in the cache, with an optional TTL.
     *
     * @param iterable               $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|\DateInterval $ttl    Optional. The TTL value of this item.
     *                                       If no value is sent and the driver supports TTL then the library may set a default value for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if $values is neither an array nor a Traversable, or if any of the $values are not a legal value.
     */
    public function setMultiple($values, $ttl = null): bool {
        if (!empty($this->cache_handle)) {
            try {
                return $this->cache_handle->setMultiple($values, $ttl);
            } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            }
        }

        foreach ($values as $key => $val) {
            $result = $this->set($key, $val, $ttl);
            if (false === $result) {
                return false;
            }
        }

        return true;
    }

    /**
     * 删除缓存
     *
     * @access public
     * Deletes multiple cache items in a single operation.
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if $keys is neither an array nor a Traversable, or if any of the $keys are not a legal value.
     */
    public function deleteMultiple($keys): bool {
        if (!empty($this->cache_handle)) {
            $this->cache_handle->setMultiple($keys, 1);
            try {
                return $this->cache_handle->deleteMultiple($keys);
            } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            }
        }

        foreach ($keys as $key) {
            $result = $this->delete($key);
            if (false === $result) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines whether an item is present in the cache.
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     * throws \Psr\SimpleCache\InvalidArgumentException MUST be thrown if the $key string is not a legal value.
     */
    public function has($key): bool {
        if (!empty($this->cache_handle)) {
            try {
                return $this->cache_handle->has($this->getCacheKey((string)$key));
            } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
            }
        }

        return false;
    }

    /**
     * Call a callback with an array of parameters
     *
     * @link https://php.net/manual/en/function.call-user-func-array.php
     *
     * @param callback $callback The function to be called.
     * @param array    $args     The parameters to be passed to the function, as an indexed array.
     *
     * @return mixed|false the function result, or false on error.
     */
    public function __call($callback, $args) {
        return call_user_func_array([$this->cache_handle, $callback], $args);
    }

    /**
     * 获取有效期
     *
     * @access protected
     *
     * @param int|\DateTimeInterface|\DateInterval $expire
     *
     * @return int
     */
    protected function getExpireTime($expire): int {
        if ($expire instanceof \DateTimeInterface) {
            return (int)($expire->getTimestamp() - time());
        }

        if ($expire instanceof \DateInterval) {
            return (int)(\DateTime::createFromFormat('U', (string)time())->add($expire)->format('U') - time());
        }

        return (int)$expire;
    }

    /**
     * 获取实际的缓存标识
     *
     * @access public
     *
     * @param string $key
     *
     * @return string
     */
    public function getCacheKey(string $key): string {
        return ($this->options['prefix'] ?? '') . $key;
    }

    /**
     * 获取缓存读取次数
     *
     * @access public
     * @return int
     */
    public function getReadTimes(): int {
        return $this->readTimes;
    }

    /**
     * 获取缓存写入次数
     *
     * @access public
     * @return int
     */
    public function getWriteTimes(): int {
        return $this->writeTimes;
    }

    /**
     * 序列化数据
     *
     * @access protected
     *
     * @param mixed $data
     *
     * @return string
     */
    protected function serialize($data): string {
        if (is_numeric($data)) {
            return (string)$data;
        }

        $serialize = $this->options['serialize'][0] ?? 'serialize';

        return $serialize($data);
    }

    /**
     * 反序列化数据
     *
     * @access protected
     *
     * @param string $data
     *
     * @return mixed
     */
    protected function unserialize($data) {
        if (is_numeric($data)) {
            return $data;
        }

        if (is_array($data)) {
            return $data;
        }

        if (is_bool($data)) {
            return $data;
        }

        $unserialize = $this->options['serialize'][1] ?? 'unserialize';
        return $unserialize($data);
    }
}