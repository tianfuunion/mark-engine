# mark-engine

[![Latest Stable Version](https://poser.pugx.org/tianfuunion/mark-engine/v/stable)](https://packagist.org/packages/tianfuunion/mark-engine)
[![Build Status](https://travis-ci.org/tianfuunion/mark-engine.svg?branch=master)](https://travis-ci.org/tianfuunion/mark-engine)
[![Coverage Status](https://coveralls.io/repos/github/tianfuunion/mark-engine/badge.svg?branch=master)](https://coveralls.io/github/tianfuunion/mark-engine?branch=master)

#### 介绍

马克引擎（Mark Engine） [天府联盟](https://www.tianfuunion.cn) 。详情请看 [https://gitee.com/tianfuunion/mark-engine/wikis](https://gitee.com/tianfuunion/mark-engine/wikis)

#### 软件架构

软件架构说明

#### 安装教程

~~~
composer require tianfuunion/mark-engine
~~~

或者在你的`composer.json`中声明对 Mark Engine SDK For PHP 的依赖：

~~~
"require": {
    "tianfuunion/mark-engine": "^2.0"
}
~~~

## 版权信息

Mark Engine遵循MulanPSL-2.0开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有 南阳市天府网络科技有限公司

Copyright © 2017-2024 by TianFuUnion (https://www.tianfuunion.cn) All rights reserved。

TianFuUnion® 商标和著作权所有者为南阳市天府网络科技有限公司。

- [MulanPSL-2.0](http://license.coscl.org.cn/MulanPSL2/)
- 更多细节参阅 [LICENSE.txt](https://gitee.com/tianfuunion/mark-engine/blob/master/LICENSE.txt)
-

### 联系我们

- [天府联盟官方网站：www.tianfuunion.cn](https://www.tianfuunion.cn)
- [天府联盟联合授权：auth.tianfu.ink](https://auth.tianfu.ink)
- [天府联盟反馈邮箱：report@tianfuunion.cn](mailto:report@tianfuunion.cn)